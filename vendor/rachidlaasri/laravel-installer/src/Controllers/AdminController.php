<?php

namespace RachidLaasri\LaravelInstaller\Controllers;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;

class AdminController extends Controller
{

    /**
     * @var DatabaseManager
     */
    private $databaseManager;

    /**
     * @param DatabaseManager $databaseManager
     */
    public function __construct(User $users)
    {
        $this->users = $users;
    }

    /**
     * Migrate and seed the database.
     *
     * @return \Illuminate\View\View
     */
    public function admin()
    {
        return view('vendor.installer.admin');
    }



    public function postAdmin( Request $request )
    {
        // Validate
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Get the role
        $role = Role::where('name', 'admin')->first();

        // Create the admin
        $user = User::create([
            'first_name'    => $request->get('first_name'),
            'last_name'     => $request->get('last_name'),
            'email'         => $request->get('email'),
            'password'      => $request->get('password'),
            'role_name'     => 'Admin'
        ]);

        $user->attachRole($role);

        
        return redirect()->route('LaravelInstaller::final')
                         ->with(['message' => 'Success']);
    }
}
