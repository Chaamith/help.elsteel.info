<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'password_reset_message' => 'Click here to reset the password',
    'loginTitle' => 'Hannover 2018',
    'registerTitle' => 'Register now',
    'reset_password_title' => 'Reset Password',
    'remember' => 'Remember me?',
    'login_btn' => 'Login',
    'register_btn' => 'Register',
    'register_now' => 'Register now',
    'forget_password' => 'Forget password?',
    'already_user' => 'Already a user? Login here',

    'placeholders' => [
        'first_name'    => 'First name',
        'last_name'    => 'Last name',
        'email'    => 'Email address',
        'password'    => 'Password',
        'confirm_password'    => 'Confirm password',
    ]

];
