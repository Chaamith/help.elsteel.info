<?php

return [

    /**
     *
     * Shared translations.
     *
     */
    'title' => 'Support Pro Installer',
    'next' => 'Next Step',


    /**
     *
     * Home page translations.
     *
     */
    'welcome' => [
        'title'   => 'Welcome To The Support Pro Installer',
        'message' => 'Welcome to the Support pro setup wizard.',
    ],


    /**
     *
     * Home page view translations.
     *
     */
    'homepage' => [
        'facing_problem'   => 'Facing a problem? Try searching here...',
        'form_title' => 'Didn’t find what you want?  <span>Open a ticket now!</span>',
        'form_subtitle' => 'We are very responsive in our service. You will get reply within 24 hours.',

        'placeholders' => [
            'subject'   => 'Subject',
            'department'   => 'What type of support you want?',
            'message'   => 'Type your message here',
            'file'   => '<span class="browse">Browse</span> your file here&hellip;',
        ]
    ],



    /**
     *
     * Dashboard Language translations
     *
     */
    'dashboard' => [
        'pageTitle' => 'Dashboard | ',
        'allTickets' => 'All Tickets',
        'newTickets' => 'New Tickets',
        'solvedTickets' => 'Solved Tickets',
        'pendingTickets' => 'Pending Tickets',
        'recentTickets' => 'Recent Tickets',
        'ticketID' => 'Ticket ID',
        'ticketTitle' => 'Ticket title',
        'department' => 'Department',
        'date' => 'Date',
        'client' => 'Client',
        'status' => 'Status',
        'noTickets' => 'No tickets found',
        'newClient' => 'New Clients',
        'allClient' => 'All Clients',
        'noClients' => 'No Clients',
        'staffStatus' => 'Staff status',
        'allStaffs' => 'All staffs',
        'staffName' => 'Staff name',
        'department' => 'Department',
        'assignedTask' => 'assigned task',
        'solved' => 'solved',
        'noStaffs' => 'No staffs found',
        'statisticsTitle' => 'statistics of this month',
    ],
    


    /**
     *
     * Tickets page language Translations
     *
     */
    
    'tickets'   => [
        'pageTitle' => 'Search for ',
        'title' => ' Tickets | ',
        'tickets' => ' Tickets',
        'searchTitle' => ' Showing Search results for',
        'ticketID' => 'Ticket ID',
        'ticketTitle' => 'Ticket title',
        'department' => 'Department',
        'date' => 'Date',
        'client' => 'Client',
        'status' => 'Status',
        'action' => 'Action',
        'noTickets' => 'No tickets found',
    ],



    /**
     *
     * Single Ticket page language Translations
     *
     */
    'singleTicket' => [
        'id' => '#ID:',
        'attachedFile' => 'Attached File:',
        'ticketID' => 'Ticket ID:',
        'client' => 'Client',
        'supportType' => 'Support Type:',
        'ticketStatus' => 'Ticket Status:',
        'assignedTicket' => 'Assigned Ticket:',
        'solvedBy' => 'Solved By:',
        'assignNewTicket' => 'Assign New Ticket',
        'assignThisTicket' => 'Assign This Ticket',
        'notAssignedYet' => 'Not assigned yet.',
        'noStaffsFound' => 'No Staff found',
        'updateStatus' => 'Update STatus',
        'updateTicketStatus' => 'Update Ticket STatus',
        'solved' => 'Solved',
        'pending' => 'Pending',
    ],


    /**
     *
     * Replies language translations
     *
     */
    'replies' => [
        'label' => 'Reply this ticket',
        'AttachedFIle' => 'Attached File:',
        'attachFIle' => 'Attach file&hellip;',
        'sendReply' => 'Send reply',
    ],


    /**
     *
     * All Clients
     *
     */
    'allClients'    => [
        'pageTitle' => 'All Clients | ',
        'title' => 'Clients ',
        'name' => 'name',
        'email' => 'email',
        'clientImage' => 'Client Image',
        'registeredAt' => 'Registered at',
        'action' => 'Action',
        'noClients' => 'No clients to show.',
    ],


    /**
     *
     * Dashboard Add new clients language translations
     *
     */
    'addNewClient'  => [
        'title'  => 'Add new Client',
        'editTitle'  => 'Edit Client',
        'first_name'  => 'first_name',
        'last_name'  => 'last_name',
        'email'  => 'email',
        'password'  => 'password',
        'clientImage'  => 'Client image',
        'addClientBtn'  => 'Add Client',
        'editClientBtn'  => 'Edit Client',
        'placeholders' => [
            'firstName'   => 'John',
            'lastName'   => 'Doe',
            'email'   => 'john@company.com',
            'file'   => 'Select client image&hellip;',
        ],
    ],


    /**
     *
     * Dashboard Add new Staff language translations
     *
     */
    'addNewStaff'  => [
        'title'  => 'Add new staff',
        'editTitle'  => 'Edit staff',
        'firstName'  => 'first_name',
        'email'  => 'email',
        'password'  => 'password',
        'department'  => 'Department',
        'roleName'  => 'role_name',
        'staffImage'  => 'Staff image',
        'addStaffBtn'  => 'Add Staff',
        'updateStaffBtn'  => 'Update',
        'placeholders' => [
            'first_name'   => 'John',
            'lastName'   => 'Doe',
            'email'   => 'john@company.com',
            'role_name'   => 'Support Developer',
            'file'   => 'Select staff image&hellip;',
        ],
    ],



    /**
     *
     * All Staffs language translations
     *
     */
    'allStaffs' => [
        'pageTitle' => 'All Staffs | ',
        'title' => 'Staffs',
        'name' => 'name',
        'email' => 'email',
        'department' => 'department',
        'staffImage' => 'staff Image',
        'assignedTickets' => 'Assigned Tickets',
        'solvedTickets' => 'solved Tickets',
        'registeredAt' => 'Registered At',
        'action' => 'action',
        'noStaffs' => ' No staffs to show.',
    ],


    /**
     *
     * Settings page Languages translations
     *
     */
    'settings' => [
        'pageTitle' => 'Settings | ',
        'title' => 'Settings',
        'generalSettings' => 'General Settings',
        'themeSettings' => 'Theme Settings',
        'tiketSettings' => 'Ticket Settings',
        'appSettings' => 'App Settings',
        'yourProfile' => 'Your Settings',
    ],



    /**
     *
     * Sidebar view page translations
     *
     */
    'sidebar'   => [
        'mainMenu'  => 'Main Menu',
        'dashboard'  => 'Dashboard',
        'tickets'  => 'tickets',
        'allTickets'  => 'All Tickets',
        'newTickets'  => 'New Tickets',
        'pendingTickets'  => 'Pending Tickets',
        'solvedTickets'  => 'Solved Tickets',
        'staffs' => 'Staffs',
        'allStaffs' => 'All Staffs',
        'newStaffs' => 'New Staffs',
        'clients' => 'Clients',
        'allclients' => 'All Clients',
        'newclients' => 'New Clients',
        'settings' => 'Settings',
        'logout' => 'Logout',
    ],



    /**
     *
     * Nav page translations
     *
     */
    'nav' => [
        'settings' => 'Settings',
        'logout' => 'Logout',
        'notifications' => 'Notifications',
        'noNotifications' => 'No Notifications to show',
        'clearAll' => 'Clear All',
        'placeholders' => [
            'formSearch' => 'Search ticket by id or name'
        ],
    ],
    



    /**
     *
     * All tickets page frontend
     *
     */
    'allTickets' => [
        'pageTitle' => 'All tickets | ',
        'title' => 'All Tickets',
        'ticketID' => 'Ticket ID',
        'ticketTitle' => 'Ticket title',
        'department' => 'Department',
        'date' => 'Date',
        'status' => 'Status',
        'action' => 'Action',
        'noTickets' => 'Nothing to show.',
    ],



    /**
     *
     * Single ticket page frontend
     *
     */
    'allTickets' => [
        'id' => '#ID: ',
        'attachedFile' => '#ID: ',
    ],

    'errors' => [
        'siteRegistrations'  => 'Sorry, currently site Registration is turned off. ',
    ],



    /**
     *
     * Edit tickets page frontend
     *
     */
    'editTickets' => [
        'pageTitle' => 'Editing ticket -  ',
        'attachFIle' => 'Attached File:',
        'file' => '<span class="browse">Browse</span> your file here&hellip;',
        'fileAttachWarning' => 'By attacting new file, old file will be <strong>deleted.</strong> You can not attach more than 1 file.',
        'updateBtn' => 'Update ticket',
        'placeholders' => [
            'subject' => 'subject',
            'message' => 'Type your message here',
        ],
    ],


    /**
     *
     * Requirements page translations.
     *
     */
    'requirements' => [
        'title' => 'Requirements',
    ],


    /**
     *
     * Permissions page translations.
     *
     */
    'permissions' => [
        'title' => 'Permissions',
    ],


    /**
     *
     * Environment page translations.
     *
     */
    'environment' => [
        'title' => 'Environment Settings',
        'save' => 'Save .env',
        'success' => 'Your .env file settings have been saved.',
        'errors' => 'Unable to save the .env file, Please create it manually.',
    ],


    /**
     *
     * Final page translations.
     *
     */
    'final' => [
        'title' => 'Finished',
        'finished' => 'Application has been successfully installed.',
        'exit' => 'Click here to exit',
    ],
];
