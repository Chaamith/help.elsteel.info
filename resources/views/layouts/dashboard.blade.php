<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> @yield('title') </title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        
        <!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet"> -->


        <!-- NORMALIZING CSS -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css') }}">

        <!-- ICONIC FONT - FONT AWESOME -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}">

        <!-- Animate CSS-->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}">

        <!-- BOOTSTRAP FRONTEND FRAMEWORD -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.css') }}">

        <!-- SWEET ALERT FOR CUSTOM ALERT -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/sweetalert.css') }}">

        <!-- MODAL CSS FOR POPUP MODAL -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/modal.css') }}">

        <!-- JQUERY POPOVER FOR ON CLICK POPUP MENU -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery-popover-0.0.3.css') }}">

        <!-- JQUERY CUSTOM SCROLLBAR -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery.custom-scrollbar.css') }}">

        <!-- LOAD WEB FONTS -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/font-stylesheet.css') }}">

        <!-- MAIN STYLESHEET FILE -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/admin_style.css') }}">

        <!-- RESPONSIVE STYLESHEET -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/admin_responsive.css') }}">

        <!-- DATATABLE STYLESHEET -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/jquery.dataTables.min.css') }}">

        <!-- DateRange Picker STYLESHEET -->
        <link rel="stylesheet" href="{{ URL::asset('assets/css/daterangepicker.css') }}">

        <!-- MODERNIZER -->
        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.8.3.min.js') }}"></script>

        <script>
            var siteUrl = '{{ url("/") }}';
            var token = '{{ csrf_token() }}';
        </script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        


        <!--====================================
        =            WRAPPER STARTS            =
        =====================================-->


        <div class="wrapper">


            <!--===================================
            =            HEADER STARTS            =
            ====================================-->
            <div class="header">
                <div class="site-logo">
                    <a href="{{ route('dashboard.index') }}">
                        <img src="{{ URL::asset('assets/img/' . Options::getBackendLogo() ) }}" alt=" {{  Options::get('title') }} " class="logo">
                    </a>
                </div>

                @include('partials.dashboard.nav')

            </div>

            <!--====  End of HEADER  ====-->

            {{-- Dashboard Sidebar --}}
            @include('partials.dashboard.sidebar')


            <!--=====================================
            =            MAIN CONTENT WRAPPER       =
            ======================================-->


            <div class="main-content">
                @yield('content')

            </div>
            <footer class="footer" style="position: fixed;bottom: 0; background-color: #cdcdcd; width: 100%">

        <span class="text-muted" style="margin-left:50px;margin-top:15px;position: absolute">
            &copy; 2017 Elsteel &nbsp;| &nbsp;<a href="http://www.elsteel.com/elsteel-privacy-statement.php">Privacy Policy</a>
        </span>
                <span class="pull-right">
            <img src="{{ url('assets/img/footer-images.jpg') }}">
        </span>

            </footer>
        </div>
        

        {{-- Including Add Staff And Client Modal --}}
        @include('partials.dashboard.add_staff_and_client')
        @include('partials.dashboard.edit_staff_and_client')
        
        
        <!-- Overlay element for New Staff Modal -->
        <div class="md-overlay  hidden-xs"></div>
    


        <!-- IF ANY REASON CDN IS NOT AVAILABLE, THEN LOAD JQUERY FROM LOCAL FILE -->
        <script src="{{ URL::asset('assets/js/vendor/jquery-1.12.0.min.js') }}"></script>

        <!-- JQUERY CUSTOM SCROLLBAR -->
        <script src="{{ URL::asset('assets/js/jquery.custom-scrollbar.js') }}"></script>

        <!-- BOOTSTRAP JS -->
        <script src="{{ URL::asset('assets/js/bootstrap.js') }}"></script>

        <!-- CLASSIE JS FOR MODAL EFFECT -->
        <script src="{{ URL::asset('assets/js/classie.js') }}"></script>

        <!-- MODAL EFFECT PLUGIN -->
        <script src="{{ URL::asset('assets/js/modalEffects.js') }}"></script>

        <!-- SWEETALERT JS -->
        <script src="{{ URL::asset('assets/js/sweetalert.min.js') }}"></script>
    
        <!-- CHART JS FOR STATISTICS CHART -->
        {{--<script src="{{ URL::asset('assets/js/Chart.min.js') }}"></script>--}}
        <script src="{{ URL::asset('assets/js/highcharts.js') }}"></script>

        <!-- LEGEND FOR CHART JS -->
        <script src="{{ URL::asset('assets/js/legend.js') }}"></script>

        <!-- JQUERY POPOVER -->
        <script src="{{ URL::asset('assets/js/jquery-popover-0.0.3.js') }}"></script>

        <!-- JQUERY DATATABLE -->
        <script src="{{ URL::asset('assets/js/jquery.dataTables.min.js') }}"></script>

        <!-- CUSTOM SCRIPT FILE -->
        <script src="{{ URL::asset('assets/js/admin_main.js') }}"></script>
        
        <!-- SOME COMMON SCRIPTS FOR BACKEND AND FRONTEND -->
        <script src="{{ URL::asset('assets/js/commonScripts.js') }}"></script>
        <script type="text/javascript" src="/assets/js/moment.min.js"></script>
        <script type="text/javascript" src="/assets/js/daterangepicker.js"></script>
        @yield('script')

            
        <!-- jQuery Custom Scrollbar -->
        <script type="text/javascript">
            $(window).load(function () {
                $(".scrollbar").customScrollbar();
                $('#datetimepicker1').daterangepicker({
                    "timePicker": true,
                    "timePicker24Hour": true,
                    "timePickerSeconds": true,
                    locale: {
                        format: 'YYYY-MM-DD HH:mm:ss '
                    }
                }, function(start, end, label) {
                   // console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                });
            });
        </script>

        
    </body>
</html>