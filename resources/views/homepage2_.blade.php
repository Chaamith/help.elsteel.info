@extends('layouts.app')

@section('title', Options::get('title') )

@section('head_scripts')
    <!-- Google Recaptcha -->
        {{--<script src='https://www.google.com/recaptcha/api.js'></script>--}}
@stop


@section('body_class', 'style2')

@section('content')

<div class="container">
<div class="search-pblm">
    {{--<form action="">--}}
        {{--<div class="form-group">--}}
            {{--<input type="text" name="problem" id="problem" class="form-control" placeholder=" {{ trans('messages.homepage.facing_problem') }} ">            --}}
            {{--<button type="submit"><i class="fa fa-search"></i></button>--}}
        {{--</div>--}}
    {{--</form>--}}
</div>

<div class="ticket-area">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                {{--<div class=" col-md-8" >--}}
                    {{--<div class="panel panel-default">--}}
                        {{--<div class="panel-heading">Elsteel Group Infrastructure Status</div>--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="row" style="display:inherit;">--}}
                                {{--<div class="col-md-3 col-lg-2">--}}
                                    {{--<h1 style="margin:0;font-size: 36px;font-weight: bold" class="hidden-sm hidden-xs">--}}
                                        {{--<span class="fa fa-check-circle-o" style="font-size:60px;color:#008a32"></span>--}}
                                    {{--</h1>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-9 col-lg-10">--}}
                                    {{--<h3 style="margin-top:0;margin-bottom:0px;">All Systems Operational</h3>--}}
                                    {{--<small id="noissuesubtitle" style="color: #A6A6A6;">If you are experiencing an issue, please contact customer support.</small>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--<h1 id="category"style="margin:0;font-size: 36px;font-weight: bold">STATUS</h1>--}}
                    {{--<h6 id="subcategory" style="margin:0;">Elsteel Group Infrastructure Status</h6>--}}
                {{--</div>--}}
                {{--<div class=" col-md-8" >--}}
                    {{--<div class="row" style="display:inherit;">--}}
                        {{--<div class="col-md-3 col-lg-2">--}}
                            {{--<h1 style="margin:0;font-size: 36px;font-weight: bold" class="hidden-sm hidden-xs">--}}
                                {{--<span class="fa fa-check-circle-o" style="font-size:60px;color:#008a32"></span>--}}
                            {{--</h1>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-9 col-lg-10">--}}
                            {{--<h3 style="margin-top:0;margin-bottom:0px;">All Systems Operational</h3>--}}
                            {{--<small id="noissuesubtitle" style="color: #A6A6A6;">If you are experiencing an issue, please contact customer support.</small>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
                <div class=" col-md-8">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><strong>System Status..</strong></div>
                        <div class="panel-body">
                            <ul class="">

                                @foreach($messages as $message)
                                   <h4> {!! $message->statusMessage() !!} {{ $message->message }}</h4>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <!-- Ticket submit form -->
            <div class="submit-ticket">
                <div class="heading-text text-center">
                    <h2>{!! trans('messages.homepage.form_title') !!}</h2>
                    <p> {{ trans('messages.homepage.form_subtitle') }} </p>
                </div>

                @include('partials.site.ticket_form')

                        <div class="form_row hm_p_f_r">
                            <div class="form_half half_left ">
                                <div class="ticket-submit-file">
                                    <input type="file" name="file" id="file-2" class="inputfile" data-multiple-caption="{count} files selected" />
                                    <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span> {!! trans('messages.homepage.placeholders.file') !!} </span></label>
                                </div>
                            </div>

                            {{--<div class="form_half half_right form_gcaptha_controllable">--}}
                                {{--<div class="g-recaptcha" data-sitekey="{{ env('G_RECAPTCHA_KEY') }}"></div>--}}
                            {{--</div>--}}
                        </div>

                        <div class="form-status"></div>


                        <div class="form-group">
                            <input type="submit" onclick="tinyMCE.triggerSave(true,true);"  value="Submit your ticket" class="btn btn-submit create-ticket-btn">
                        </div>

                    </form>

                </div>
                <!-- Ticket form ends -->
            </div>
            <!-- Ticket submit form ends -->
        </div>
        
    </div>
</div>
</div>



@endsection


@section('script')

    <script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js') }}"></script>

    {{-- Including tinymce plugin load script --}}
    @include('partials.site.homepage_tinymce_script')


@stop

