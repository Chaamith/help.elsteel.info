@extends('layouts.auth')

@section('title', 'Login | ' . Options::get('title') )

@section('content')
<p class="title"> {{ trans('auth.loginTitle') }} </p>

<div class="the-form">

    {{ Form::open(['url' => 'login', 'method' => 'post'])}}

    
        <div class="form-group">
            <div class="input-group">
              <span class="input-group-addon" id="email_addon">
                  <i class="fa fa-user"></i>
              </span>
                {{ Form::email('email', null, ['placeholder' => trans('auth.placeholders.email'), 'class' => 'form-control', 'aria-describedby' => 'email_addon']) }}
            </div>
            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('email') }} </span>
            @endif
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon" id="password_addon">
                    <i class="fa fa-lock"></i>
                </span>
                {{ Form::password('password', ['placeholder' => trans('auth.placeholders.password'), 'class' => 'form-control', 'aria-describedby' => 'password_addon']) }}
            </div>
            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('password') }} </span>
            @endif
        </div>


        <div class="form-group clearfix sp_check">
            <input type="checkbox" name="remember" id="remember">
            
            <label for="remember"> <span></span> {{ trans('auth.remember') }} </label>
        </div>

        {{ Form::submit( trans('auth.login_btn') , ['class' => 'btn btn-info']) }}

        <div class="help-links">
            
            {{-- Check if site registration is turned on or not --}}
            @if( Options::get('user_registration') == 'on' )
                <p><a href="register"> {{ trans('auth.register_now') }} </a></p>
            @endif

            <p><a href="{{ url('/password/reset') }}">{{ trans('auth.forget_password') }} </a></p>
        </div>
    {{ Form::close() }}


</div>

@endsection
