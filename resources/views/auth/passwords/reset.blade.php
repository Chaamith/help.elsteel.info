@extends('layouts.auth')

@section('title', 'Reset Password | ' . Options::get('title') )

@section('content')
<p class="title"> {{ trans('auth.reset_password_title') }} </p>

<div class="the-form">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group">
                              <span class="input-group-addon" id="email_addon">
                                  <i class="fa fa-user"></i>
                              </span>
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}">

                            </div>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>



                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <div class="input-group">
                              <span class="input-group-addon" id="email_addon">
                                  <i class="fa fa-lock"></i>
                              </span>
                            
                            <input id="password" type="password" class="form-control" name="password" placeholder=" {{ trans('auth.placeholders.password') }} ">

                            </div>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <div class="input-group">
                              <span class="input-group-addon" id="email_addon">
                                  <i class="fa fa-lock"></i>
                              </span>
                            
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="{{trans('auth.placeholders.confirm_password')}}">

                            </div>
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

            

                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-refresh"></i> {{ trans('auth.reset_password_title') }}
                                </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
