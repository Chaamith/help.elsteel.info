@extends('layouts.auth')

@section('title', 'Register | ' . Options::get('title') )


@section('content')
<p class="title"> {{ trans('auth.registerTitle') }} </p>
<div class="the-form register-form">

    {{ Form::open(['url' => 'register', 'method' => 'post'])}}
        <div class="form-group">
            {{ Form::text('first_name', null, ['placeholder' =>  trans('auth.placeholders.first_name') , 'class' => 'form-control']) }}

            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('first_name') }} </span>
            @endif
        </div>
        <div class="form-group">
            {{ Form::text('last_name', null, ['placeholder' => trans('auth.placeholders.last_name'), 'class' => 'form-control']) }}

            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('last_name') }} </span>
            @endif
        </div>
        <div class="form-group">
            {{ Form::email('email', null, ['placeholder' => trans('auth.placeholders.email'), 'class' => 'form-control']) }}

            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('email') }} </span>
            @endif

        </div>
        <div class="form-group">
            {{ Form::password('password', ['placeholder' => trans('auth.placeholders.password'), 'class' => 'form-control']) }}

            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('password') }} </span>
            @endif
        </div>

        <div class="form-group">
            {{ Form::password('password_confirmation', ['placeholder' => trans('auth.placeholders.confirm_password'), 'class' => 'form-control']) }}
            <span class="help-block"></span>

            @if( $errors->any() ) 
                <span class="help-block"> {{ $errors->first('password_confirmation') }} </span>
            @endif
        </div>

        <div class="form-group">
            {{-- Check if site registration is turned on or not --}}
            @if( Options::get('user_registration') !== 'on' )
                <p class="text-error"> {{ trans('messages.errors.siteRegistrations') }} </p>
            @endif
        </div>

            {{-- Check if site registration is turned on or not --}}
            @if( Options::get('user_registration') == 'on' )
                
                {{ Form::submit( trans('auth.register_btn') , ['class' => 'btn btn-success']) }}

            @else 

                {{ Form::submit( trans('auth.register_btn') , ['class' => 'btn btn-success', 'disabled' => 'disabled']) }}

            @endif

        <div class="help-links">
            <p><a href="login"> {{ trans('auth.already_user') }} </a></p>
        </div>
        
    {{ Form::close() }}
</div>
@endsection
