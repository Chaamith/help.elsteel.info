@extends('vendor.installer.layouts.master')

@section('title', trans('messages.final.title'))
@section('container')
    <p class="paragraph">Your application is installed Successfully. </p>
    <div class="buttons">
        <a href="/" class="button">{{ trans('messages.final.exit') }}</a>
    </div>
@stop