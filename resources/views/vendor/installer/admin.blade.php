@extends('vendor.installer.layouts.master')

@section('title', 'Create Admin User')
@section('container')

    <form action="{{ route('LaravelInstaller::postAdmin') }}" class="add_admin_form" method="post">

        <input type="text" name="first_name" placeholder="First Name" class="form-control" required>
        <input type="text" name="last_name" placeholder="Last Name" class="form-control" required>
        <input type="email" name="email" placeholder="Email" class="form-control" required>
        <input type="password" name="password" placeholder="Password" class="form-control" required>

        <input type="submit" value="Create" class="btn">
    </form>
@stop