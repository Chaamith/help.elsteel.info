@extends('vendor.installer.layouts.master')

@section('title', trans('messages.environment.title'))
@section('container')
    @if (session('message'))
    <p class="alert">{{ session('message') }}</p>
    @endif
    <form method="post" action="{{ route('LaravelInstaller::environmentSave') }}" class="add_admin_form">
        <label for="db_host">DB HOST</label>
        <input type="text" name="db_host" id="db_host" placeholder="DB HOST" class="form-control" value="localhost" required>

        <label for="db_database">DB DATABASE</label>
        <input type="text" name="db_database" id="db_database" placeholder="Your database name" class="form-control" required>

        <label for="db_username">DB USERNAME</label>
        <input type="text" name="db_username" id="db_username" placeholder="Database username" class="form-control" value="root" required>

        <label for="db_password">DB PASSWORD</label>
        <input type="text" name="db_password" id="db_password" placeholder="Database password" class="form-control" >

        <label for="db_prefix">DB PREFIX</label>
        <input type="text" name="db_prefix" id="db_prefix" placeholder="sp_"  value="sp_" class="form-control" >

        {!! csrf_field() !!}
        @if(!isset($environment['errors']))
        <div class="buttons">
            <button class="button">
                {{ trans('messages.next') }}
            </button>
        </div>
        @endif
    </form>

@stop