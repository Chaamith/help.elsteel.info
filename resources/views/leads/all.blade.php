@extends('layouts.app')

@section('title', 'All Leads' )

@section('content')

<div class="container all-tickets-area">
    @include('partials.site.flash.success')
    <div class="ticket-area">

                    <div class="ticket-big-box ticket-box all-user-ticket-box">
                        <div class="box-title">
                            <div class="title-text">
                                <h3>All Leads</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover  responsive-table">
                                <thead>
                                    <tr>
                                        <th>Company</th>
                                        <th>Contact 1</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                @if( count($leads) )

                                    @foreach($leads as $lead)
    	                                <tr class="ticket_row">

    	                                    <td>
    	                                    	<a href="{{ route('edit.lead', [
    	                                    		'id' => $lead->id
                                                ]) }}">
    	                                    		{{ $lead->company }}
    	                                    	</a>
    	                                    </td>

                                            <td>{{ $lead->contact1 }}</td>

    	                                    <td class="action-btns">

                                                <a href="{{ route('edit.lead', $lead->id) }}"  title="Edit"><i class="fa fa-edit"></i></a>

    	                                    	<button type="submit" class="lead_remove remove-btn" value="{{ $lead->id }}" title="Delete">
                                                    <i class="fa fa-times"></i>
                                                </button> 
    	                                    </td>
    	                                </tr>
                                    @endforeach


                                @else
                                    <td colspan="6" align="center"> N/A </td>
                                @endif
                            </table>                              
                        </div>
                    </div>



                    <!-- Pagination -->
                   {{--{{ $lead->render() }}--}}
    </div>
</div>
@stop

@section('script')
<script>
    $(document).ready(function() {
    /*=======================================================
     =             REMOVE LEADS                     =
     =======================================================*/
    $('.ticket-area').on('click','.lead_remove', function(e) {
        e.preventDefault();
        var $this = $(this);
        var id = $this.val();

        swal({
                title: "Are you sure to delete?",
                text: "By deleting any lead all related to it will be deleted too and it can not be undone.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#FF474C",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if(isConfirm) {

                    processLeadRemoveAjaxRequest($this, id);

                } else {
                    // Notify the user
                    swal({
                        title: "Cancelled",
                        type: 'error',
                        text: "Deletion action has been cancelled :)",
                        timer: 1500,
                        showConfirmButton: true
                    });
                }
            });

    });

    });
    function processLeadRemoveAjaxRequest($this, id) {

        //  Send the ajax request
        $.ajax({
            url: siteUrl + '/lead/remove',
            type: 'POST',
            data: {id : id, _token: token},
            success: function(data) {

                // Delete the row
                $this.closest('tr').remove();
                // Notify the user
                swal({
                    title: "Deleted",
                    type: 'success',
                    text: "Deleted successfully.",
                    timer: 1500,
                    showConfirmButton: true
                });

            },
            error: function(data) {
                swal({
                    title: "Error",
                    type: 'error',
                    text: "Sorry, an error occured. Please try again later.",
                    timer: 1500,
                    showConfirmButton: true
                });
            }
        });
    }

</script>
@stop