@extends('layouts.app')

@section('title', trans('messages.editTickets.pageTitle') . $ticket->subject . ' |  ' . Options::get('title') )

@section('content')

<div class="container">

<div class="ticket-area">
    <div class="row">
        <div class="col-md-7">
            <!-- Ticket submit form -->
            <div class="submit-ticket">

                <div class="heading-text">
                    <h2>{{ trans('messages.editTickets.pageTitle')}}  <i>{{ $ticket->subject }}</i> </h2>
                </div>


                <div class="ticket-form">
                    {{ Form::model($ticket, [
                        'route' => ['edit.post.ticket', $ticket->id],
                        'method' => 'post',
                        'id' => 'createTicket',
                        'enctype' => 'multipart/form-data'
                    ])}}

                        {{-- Including the session flash success message  --}}
                        @include('partials.site.flash.success')

                    <div class="form-group location-group">

                        {{ Form::select('location', config('settings.location'), $ticket->location, ['class' => 'form-control']) }}
                        <span class="help-block"></span>

                    </div>
                        <div class="form-group subject-group">

                            {{ Form::text('subject', null, ['class' => 'form-control subject', 'placeholder' => trans('messages.editTickets.placeholders.subject') ]) }}
                            <span class="help-block "></span>

                        </div>

                        <div class="form-group department-group">

                            {{ Form::select('department_id', $departments, $ticket->department_id, ['class' => 'form-control department']) }}
                            <span class="help-block"></span>

                        </div>


                        <div class="form-group message-group">

                            {{ Form::textarea('message', null, ['class' => 'form-control message', 'placeholder' => trans('messages.editTickets.placeholders.message'), 'wrap' => 'hard', 'onkeyup' => 'autoGrow(this);' ]) }}
                            <span class="help-block"></span>

                        </div>

                        @if( $ticket->files )
                            <div class="file-attached" style="margin-bottom: 30px;">
                                <div class="file-single">
                                    <div class="file-t">
                                        <span>{{ trans('messages.editTickets.attachFIle') }}</span>
                                    </div>
                                    <div class="file-name">
                                        <span>
                                            <a href="/uploads/{{ $ticket->files }}"> {{ $ticket->files }} </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @endif

                    <div class="row file-row edit-page-file clearfix">
                            <div class="col-md-12">
                                <div class="form-group ticket-submit-file">
                                    <input type="file" name="file" id="file-2" class="inputfile" data-multiple-caption="{count} files selected" />
                                    <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span> {!! trans('messages.editTickets.file') !!} </span></label>

                                </div>

                            </div>
                        </div>

                        {{-- Show warning message  --}}
                        @if( $ticket->files )
                           <div class="edit-waring-msg">
                                <span class="help-block text-warning">{!! trans('messages.editTickets.fileAttachWarning') !!}</span>
                           </div>
                        @endif

                        {{-- Including the error message  --}}
                        @include('partials.site.flash.error')


                        <div class="form-group">
                            <input type="submit" value="{{ trans('messages.editTickets.updateBtn')}}" class="btn btn-submit">
                        </div>

                    </form>
                </div>
            </div>
            <!-- Ticket submit form ends -->
        </div>

    </div>
</div>
</div>



@endsection
