@extends('layouts.app')

@section('title', $ticket->subject . ' | ' . Options::get('title') )

@section('content')
            <div class="container">

                <div class="ticket-area">
                    <!-- SINGLE TICKET -->
                    <div class="row content-body ticket-single-page">
                        <div class="col-lg-12 col-md-12 ticket-details">

                            <!-- TICKET BOX, TICKET DETAIL -->
                            <div class="ticket-box">
                                <div class="ticket-header">
                                    <h4>{{ trans('messages.singleTicket.id')}} {{ $ticket->id }}</h4>
                                </div>
                                <div class="ticket-title row">
                                    <div class="col-md-8 col-lg-9">
                                        <h3> {{ $ticket->subject .' ('.@config('settings.location')[$ticket->location].')'}} </h3>
                                    </div>
                                    <div class="col-md-4 col-lg-3 ">

	                                     <!-- Check If the date is over 1 month or not -->
                                   		<?php $date = $ticket->created_at; ?>
                                        <p class="t-t-d">   {{ $date->diffInMonths(Carbon::now()) >= 1 ? $date->format('j M Y, g:ia') : $date->diffForHumans() }}  </p>

                                    </div>
                                </div>
                                <div class="ticket-detail">
                                    {!! $ticket->message !!}
									
									@if( $ticket->files )
	                                    <div class="file-attached">
	                                        <div class="file-single">
	                                            <div class="file-t">
	                                                <span>{{ trans('messages.singleTicket.attachedFile')}}</span>
	                                            </div>
	                                            <div class="file-name">
	                                                <span>
	                                                    <a href="/uploads/{{ $ticket->files }}"> {{ $ticket->files }} </a>
	                                                </span>
	                                            </div>
	                                        </div>
	                                    </div>
                                    @endif
                                </div>

                                
                                @include('partials.site.replies')
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
@stop


{{-- Include scripts --}}
@section('script')
    
    
    <script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js') }}"></script>
    <script>
        // Tiny MCE Editor for the Custom Textarea,
        tinymce.init({
        selector: '#reply-textarea', 
        height: 150,
        menubar: false,
        paste_auto_cleanup_on_paste : true,
        paste_remove_styles: true,
        fullpage_default_fontsize: "18px",
        paste_as_text: true,
        paste_remove_styles_if_webkit: true,
        paste_strip_class_attributes: "all",
        plugins : 'paste advlist autolink link image lists charmap print preview',
        toolbar: false,
        content_css: siteUrl + '/assets/css/tinymce.site.css',
        });
    </script>

@stop