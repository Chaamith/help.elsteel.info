<style>
    .button{
        box-shadow: rgb(84, 163, 247) 0px 1px 0px 0px inset;
        background: linear-gradient(rgb(0, 125, 193) 5%, rgb(0, 97, 167) 100%) rgb(0, 125, 193);
        border-radius: 3px;
        border: 1px solid rgb(18, 77, 119);
        display: inline-block;
        cursor: pointer;
        font-family: Arial;
        font-size: 13px;
        padding: 6px 24px;
        text-decoration: none;
        text-shadow: rgb(21, 70, 130) 0px 1px 0px;
    }
</style>

<table class="mui-body" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td  style="padding:50px;" class="mui-panel">


            <!--[if mso]><table><tr><td class="mui-container-fixed"><![endif]-->
            <div style="text-align:center;" class="mui-container">
                <!--

                email goes here

                -->
                <div class="mui-divider-bottom">
                    <h3 style="margin-top: 5px!important; margin-bottom: 16px!important; color: grey!important;">New Ticket ({{$ticket->subject}})</h3>
                    <div class="mui-divider-bottom"></div>

                    <p style="padding:20px; text-align:left;">
                        Hello  {{ $user->fullName() }} - <br><br>
                        We have received your ticket. You will get a reply from us as soon as possible.</p>
                     <a class="button" href="{!! route('single.ticket', [ 'id' => $ticket->id, 'subject' => $ticket->FormatSubject() ]) !!}"> View ticket </a>
                    <br><hr><br>
                </div>
            </div>
<div align="left">
    <p><strong>Kind regardsThe <br>Elsteel IT Team</strong></p>
    <img height="46"  style="width:200px" src="https://www.elsteel.com/images/elsteel-logo.png">
    <p><strong>For more information please visit www.elsteel.com</strong></p>
<p style="font-size: 10px">This newsletter contains ELSTEEL’s proprietary and confidential information. You received this newsletter because you have subscribed to it. If you are not the intended recipient you are hereby notified not to disclose, copy, distribute or use the information contained herein. Please delete this communication from your e-mail system and let us know immediately by emailing to elsteel@elsteel.com, if you have received this communication by mistake or if you do not wish to keep your subscription to this newsletter any longer.</p>
</div>

            <!--[if mso]></td></tr></table><![endif]-->

        </td>
    </tr>
</table>