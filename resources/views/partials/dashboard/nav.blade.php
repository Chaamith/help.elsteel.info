<!-- NAVIGATION -->
                <div class="navigation">
                    <div class="nav-mobile-menu">
                        <div class="mb-menu">
                            <i class="fa fa-bars"></i>
                        </div>
                    </div>
                    <div class="nav-form">
                        {{-- Search form --}}
                        {{ Form::open(['route' => 'dashboard.ticket.search', 'method' => 'get']) }}

                            <input type="text" name="term" id="ticket" class="ticket" placeholder="{{ trans('messages.nav.placeholders.formSearch') }}" value="{{ Request::get('term') ? Request::get('term') : '' }}">

                            <button type="submit"><i class="fa fa-search"></i></button>

                        {{ Form::close() }}

                    </div>
                    <div class="nav-profile">
                        <div class="nav-right-item nav-user-profile">
                            <div  class="nav-user-name">
                                <h3>{{ Auth::user()->fullName() }}</h3>
                                <span class="role"> {{ Auth::user()->role_name }} </span>
                            </div>
                            <div class="nav-user-img">
                                <img src="{{ Auth::user()->profileImg(35) }}" alt="">
                            </div>
                            <div class="nav-user-drop-profile">
                                <span class="popover-wrapper right">
                                  <button data-role="popover" data-target="nav-user-profile"><i class="fa fa-caret-down"></i></button>
                                  <div class="popover-modal nav-user-profile">
                                    <div class="popover-body">
                                        <ul>
                                            <li><a href="{{ URL::route('dashboard.settings.index') }}">{{ trans('messages.sidebar.settings')}}</a></li>
                                            <li><a href="/logout">{{ trans('messages.sidebar.logout')}}</a></li>
                                        </ul>
                                    </div>
                                  </div>
                                </span>
                            </div>

                        </div>
                        <div class="nav-right-item notification">
                            <div class="popover-notification">
                                <span class="popover-wrapper right">


                                <!-- NOTIFICATION POPOVER BUTTON -->
                                  <button data-role="popover" data-target="notification_button">
                                    <i class="fa fa-bell-o"></i>
                                    @if( count( $notifications ) )
                                        <span class="notification-count">
                                            {{ $notifications->count() }}
                                        </span>
                                    @endif
                                  </button>


                                <!-- NOTIFICATION POPOVER BOX -->
                                  <div class="popover-modal notification_button">
                                    <div class="popover-body " >
                                        <div class="notif_top">
                                            <span class="title">{{ trans('messages.nav.notifications')}}</span>

                                            @if( count($notifications) ) 
                                                <button class="remove_all_notif " title="Remove all notifications" > {{ trans('messages.nav.clearAll')}} </button>
                                            @endif
    

                                        </div>
                                        <ul class="scrollbar gray-skin">

                                            @if( count( $notifications ) )

                                                @foreach( $notifications as $notification )

                                                    <!-- Single notification  -->
                                                    <li class="notif-item">
                                                        <a href="{!! route('dashboard.single.ticket', [
                                                            'subject' => $notification->ticket->FormatSubject(), 
                                                            'id' => $notification->ticket->id
                                                        ]) !!}">
                                                            <div class="notif-item-l">
                                                                <span class="{{ $notification->type }}">
                                                                    <i class="fa fa-circle"></i>
                                                                </span>
                                                            </div>
                                                            <div class="notif-item-r">
                                                                @if( $notification->type == 'pending' || $notification->type == 'solved'  )
                                                                    <p>
                                                                        <b>#{{ $notification->ticket->id }}</b> 
                                                                        {{ strtolower( $notification->title ) }}
                                                                    </p>
                                                                @else 
                                                                    <p>
                                                                        <b>{{ $notification->user->fullName() }}</b> 
                                                                        {{ strtolower( $notification->title ) }}
                                                                    </p>
                                                                @endif
                                                                <span class="date">
                                                                    {{ $notification->created_at->diffForHumans() }}
                                                                </span>
                                                                <button class="close remove_notif" value="{{ $notification->id }}"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </a>
                                                    </li>

                                                @endforeach
                                            @else 
                                                <li class="notif-item no-notif">
                                                    <div class="notif-item-l">
                                                        <span class="active">
                                                        </span>
                                                    </div>
                                                    <div class="notif-item-r">
                                                        <p>{{ trans('messages.nav.noNotifications')}}</p>
                                                    </div>
                                                </li>
                                            @endif

                                        </ul>
                                    </div>
                                  </div>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>