<script>
    {!! 'var departments='.$departments.';' !!}
    function drawCountryVsSubject(json) {
        $('#barchart').html('');
        var chartType=$('#chart option:selected');
        $('#chartTitle').text(chartType.text());
        var location=$('#location option:selected').text();
        var month=$('#month option:selected').text();

        var xAxisData=[];
        var dataSeries=[];
        var dataZero=[];
        $.each(departments, function(index, value) {
            xAxisData.push(value.name);
            dataZero.push(0);
        });


        $.each(json, function(index, countryData) {
            var showLegend=undefined;
            var marginTop=50;
            var titleY=20;
            if(index=='Katunayake'){
                showLegend=true;
                marginTop=175;
                titleY=130;
            }

            $('<div class="chart">')
                .appendTo('#barchart')
                .highcharts({
                    chart: {
                        marginTop: marginTop,
                        type: 'column',

                    },
                    title: {
                        align: 'left',
                        x: 100,
                        y: titleY,
                        text: '('+index+''+month+')'
                    },
                    xAxis: {
                        categories: xAxisData
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Tickets'
                        }
                    },
                    legend: {
                        floating: true,
                        enabled: showLegend,
                        verticalAlign: 'top',

                        itemWidth: 160,
                        itemStyle: {
                            'font-size': '10px'
                        }
                    },
                    plotOptions: {

                    },
                    series: countryData
                });

        });





    }
$(document).ready(function() {
    $.getJSON("/dashboard/reports/common-chart-data", null, function(data) {

        drawCountryVsSubject(data)
    });

  });
</script>