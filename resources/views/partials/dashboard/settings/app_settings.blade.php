                        <!--====  PROFILE SETTINGS  ====-->

                        <div role="tabpanel" class="tab-pane {{ Auth::user()->hasRole('staff') ? 'active' : '' }}" id="app_settings">
                            {{ Form::model($user, [
                                'route' => 'dashboard.app.settings',
                                'method' => 'post',
                                'id' => 'app_settings_form'
                            ])}}
                                <h3>Mail Settings</h3>
                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAIL_DRIVER') }}
                                        {{ Form::text('MAIL_DRIVER', env('MAIL_DRIVER'), ['class' => 'form-control', 'placeholder' => 'mail']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * To use PHP's default mail server put it as "mail". Then you will  not need to fill below fields. Or you could user these services "smtp", "mailgun", "sendmail", "mandrill", "ses",  and "sparkpost".
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAIL_HOST') }}
                                        {{ Form::text('MAIL_HOST', env('MAIL_HOST'), ['class' => 'form-control', 'placeholder' => 'Mail host']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mail host
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAIL_PORT') }}
                                        {{ Form::text('MAIL_PORT', env('MAIL_PORT'), ['class' => 'form-control', 'placeholder' => 'Mail Port']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mail Port
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAIL_USERNAME') }}
                                        {{ Form::text('MAIL_USERNAME', env('MAIL_USERNAME'), ['class' => 'form-control', 'placeholder' => 'Mail Username']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mail Username
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAIL_PASSWORD') }}
                                        {{ Form::text('MAIL_PASSWORD', env('MAIL_PASSWORD'), ['class' => 'form-control', 'placeholder' => 'Mail Password']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mail Password
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAIL_ENCRYPTION') }}
                                        {{ Form::text('MAIL_ENCRYPTION', env('MAIL_ENCRYPTION'), ['class' => 'form-control', 'placeholder' => 'Mail Encryption']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Encryption
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <h3>MAIL DRIVER KEY</h3>
                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAILGUN_SECRET') }}
                                        {{ Form::text('MAILGUN_SECRET', env('MAILGUN_SECRET'), ['class' => 'form-control', 'placeholder' => 'Mailgun secret']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mailgun secret 
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MAILGUN_DOMAIN') }}
                                        {{ Form::text('MAILGUN_DOMAIN', env('MAILGUN_DOMAIN'), ['class' => 'form-control', 'placeholder' => 'Mailgun domain']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mailgun domain 
                                    </div>
                                </div>
                                <hr>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('MANDRILL_SECRET') }}
                                        {{ Form::text('MANDRILL_SECRET', env('MANDRILL_SECRET'), ['class' => 'form-control', 'placeholder' => 'Mandrill secret']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter Mandrill secret 
                                    </div>
                                </div>
                                <hr>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('SES_KEY') }}
                                        {{ Form::text('SES_KEY', env('SES_KEY'), ['class' => 'form-control', 'placeholder' => 'SES key']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter SES key 
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('SES_SECRET') }}
                                        {{ Form::text('SES_SECRET', env('SES_SECRET'), ['class' => 'form-control', 'placeholder' => 'SES secret']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter SES secret 
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('SES_REGION') }}
                                        {{ Form::text('SES_REGION', env('SES_REGION'), ['class' => 'form-control', 'placeholder' => 'SES region']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter SES region 
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <h3>Google reCaptcha key</h3>
                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('recaptcha', 'reCaptcha') }}
                                        {{ Form::text('recaptcha',  env('G_RECAPTCHA_KEY'), ['class' => 'form-control', 'placeholder' => 'reCaptcha key']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter your google reCaptcha key <br> Create your key from <a href="https://www.google.com/recaptcha">here</a> 
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-status app_form_status"></div>


                                <div class="form-group clearfix submit-btns">
                                    <button type="submit" class="btn btn-success app_settings_btn">Update</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                
                            {{ Form::close() }}
                        </div>