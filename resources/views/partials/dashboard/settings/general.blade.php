@inject('timezones', 'App\Classes\Options')
                        <!--====  GENERAL SETTINGS  ====-->
                        
                        <div role="tabpanel" class="tab-pane {{ Auth::user()->hasRole('admin') ? 'active' : '' }} " id="general">
                            {{ Form::open([
                                'route' => 'dashboard.general.settings',
                                'method' => 'post',
                                'id' => 'general_settings_form',
                            ])}}
                                <div class="form-group clearfix clearfix">
                                    <div class="form-input">
                                        {{ Form::label('company_name') }}
                                        {{ Form::text('company_name', Options::get('title'), ['class' => 'form-control', 'placeholder' => 'Support Pro']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter your company name
                                    </div>
                                </div>
                                <div class="form-group clearfix ">
                                    <div class="form-input">
                                        <label for="company_logo" id="company_logo">Company Logo</label>
                                        <div class="logo_file_type">

                                            <input type="file" name="logo" id="file-1" class="inputfile inputfile-1" />
                                            <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Select your logo&hellip;</span></label>


                                        </div>
                                    </div>
                                    <div class="form-required-lable">
                                       * Change your logo
                                    </div>
                                </div>


                                <div class="form-group clearfix ">
                                    <div class="form-input">
                                        <label for="company_dashboard_logo" id="company_dashboard_logo">Company Dashboard Logo</label>
                                        <div class="logo_file_type">

                                            <input type="file" name="dashboard_logo" id="file-5" class="inputfile inputfile-5" />
                                            <label for="file-5"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Select your logo&hellip;</span></label>


                                        </div>
                                    </div>
                                    <div class="form-required-lable">
                                       * Change Dashboard logo
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        <label for="timezone">Time Zone</label>
                                        <select name="timezone" id="timezone" class="form-control">
                                            
                                            {{-- Get timezones for select element --}}
                                            @foreach( $timezones::getTimezones() as $key => $value )
                                                <option value="{{ $key }}" {{ Options::get('timezone') == $key ? 'selected' : ''}}>{{ $value }}</option> 
                                            @endforeach

                                        </select>
                                    </div>
                                    <div class="form-required-lable">
                                        * Change your Time Zone
                                    </div>
                                </div>

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        <label for="staff_perm">Staff permission</label>
                                        <select name="staff_perm" id="staff_perm" class="form-control">
                                            
                                            <option value="can_view_clients" 
                                                {{ Options::get('staff_permission') == 'can_view_clients' ? 'selected' : '' }} 
                                            >Can view clients</option>

                                            <option value="can_view_and_delete_clients" 
                                                {{ Options::get('staff_permission') == 'can_view_and_delete_clients' ? 'selected' : '' }} 
                                            >Can view and delete clients</option>

                                            <option value="none" 
                                                {{ Options::get('staff_permission') == 'none' ? 'selected' : '' }} 
                                            >None</option>

                                        </select>
                                    </div>
                                    <div class="form-required-lable">
                                        * Change your Time Zone
                                    </div>
                                </div>

                                <div class="form-group clearfix sp_check">
                                    <input 
                                        type="checkbox" 
                                        name="email_notification" 
                                        id="email_notification"
                                        {{ Options::get('email_notification') == 'on' ? 'checked' : '' }}
                                    >
                                    
                                    <label for="email_notification"> <span></span> Send email notification when someone opens a ticket</label>
                                </div>
                            <div class="form-group clearfix sp_check">
                                <input
                                        type="checkbox"
                                        name="email_notification_to_department"
                                        id="email_notification_to_department"
                                        {{ Options::get('email_notification_to_department') == 'on' ? 'checked' : '' }}
                                >

                                <label for="email_notification_to_department"> <span></span> Send email notification to department staff</label>
                            </div>

                                <div class="form-group clearfix sp_check">
                                    <input 
                                        type="checkbox" 
                                        name="edit_ticket" 
                                        id="edit_ticket"
                                        {{ Options::get('edit_ticket') == 'on' ? 'checked' : '' }}
                                    >
                                    
                                    <label for="edit_ticket"> <span></span> Allow clients to edit tickets.</label>
                                </div>

                                <div class="form-group clearfix sp_check">
                                    <input 
                                        type="checkbox" 
                                        name="user_register" 
                                        id="user_register"
                                        {{ Options::get('user_registration') == 'on' ? 'checked' : '' }}
                                    >
                                    
                                    <label for="user_register"> <span></span> User can register?</label>
                                </div>

                                <div class="form-status"></div>

                                <div class="form-group clearfix submit-btns">
                                    <button type="submit" class="btn btn-success general_settings_btn">Update</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                            </form>
                        </div>