                        <!--====  THEME SETTINGS  ====-->

                        <div role="tabpanel" class="tab-pane" id="theme">

                            {{ Form::open([
                                'route'     =>  'dashboard.theme.update',
                                'method'    =>  'post',
                                'id'        =>  'theme_update_form',
                            ]) }}

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        <label for="page_style">Select Support Page Style</label>
                                        <select name="style" id="page_style" class="form-control">
                                            <option 
                                                value="homepage_style_1"
                                                {{ $style->value == 'homepage_style_1' ? 'selected' : ''}}
                                            >
                                                Homepage Style-1
                                            </option>
                                            <option 
                                                value="homepage_style_2"
                                                {{ $style->value == 'homepage_style_2' ? 'selected' : ''}}
                                            >
                                                Homepage Style-2
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-required-lable">
                                        
                                    </div>
                                </div>
                                <!-- form-group ends -->
    

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        <label for="extra_css">Extra CSS</label>
                                        <textarea name="extra_css" id="extra_css" rows="6" class="form-control">{{ $extra_css->value }}</textarea>
                                    </div>
                                    <div class="form-required-lable">
                                        * Add style if you want to customize by yourself
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-status"></div>

                                <div class="form-group clearfix submit-btns">
                                    <button type="submit" class="btn btn-success update_theme_btn">Update</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                            

                            {{ Form::close() }}
                        </div>