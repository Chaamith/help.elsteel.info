                        <!--====  PROFILE SETTINGS  ====-->

                        <div role="tabpanel" class="tab-pane {{ Auth::user()->hasRole('staff') ? 'active' : '' }}" id="settings">
                            {{ Form::model($user, [
                                'route' => 'profile.update',
                                'method' => 'post',
                                'id' => 'profile_update_form'
                            ])}}
                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('first_name') }}
                                        {{ Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'John']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter your first name
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('last_name') }}
                                        {{ Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'John']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter your last name
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix ">
                                    <div class="form-input">
                                        <label for="profile_img" id="profile_img">Profile Image</label>
                                        <div class="logo_file_type">

                                            <input type="file" name="file" id="file-2" class="inputfile inputfile-2 input_profile"/>
                                            <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Select your picture&hellip;</span></label>


                                        </div>
                                    </div>
                                    <div class="form-required-lable">
                                       * Change your logo
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('email') }}
                                        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'John@company.com']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter your email
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-group clearfix">
                                    <div class="form-input">
                                        {{ Form::label('password') }}
                                        {{ Form::password('password', ['class' => 'form-control']) }}
                                    </div>
                                    <div class="form-required-lable">
                                        * Enter your password
                                    </div>
                                </div>
                                <!-- form-group ends -->

                                <div class="form-status profile_form_status"></div>


                                <div class="form-group clearfix submit-btns">
                                    <button type="submit" class="btn btn-success profile_update_btn">Save</button>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                </div>
                                
                            {{ Form::close() }}
                        </div>