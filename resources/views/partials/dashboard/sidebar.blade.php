@inject('active', 'App\Http\Utilities\Active')

<!--=====================================
            =            MAIN SIDEBAR STARTS            =
            ======================================-->
<aside class="main-sidebar">
    <section class="sidebar">
        {{--<div class="menu-label">--}}
        {{--<p>{{ trans('messages.sidebar.mainMenu')}}</p>--}}
        {{--</div>--}}

        <ul class="sidebar-menu">
            <li>
                <a href="{{ URL('/') }}">

                    <span class="title">{{ trans('Create Ticket')}}</span>
                </a>
            </li>
            <li class="{{ $active::set(['dashboard']) }}">
                <a href="{{ route('dashboard.index') }}">
                    <img src="{{ URL::asset('assets/img/icons/icon-dashboard.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.dashboard')}}</span>
                </a>
            </li>
            <li class="has-dropdown {{ $active::set(['dashboard/tickets*']) }}">
                <a href="">
                    <img src="{{ URL::asset('assets/img/icons/icon-file.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.tickets')}}</span>

                    @if( $new_ticket_count !== 0)
                        <span class="menu-label label-new">{{ $new_ticket_count }}</span>
                    @endif
                    <i class="arrow-icon fa fa-angle-down"></i>
                </a>
                <div class="sidebar-dropdown-menu">
                    <ul>
                        <li><a href="{{ route('dashboard.tickets', 'my')}}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.mytickets')}}
                                @if( $my_ticket_count !== 0)
                                    <span class="menu-label label-all">{{ $my_ticket_count }}</span>
                                @endif
                            </a></li>
                        <li><a href="{{ route('dashboard.tickets.home')}}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.allTickets')}}
                                @if( $all_ticket_count !== 0)
                                    <span class="menu-label label-all">{{ $all_ticket_count }}</span>
                                @endif
                            </a></li>
                        <li><a href="{{ route('dashboard.tickets.home',['status'=>'new','title'=>'New Tickets']) }}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.newTickets')}}
                                @if( $new_ticket_count !== 0)
                                    <span class="menu-label label-new">{{ $new_ticket_count }}</span>
                                @endif
                            </a></li>
                        <li><a href="{{ route('dashboard.tickets.home',['status'=>'pending','title'=>'Pending Tickets']) }}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.pendingTickets')}}
                                @if( $pending_ticket_count !== 0)
                                    <span class="menu-label label-pending">{{ $pending_ticket_count }}</span>
                                @endif
                            </a></li>
                        <li><a href="{{ route('dashboard.tickets.home',['status'=>'solved','title'=>'Solved Tickets']) }}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.solvedTickets')}}
                                @if( $solved_ticket_count !== 0)
                                    <span class="menu-label label-solved">{{ $solved_ticket_count }}</span>
                                @endif
                            </a></li>
                    </ul>
                </div>

            </li>
            <li class="has-dropdown {{ $active::set(['dashboard/reports*']) }}">
                <a href="">
                    <img src="{{ URL::asset('assets/img/icons/icon-file.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.reports')}}</span>


                    <i class="arrow-icon fa fa-angle-down"></i>
                </a>

                <div class="sidebar-dropdown-menu">
                    <ul>
                        <li><a href="{{ route('dashboard.reports.mystats') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.mystats')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.reports.locationvsdeptkt') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.depvstkt')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.reports.clientvstkt') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.clientvstkt')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.reports.uservstkt') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.uservstkt')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.reports.locationvstkt') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.locationvstkt')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.tickets.report') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ 'Time Spent' }}
                            </a></li>
                        <li><a href="{{ route('dashboard.reports.commonreport') }}" >
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('common report')}}
                            </a></li>
                    </ul>
                </div>
            </li>
            <li class="has-dropdown {{ $active::set(['dashboard/staffs*']) }}">
                <a href="">
                    <img src="{{ URL::asset('assets/img/icons/icon-profile.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.staffs')}}</span>

                    @if( $new_staffs_count !== 0)
                        <span class="menu-label label-new">{{ $new_staffs_count }}</span>
                    @endif

                    <i class="arrow-icon fa fa-angle-down"></i>
                </a>

                <div class="sidebar-dropdown-menu">
                    <ul>
                        <li><a href="{{ route('dashboard.staffs.all') }}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.allStaffs')}}

                                @if( $all_staffs_count !== 0)
                                    <span class="menu-label label-all">{{ $all_staffs_count }}</span>
                                @endif
                            </a></li>
                        <li><a href="javascript:void(0)" class="md-trigger hidden-xs" data-modal="newStaff">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.newStaffs')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.staffs.create') }}" class="visible-xs">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.newStaffs')}}
                            </a></li>
                    </ul>
                </div>
            </li>
            <li class="has-dropdown {{ $active::set(['dashboard/clients*']) }}">
                <a href="">
                    <img src="{{ URL::asset('assets/img/icons/icon-clients.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.clients')}}</span>

                    @if( $new_clients_count !== 0)
                        <span class="menu-label label-new">{{ $new_clients_count }}</span>
                    @endif


                    <i class="arrow-icon fa fa-angle-down"></i>

                </a>

                <div class="sidebar-dropdown-menu">
                    <ul>

                        {{-- If the user is a Admin or a staff member has those permission, then only they can see those --}}
                        @if(
                            Auth::user()->hasRole('admin') ||
                            Options::get('staff_permission') == 'can_view_and_delete_clients' ||
                            Options::get('staff_permission') == 'can_view_clients'
                        )
                            <li><a href="{{ route('dashboard.clients.all') }}">
                                    <i class="fa fa-circle-thin"></i>
                                    {{ trans('messages.sidebar.allclients')}}

                                    @if( $all_clients_count !== 0)
                                        <span class="menu-label label-all">{{ $all_clients_count }}</span>
                                    @endif

                                </a></li>

                        @endif
                        <li><a href="javascript:void(0)" class="md-trigger" data-modal="newClient">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.newclients')}}
                            </a></li>
                    </ul>
                </div>

            </li>
            <li class=" {{ $active::set(['dashboard/settings*']) }} ">
                <a href="{{ route('dashboard.settings.index') }}">
                    <img src="{{ URL::asset('assets/img/icons/icon-settings.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.settings')}}</span>
                </a>
            </li>
            {{-- For admin messages --}}
            <li class="has-dropdown {{ $active::set(['dashboard/all-messages*']) }}">
                <a href="">
                    <img src="{{ URL::asset('assets/img/icons/icon-file.png') }}" alt="" class="menu-icon">
                    <span class="title">{{ trans('messages.sidebar.adminMessage')}}</span>

                    @if( $new_messages_count !== 0)
                        <span class="menu-label label-new">{{ $new_messages_count }}</span>
                    @endif

                    <i class="arrow-icon fa fa-angle-down"></i>
                </a>

                <div class="sidebar-dropdown-menu">
                    <ul>
                        <li><a href="{{ route('dashboard.all.messages') }}">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.allMessage')}}

                                @if( $new_messages_count !== 0)
                                    <span class="menu-label label-all">{{ $new_messages_count }}</span>
                                @endif
                            </a></li>
                        <li><a href="javascript:void(0)" class="md-trigger hidden-xs" data-modal="newMsg">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.newMessage')}}
                            </a></li>
                        <li><a href="{{ route('dashboard.staffs.create') }}" class="visible-xs">
                                <i class="fa fa-circle-thin"></i>
                                {{ trans('messages.sidebar.newMessage')}}
                            </a></li>
                    </ul>
                </div>
            </li>
            {{-- For admin messages --}}
            <li class="has-dropdown {{ $active::set(['dashboard/server*']) }}">
                <a href="">

                    <span class="title"><i class="fa fa-server"></i> &nbsp Data Center</span>


                    <i class="arrow-icon fa fa-angle-down"></i>
                </a>

                <div class="sidebar-dropdown-menu">
                    <ul>
                        <li><a href="{{ route('dashboard.server.all') }}">
                                <i class="fa fa-circle-thin"></i>
                                Server Status

                            </a></li>
                        <li><a href="{{ route('dashboard.server.create') }}" class="md-trigger">
                                <i class="fa fa-circle-thin"></i>
                               New Server Status
                            </a></li>
                        <li><a href="{{ route('dashboard.server.report') }}" class="md-trigger">
                                <i class="fa fa-circle-thin"></i>
                              Report
                            </a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </section>
</aside>
<!--====  End of SIDEBAR  ====-->