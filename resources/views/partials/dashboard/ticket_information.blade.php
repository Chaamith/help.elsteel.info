<!-- TICKET INFORMATAION -->
<div class="col-lg-4 col-md-12 ticket-information">
    <div class="ticket-box">
        <div class="ticket-header">
            <h4>Ticket information</h4>
        </div>
        <div class="ticket-info-body">
            <table class="table table-hover">
                <tr>
                    <th> {{ trans('messages.singleTicket.ticketID') }}</th>
                    <td>#{{ $ticket->id }}</td>
                </tr>
                <tr>
                    <th>{{ trans('messages.singleTicket.client') }}:</th>
                    <td>{{ $ticket->user->fullName() }}</td>
                </tr>
                <tr>
                    <th>{{ trans('messages.singleTicket.supportType') }}</th>
                    <td>{{ $ticket->getDepartment() }}</td>
                </tr>
                <tr>
                    <th>{{ trans('messages.singleTicket.ticketStatus') }}</th>
                    <td class="template-ticket-status"><span class="status-{{ $ticket->status }} ">{{ ucwords($ticket->status) }}</span></td>
                </tr>
                <tr>
                    <th>{{ trans('messages.singleTicket.assignedTicket') }}</th>
                    <td  class=" ticket-info-assigned-staff"> {{ $ticket->getAssignedStaff() }} </td>
                </tr>
                @if( $ticket->solved_by )
                    <tr class="solved_by_staff">
                        <th>{{ trans('messages.singleTicket.solvedBy') }}</th>
                        <td  class=" ticket-info-solved-staff"> {{ $ticket->getSolvedBy() }} </td>
                    </tr>
                @endif
                <tr>
                    <th>Created At</th>
                    <td  class=" ticket-info-assigned-staff"> {{ $ticket->created_at }} </td>
                </tr>
                <tr>
                    <th>Updated At</th>
                    <td  class=" ticket-info-assigned-staff"> {{ $ticket->updated_at }} </td>
                </tr>
            </table>
        </div>
        <div class="ticket-info-footer">
            <div class="ticket-info-buttons">
                <div class="as-n-s">
                    <button data-modal="modal-5" class="btn btn-info assign-new-trigger">{{ trans('messages.singleTicket.assignNewTicket') }}</button>
                    
                    <div class="assign-staff-up-box up-info-box ">
                        <h3>{{ trans('messages.singleTicket.assignThisTicket') }}</h3>
                        <span class="up-box-cancel"><i class="fa fa-times"></i></span>
                        
                        <span class="up-box-status assign_upbox"><i class="fa fa-circle-o-notch status-success"></i></span>

                        <!-- ASSIGN TICKET POPUP TO ASSIGN THIS TICKET TO STAFF -->
                        <div class="box-form-outer scrollbar">
                            {{ Form::open([
                                'route' => ['ticket.assign.user', $ticket->id], 
                                'method' => 'post', 
                                'id' => 'ticket_assign_user'
                            ]) }}

                                @if( count($staffs) )

                                    @foreach($staffs as $staff)

                                        <!-- EACH STAFF IN THE LIST -->
                                        <div class="media assign-user-media">
                                            <label for="assign_user_{{ $staff->id }}">

                                              <div class="media-left">
                                                  <img class="media-object" src="{{ $staff->profileImg(40) }}" alt="Pic">
                                              </div>
                                              <div class="media-body">
                                                <h4 class="media-heading">{{ $staff->fullName() }}</h4>
                                                <p>{{ $staff->role_name }}</p>
                                                <div>
                                                    <input 
                                                        type="radio" 
                                                        name="assign_user" 
                                                        class="assign_user" 
                                                        value="{{ $staff->id }}" 
                                                        id="assign_user_{{ $staff->id }}"
                                                        {{ $staff->fullName() == $ticket->getAssignedStaff() ? 'checked' : '' }}
                                                    >
                                                    <span class="input_radio"><i class="fa fa-check"></i></span>
                                                </div>
                                              </div>
                                            </label>
                                        </div>
                                    @endforeach

                                @else
                                    <div class="media">
                                        <label for="assign_user_none">

                                          <div class="media-body">
                                            <h4 class="media-heading">{{ trans('messages.singleTicket.noStaffsFound') }}</h4>
                                          </div>

                                        </label>
                                    </div>
                                @endif


                            </form>
                        </div>
                    </div>
                </div>

                <div class="as-n-u">
                    <button class="btn btn-success update_status_trigger">{{ trans('messages.singleTicket.updateStatus') }}</button>                                            

                    <div class="up-info-box update-status">
                        <h3>{{ trans('messages.singleTicket.updateTicketStatus') }}</h3>
                        <span class="up-box-cancel"><i class="fa fa-times"></i></span>
                        <span class="up-box-status statusUpBx"><i class="fa fa-circle-o-notch fa-spin status-success"></i></span>

                        <!-- UPDATE TICKET STATUS POPUP -->
                        <div class="box-form-outer">                                                    
                            {{ Form::open([
                                'route' => ['update.ticket.status', $ticket->id],
                                'method' => 'post',
                                'id' => 'ticket_assign_form'
                            ])}}

                                <!-- UPDATE STATUS LIST -->
                                <div class="media">
                                    <label for="ticket_status_solved">

                                      <div class="media-body">
                                        <h4 class="media-heading">{{ trans('messages.singleTicket.solved') }}</h4>
                                        <div>
                                            <input 
                                                type="radio" 
                                                name="ticket_status" 
                                                class="ticket_status {{ $ticket->status == 'solved' ? 'checked' : '' }} " 
                                                value="solved" 
                                                id="ticket_status_solved"
                                                {{ $ticket->status == 'solved' ? 'checked' : '' }}
                                            >
                                            <span class="input_radio"></span>
                                        </div>
                                      </div>
                                    </label>
                                </div>

                                    <!-- UPDATE STATUS LIST -->
                                <div class="media">
                                    <label for="ticket_status_pending">

                                      <div class="media-body">
                                        <h4 class="media-heading">{{ trans('messages.singleTicket.pending') }}</h4>
                                        <div>
                                            <input 
                                                type="radio" 
                                                name="ticket_status" 
                                                class="ticket_status {{ $ticket->status == 'pending' ? 'checked' : '' }}" 
                                                value="pending" 
                                                id="ticket_status_pending"
                                                {{ $ticket->status == 'pending' ? 'checked' : '' }}
                                            >
                                            <span class="input_radio ticket_pending"></span>
                                        </div>
                                      </div>
                                    </label>
                                </div>

                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>