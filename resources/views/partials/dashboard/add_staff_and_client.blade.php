<!-- New Client   -->

<div class="md-modal md-effect-10 hidden-xs" id="newClient">
    <div class="md-content settings-tab new-staff-modal">

        <h4> {{ trans('messages.addNewClient.title') }} </h4>

        {{ Form::open(['route' => 'dashboard.clients.create', 'method' => 'post', 'id' => 'add_client_form']) }}

        <div class="form-group location_group">
            {{ Form::label(trans('messages.settings.location')) }}
            {{ Form::select('location', config('settings.location'), null, ['class' => 'form-control']) }}
            <span class="help-block"></span>
        </div>

        <div class="form-group first_name_group">
            {{ Form::label(trans('messages.addNewClient.first_name')) }}
            {{ Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => trans('messages.addNewClient.placeholders.firstName')])}}
        </div>

        <div class="form-group last_name_group">
            {{ Form::label(trans('messages.addNewClient.last_name')) }}
            {{ Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => trans('messages.addNewClient.placeholders.lastName')])}}
        </div>

        <div class="form-group email_group">
            {{ Form::label(trans('messages.addNewClient.email')) }}
            {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => trans('messages.addNewClient.placeholders.email')])}}
        </div>


        <div class="form-group password_group">
            {{ Form::label(trans('messages.addNewClient.password')) }}
            {{ Form::password('password', ['class' => 'form-control'])}}
        </div>

        <div class="form-group client_img_g">
            <label for="client_img"> {{ trans('messages.addNewClient.clientImage') }}</label>
            <input type="file" name="client_img" id="file-2" class="inputfile inputfile-2"
                   data-multiple-caption="{count} files selected" multiple/>
            <label for="file-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                </svg>
                <span> {{ trans('messages.addNewClient.placeholders.file') }} </span></label>
        </div>

        <div class="form-status client_form_status"></div>


        <div class="form-group  submit-btns">
            <button type="submit"
                    class="btn btn-success add_client_btn"> {{ trans('messages.addNewClient.addClientBtn') }} </button>
        </div>

        {{ Form::close() }}

        <button class="md-close"><i class="fa fa-times"></i></button>

    </div>
</div>

<!-- New Message   -->

<div class="md-modal md-effect-10 hidden-xs" id="newMsg">
    <div class="md-content settings-tab new-staff-modal">

        <h4> {{ trans('messages.addNewMsg.title') }} </h4>

        {{ Form::open(['route' => 'dashboard.message.create', 'method' => 'post', 'id' => 'add_message_form']) }}

        <div class="form-group message_group">
            {{ Form::label(trans('messages.addNewMsg.message')) }}
            {{ Form::text('message', null, ['id' => 'message','class' => 'form-control', 'placeholder' => trans('messages.addNewMsg.placeholders.message')])}}
        </div>

        <div class="form-group messages_status_group">
            {{ Form::label('messages_status', trans('messages.addNewMsg.status')) }}
            {{ Form::select('status', $messages_status, null, ['id'=>'status','class' => 'form-control messages_status'])}}
        </div>


        <div class="form-status message_form_status"></div>


        <div class="form-group  submit-btns">
            <button type="submit"
                    class="btn btn-success add_client_btn"> {{ trans('messages.addNewMsg.addMsgBtn') }} </button>
        </div>

        {{ Form::close() }}

        <button class="md-close"><i class="fa fa-times"></i></button>

    </div>
</div>
<!-- New staff overlay -->

<div class="md-modal md-effect-10  hidden-xs" id="newStaff">
    <div class="md-content settings-tab new-staff-modal">

        <h4>{{ trans('messages.addNewStaff.title') }}</h4>

        {{ Form::open(['route' => 'dashboard.staffs.create', 'method' => 'post', 'id' => 'add_staffs_form']) }}

        <div class="form-group location-group">
            {{ Form::label(trans('messages.settings.location')) }}
            {{ Form::select('location', config('settings.location'), null, ['class' => 'form-control']) }}
            <span class="help-block"></span>
        </div>
        <div class="form-group staff_first_name_group">
            {{ Form::label( trans('messages.addNewStaff.firstName') ) }}
            {{ Form::text('first_name', null, ['class' => 'form-control staff_first_name', 'placeholder' => trans('messages.addNewStaff.placeholders.first_name')])}}
        </div>
        <div class="form-group last_name_group">
            {{ Form::label(trans('messages.addNewClient.last_name')) }}
            {{ Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => trans('messages.addNewClient.placeholders.lastName')])}}
        </div>
        <div class="form-group staff_email_group">
            {{ Form::label( trans('messages.addNewStaff.email') ) }}
            {{ Form::email('email', null, ['class' => 'form-control staff_email', 'placeholder' => trans('messages.addNewStaff.placeholders.email')])}}
        </div>


        <div class="form-group staff_password_group">
            {{ Form::label( trans('messages.addNewStaff.password') ) }}
            {{ Form::password('password', ['class' => 'form-control staff_password'])}}
        </div>

        <div class="form-group staff_img_g">
            <label for="staff_img"> {{trans('messages.addNewStaff.staffImage') }}</label>
            <input type="file" name="staff_img" id="file-3" class="inputfile inputfile-3"
                   data-multiple-caption="{count} files selected" multiple/>
            <label for="file-3">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
                    <path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
                </svg>
                <span>
                        {{ trans('messages.addNewStaff.placeholders.file') }}</span></label>
        </div>

        <div class="form-group staff_department_group">
            {{ Form::label('department_id', trans('messages.addNewStaff.department')) }}
            {{ Form::select('department_id', $departments, null, ['class' => 'form-control staff_department'])}}
        </div>

        <div class="form-group role_group">
            {{ Form::label( trans('messages.addNewStaff.roleName') )}}
            {{ Form::text('role_name', null, ['class' => 'form-control staff_role', 'placeholder' => trans('messages.addNewStaff.placeholders.role_name')])}}
        </div>


        <div class="form-status staff_form_status"></div>


        <div class="form-group  submit-btns">
            <button type="submit" class="btn btn-success">{{ trans('messages.addNewStaff.addStaffBtn') }}</button>
        </div>
        {{ Form::close() }}


        <button class="md-close"><i class="fa fa-times"></i></button>

    </div>
</div>