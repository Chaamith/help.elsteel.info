<div class="ticket-form">
    {{ Form::open([
        'route' => 'create.lead',
        'method' => 'post', 
        'id' => 'createLead',
        'enctype' => 'multipart/form-data'
    ])}}

    <div class="row">

        <div class="col-md-12">
            <div class="form-group">

                <label for="company" class="control-label">Company name </label>
                <input type="text"
                       class="form-control"
                       name="company"
                       id="company"
                       placeholder="Company name" required>
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="contact1" class="control-label">Contact person 1 </label>
                <input type="text"
                       class="form-control"
                       name="contact1"
                       id="contact1"
                       placeholder="Contact person 1">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="contact2" class="control-label">Contact person 2 </label>
                <input type="text"
                       class="form-control"
                       name="contact2"
                       id="contact2"
                       placeholder="Contact person 2">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="phone" class="control-label">Phone </label>
                <input type="text"
                       class="form-control"
                       name="phone"
                       id="phone"
                       placeholder="Phone">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="fax" class="control-label">Fax </label>
                <input type="text"
                       class="form-control"
                       name="fax"
                       id="fax"
                       placeholder="Fax">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="email" class="control-label">E-mail </label>
                <input type="text"
                       class="form-control"
                       name="email"
                       id="email"
                       placeholder="E-mail">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="branch_types" class="control-label">Branch</label>
                <select name="branch_types[]"
                        id="branch_types"
                        class="form-control select2" style="width:100%" data-minimum-results-for-search="Infinity"
                        multiple style="width: 100%">
                    <option></option>
                    @foreach ($branches as $branch)
                        <option value="{{ $branch->id }}">{{ $branch->name }}</option>
                    @endforeach
                </select>

            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="business" class="control-label">Type of business</label>
                <input type="text"
                       class="form-control"
                       name="business"
                       id="business"
                       placeholder="Type of business">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="turnover" class="control-label">Turnover</label>
                <input type="text"
                       class="form-control"
                       name="turnover"
                       id="turnover"
                       placeholder="Turnover">
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="employees" class="control-label">Number of employees</label>
                <select name="employees"
                        id="employees"
                        class="form-control select2" style="width:100%" data-minimum-results-for-search="Infinity">
                    <option></option>
                    @foreach ($employees as $employee)
                        <option value="{{ $employee->id }}">{{ $employee->name }}</option>
                    @endforeach
                </select>

            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="notes" class="control-label">Notes</label>
                <textarea class="form-control"
                          rows="2"
                          name="notes"
                          id="notes"
                          placeholder="Notes"></textarea>
            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">
                <div class="form_row hm_p_f_r">
                    <div class="form_half half_left ">
                        <div class="ticket-submit-file">
                            <input type="file" name="file" id="file-2" class="inputfile" data-multiple-caption="{count} files selected" />
                            <label for="file-2"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span> {!! trans('messages.homepage.placeholders.file') !!} </span></label>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-12" align="center">
            <hr>
            <label class="control-label">During the fair</label>
        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="catalogue_during_the_fair" class="control-label">Catalogue</label>
                <select name="catalogue_during_the_fair"
                        id="catalogue_during_the_fair"
                        class="form-control select2" style="width:100%" data-minimum-results-for-search="Infinity">
                    <option></option>
                    @foreach ($catalogues as $catalogue)
                        <option value="{{ $catalogue->id }}">{{ $catalogue->name }}</option>
                    @endforeach
                </select>

            </div>

        </div>

        <div class="col-md-12" align="center">
            <label class="control-label">After the fair</label>
        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="catalogue_after_the_fair" class="control-label">Catalogue</label>
                <select name="catalogue_after_the_fair"
                        id="catalogue_after_the_fair"
                        class="form-control select2" style="width:100%" data-minimum-results-for-search="Infinity">
                    <option></option>
                    @foreach ($catalogues as $catalogue)
                        <option value="{{ $catalogue->id }}">{{ $catalogue->name }}</option>
                    @endforeach
                </select>

            </div>

        </div>

        <div class="col-md-12">
            <div class="form-group">

                <label for="future_action" class="control-label">Future Action</label>
                <textarea class="form-control"
                          rows="2"
                          name="future_action"
                          id="future_action"
                          placeholder="Future Action"></textarea>
            </div>

        </div>

    </div>


</div>
