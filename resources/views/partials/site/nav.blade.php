        <div class="container">
            <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::to('/') }}">
                    <span>
                        <img src="{{ URL::asset('assets/img/' . Options::getLogo() ) }}" alt="">
                    </span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
              <ul class="nav navbar-nav navbar-right">
                @if( Auth::check() )
                    <li><a href="{{ URL::to('/') }}"> Create Lead </a></li>
                    <li><a href="{{ URL::route('all.leads') }}"> All Leads </a></li>
                    <li><a href="{{ route('proifle.settings') }}"> {{ trans('nav.settings') }} </a></li>
                    <li><a href="{{ URL::to('logout') }}">
                        {{ trans('nav.logout') }}
                        ({{ Auth::user()->first_name }})</a></li>
                @else
                    <li><a href="{{ URL::to('/login') }}"> {{ trans('nav.login') }} </a></li>
                    
                    {{-- Check if site registration is turned on or not --}}
                    @if( Options::get('user_registration') == 'on' )
                        <li><a href="{{ URL::to('/register') }}"> {{ trans('nav.register') }} </a></li>
                    @endif

                    <li><a href="{{ URL::to('/dashboard') }}"> {{ trans('nav.go_to_admin') }} </a></li>
                @endif
              </ul>
            </div><!-- /.navbar-collapse -->
            </nav>
        </div><!-- /.container -->