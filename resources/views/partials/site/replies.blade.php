
                                <!-- TICKET REPLIES -->
                                <div class="ticket-replies">
                                    <div class="all-replies">
                                        <div class="reply-container">

                                            @foreach( $replies as $reply)

                                                <!-- SINGLE REPLY -->
                                                <div id="reply-{{$reply->id}}" class="single-reply ">
                                                    <div class="media">
                                                      <div class="media-left reply-user-img" >
                                                          <img class="media-object" src="{{ $reply->user->profileImg(50) }}" alt="...">
                                                      </div>
                                                      <div class="media-body reply-user-details">
                                                        <h4 class="media-heading">
                                                             {{ $reply->user->fullName() }}
@if($reply->user_id==\Auth::user()->id)
                                                             <button class="remove-reply pull-right" onclick="removeReply(this);" value="{{ $reply->id }}"><i class="fa fa-times"></i></button>
 @endif
                                                        </h4>
                                                        <div class="row">
                                                            <p class="pull-left user-role">{{ $reply->user->role_name }} </p>
                                                            <?php $date = $reply->created_at; ?>
                                                            <p class="pull-right reply-date">
                                                                {{ $date->diffInMonths(Carbon::now()) >= 1 ? $date->format('j M Y, g:ia') : $date->diffForHumans() }}
                                                            </p>
                                                        </div>
                                                        <div class="reply-message">
                                                            {!! $reply->body !!}
                                                        </div>

                                                            @if( $reply->file )
                                                                <div class="file-attached">
                                                                    <div class="file-single">
                                                                        <div class="file-t">
                                                                            <span>Attached File:</span>
                                                                        </div>
                                                                        <div class="file-name">
                                                                            <span>
                                                                                <a href="/uploads/{{ $reply->file }}"> {{ $reply->file }} </a>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>

                                        {{ $replies->render() }}
                                    </div>


                                    <!-- REPLY FORM -->
                                    <div class="reply-form">

                                            {{ Form::open([
                                                'route' => ['create.reply', $ticket->id], 
                                                'method' => 'post', 
                                                'id' => 'createReply', 
                                                'enctype' => 'multipart/form-data'
                                            ])}}

                                            <div class="form-group reply-body">
                                                
                                                <label for="reply-textarea"> {{ trans('messages.replies.label') }} </label>

                                                {{ Form::textarea('body', null, ['class' => 'form-control message', 'id' => 'reply-textarea', 'wrap' => 'hard']) }}

                                                <!-- <input type="file" name="reply-attach-file" id="reply-attach-file"> -->
                                                <input type="file" name="file" id="file-1" class="inputfile replyfile inputfile-1" />
                                                <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>  {{ trans('messages.replies.attachFIle') }}  <span></span></label>

                                            </div>

                                            <div class="form-status reply_form_status"></div>

                                            <input type="submit" onclick="tinyMCE.triggerSave(true,true);" value="{{ trans('messages.replies.sendReply') }}" class="btn btn-success">
                                        </form>
                                    </div>
                                </div>