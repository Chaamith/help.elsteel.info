<div class="ticket-form">
    {{ Form::open([
        'route' => 'create.ticket', 
        'method' => 'post', 
        'id' => 'createTicket', 
        'enctype' => 'multipart/form-data'
    ])}}


<div class="row">
    <div class="col-md-2">
        <div class="form-group location-group">

            <select name="location" id="location" class="form-control" >

                @foreach(config('settings.location') as $k=>$location)
                    @if(Auth::user()->location==$k)
                    <option value="{{ $k }}" selected > {{ $location}} </option>
                    @else
                        <option value="{{ $k }}" > {{ $location}} </option>
                     @endif
                @endforeach
            </select>
            <span class="help-block"></span>

        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group department-group">

            <select name="department_id" id="department_id" class="form-control department" >
                <option disabled  selected value> {{ trans('messages.homepage.placeholders.department') }} </option>
                @foreach($departments as $department)
                    <option value="{{ $department->id }}"> {{ $department->name }} </option>
                @endforeach
            </select>
            <span class="help-block"></span>

        </div>
    </div>
    <div class="col-md-5">
    <div class="form-group subject-group">

        <select name="subject" id="subject_id" class="form-control subject" >
            <option disabled  selected value> {{ trans('messages.homepage.placeholders.subject') }} </option>
            @foreach($subjects as $subject)
                <option value="{{ $subject->name }}"> {{ $subject->name }} </option>
            @endforeach
        </select>
        <span class="help-block"></span>

    </div>
    </div>
</div>
        <div class="form-group message-group">

            {{ Form::textarea('message', null, ['spellcheck'=>true,'class' => 'form-control message', 'placeholder' =>trans('messages.homepage.placeholders.message'), 'wrap' => 'hard', 'onkeyup' => 'autoGrow(this);' ]) }}
            <span class="help-block"></span>

        </div>


