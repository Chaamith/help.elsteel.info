<script>
        // Tiny MCE Editor for the Custom Textarea,
        tinymce.init({
            selector: 'textarea',
            height: 220,
            menubar: false,
            paste_auto_cleanup_on_paste : true,
            paste_remove_styles: true,
            fullpage_default_fontsize: "18px",
            paste_as_text: true,
            paste_remove_styles_if_webkit: true,
            paste_strip_class_attributes: "all",
            plugins : 'paste advlist autolink  autoresize link image lists charmap print preview',
            toolbar: false,
            content_css: siteUrl + '/assets/css/tinymce1.site.css',
            font_formats: 'proxima_nova_regular',
            browser_spellcheck: true,
            setup: function (editor) {
                editor.addButton('pasteCode', {
                text: 'Paste code',
                icon: false,
                onclick: function () {
                    editor.insertContent('<pre><code>"paste your code inside the quotation, then remove the quotation marks."</code></pre>');
                }
                });
            },
        });

    </script>