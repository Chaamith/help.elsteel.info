<div class="col-md-5">
    <!-- Common answer start -->
    <div class="common-answers">
        <div class="heading-text">
            <h2>Try to find some common answers:</h2>
            <p>Here you will find some common question that can help you!</p>
        </div>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title collapsed"  role="button" data-toggle="collapse" data-parent="#accordion" href="#answerOne" aria-expanded="true" aria-controls="answerOne">
                  <i class="fa fa-angle-right"></i> Duis autem vel eum iriure dolor in hendrerit in vulputate velit?
              </h4>
            </div>
            <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eaque minima exercitationem magni, odit magnam laudantium neque harum autem voluptas? Dolorum, est dolorem aperiam! Rerum pariatur, qui molestias vitae accusamus iste!
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#answerTwo" aria-expanded="false" aria-controls="answerTwo">
                   <i class="fa fa-angle-right"></i> <span>Esum dolor sit amet, consectetuer adipiscing elit, sed diam?</span>
              </h4>
            </div>
            <div id="answerTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem minus, quasi accusantium ratione totam delectus expedita quaerat hic vero quae voluptate placeat sequi doloremque, adipisci. Odit distinctio labore aliquid iste!
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <h4 class="panel-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#answerThree" aria-expanded="false" aria-controls="answerThree">
                  <i class="fa fa-angle-right"></i> Nam liber tempor cum soluta nobis eleifend option congue nil?
              </h4>
            </div>
            <div id="answerThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                
                 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis quas quaerat perferendis doloremque, dolorem ullam possimus ducimus mollitia. Minus nulla magnam quibusdam. Animi cupiditate omnis architecto, iste aliquam delectus nemo!
              </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Common answer ends -->
</div>