@extends('layouts.dashboard')

@section('title', trans('messages.allClients.pageTitle') . Options::get('title') )

@section('content')

                <div class="content">
                    <div class="row content-body">
                        

                        <!--  CLIENT BOX -->
                        <div class="ticket-box-row-first">
                            <div class="col-md-12  ticket-big-box-col">
                                <div class="staffs-p">
                                    <div class="box-title">
                                        <div class="title-text pull-left">
                                            <h3>{{ trans('messages.allClients.title') }}</h3>
                                        </div>
                                        
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-hover responsive-table">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('messages.allClients.name') }}</th>
                                                    <th>{{ trans('messages.allClients.email') }}</th>
                                                    <th class="text-center">{{ trans('messages.allClients.clientImage') }}</th>
                                                    <th>{{ trans('messages.allClients.registeredAt') }}</th>

                                                    {{-- If the user is a Admin or a staff member has those permission, then only they can see those --}}
                                                    @if( 
                                                        Auth::user()->hasRole('admin') || 
                                                        Options::get('staff_permission') == 'can_view_and_delete_clients' 
                                                    )

                                                        <th class=" text-center">{{ trans('messages.allClients.action') }}</th>

                                                    @endif

                                                </tr>
                                            </thead>

                                            @if( count($clients) )

                                            	@foreach($clients as $client)

		                                            <tr class="client_row">
		                                                <td>{{ $client->fullName() }}</td>
		                                                <td><a href="mailto:{{ $client->email }}">{{ $client->email }}</a></td>
		                                                <td class="text-center tbl-img">
		                                                    <img src="{{ $client->profileImg(50) }}" alt="">
		                                                </td>
		                                                <td class="">{{ $client->created_at->diffForHumans() }}</td>

                                                        {{-- If the user is a Admin or a staff member has those permission, then only they can see those --}}
                                                        @if( 
                                                            Auth::user()->hasRole('admin') || 
                                                            Options::get('staff_permission') == 'can_view_and_delete_clients' 
                                                        )
    		                                                <td class="text-center tbl-btn">
                                                                <!-- Not for Mobile Browser -->
                                                                <button class="staff_edit edit-btn md-trigger hidden-xs" data-modal="editClient" value="{{ $client->id }}">
                                                                    <i class="fa fa-edit"></i>
                                                                </button>
                                                                <button class="client_remove remove-btn" type="submit" value="{{ $client->id }}"><i class="fa fa-times"></i></button>
    		                                                </td>
                                                        @endif

		                                            </tr>


	                                            @endforeach

                                            @else
                                            	<tr>
                                            		<td colspan="6" align="center"> {{ trans('messages.allClients.noClients') }} </td>
                                            	</tr>
                                            @endif

                                        </table>                              
                                    </div>

                                </div>

                                <!-- Pagination -->
                                {{ $clients->render() }}
                            </div>
                        </div>

                    </div>
                </div>

@stop
