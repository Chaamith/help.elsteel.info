
@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )


@endif
@section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )

@section('content')

    <div class="content">
        <div class="row content-body">

            <!-- TICKETS BOX -->
            <div class="ticket-box-row-first">
                <div class=" ticket-big-box-col ">
                <div class="col-md-5 notif_top">
                    <div class="ticket-big-box s-ticket-p recent-ticket-box notif_top">
                        <div class="box-title">
                            <div class="title-text pull-left">
                                @if( Request::get('term') )
                                    <h3> {{ trans('messages.tickets.searchTitle') }} <i>'{{ Request::get('term') }}'</i> </h3>
                                @else
                                    <h3> {{ trans('messages.tickets.tickets') }}</h3>
                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">

                            <table id="quotestable" class="display " width="100%">

                            </table>
                        </div>
                        </div>
                    </div>



                </div>
                <div class="col-lg-7 ticket-big-box-col">


                    <div class="ticket-big-box statistics_box">
                        <div class="box-title clearfix">
                            <div class="title-text pull-left">
                                <h3>statistics of this month</h3>
                            </div>
                        </div>
                        <div id="barchart">

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


@stop

@section('script')


    <script>

        $('document').ready(function () {
            function rendNA(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                return data;
            }
            function rendUser(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                console.log(row)
                return data + ' ' + row.assigned_to.last_name;
            }
            function rendName(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                return data + ' ' + row.last_name;
            }
            $('.search').click(function(){
                $('#'+$(this).attr('input-id')).val($(this).attr('input-val'));
                tdata.draw();
            })

            function QuotationNo(data, type, row) {
                if (row.revision_no > 0) {
                    return data + '-' + row.revision_no;
                }
                return data;
            }

            function status(data, type, row) {
                if (data == '0') {
                    return 'Pending';
                } else if (data == '1') {
                    return 'In Progress';
                } else if (data == '2') {
                    return 'Repaired';
                } else if (data == '3') {
                    return 'Done';
                }
                return data;
            }

            var tdata = $('#quotestable').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                "processing": true,
                "serverSide": true,
                "order": [[ 0, "desc" ]],
                'ajax': {
                    "url": "/dashboard/reports/department-vs-tickets-tdata/",
                    "type": "GET",
                    data: function (d) {

                        d.status = $('#tableStatus').val();

                    }
                },
                "initComplete":function( settings, json){

                    drawChart(json);
                },
                'columns': [
                    {title:'DEPARTMENT',data: 'department.name', name: 'department.name',searchable: false},
                    {title:'COUNT',data: 'count', name: 'count',searchable: false}



                ],

            })




function drawChart(json) {
    var xAxisData=[];
    var yAxisData=[];
    // call your function here
    json.data.forEach(function(key) {
        xAxisData.push(key.department.name);
        yAxisData.push(key.count);
    });
    Highcharts.chart('barchart', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },

        xAxis: {
            categories: xAxisData,
            crosshair: true
        },


        series: [{
            name:'Departments',
            data: yAxisData

        }]
    });
}



        });


    </script>

@endsection