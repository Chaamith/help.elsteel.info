
@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )


@endif
@section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )

@section('content')

    <div class="content">
        <div class="row content-body">

            <!-- TICKETS BOX -->
            <div class="ticket-box-row-first">
                <div class=" ticket-big-box-col ">
                <div class="col-md-5 notif_top">
                    <div class="ticket-big-box s-ticket-p recent-ticket-box notif_top">
                        <div class="box-title">
                            <div class="title-text pull-left">
                                @if( Request::get('term') )
                                    <h3> {{ trans('messages.tickets.searchTitle') }} <i>'{{ Request::get('term') }}'</i> </h3>
                                @else
                                    <h3> {{ trans('messages.tickets.tickets') }}</h3>
                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">

                            <table id="quotestable" class="display " width="100%">

                            </table>
                        </div>
                        </div>
                    </div>



                </div>
                <div class="col-lg-7 ticket-big-box-col">


                    <div class="ticket-big-box statistics_box">
                        <div class="box-title clearfix">
                            <div class="title-text pull-left">
                                <h3>statistics of time</h3>
                            </div>
                        </div>
                        <div id="barchart"  >

                        </div>
                        <hr>
                        <div id="barchartStaff"  >

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>


@stop

@section('script')


    <script>

        $('document').ready(function () {

            var tdata = $('#quotestable').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                "processing": true,
                "serverSide": true,
                "order": [[ 0, "desc" ]],
                'ajax': {
                    "url": "/dashboard/reports/client-vs-tickets-tdata/",
                    "type": "GET",
                    data: function (d) {

                        d.status = $('#tableStatus').val();

                    }
                },
                "initComplete":function( settings, json){

                    drawChart(json);
                },
                'columns': [
                    {title:'Client',data: 'name', name: 'user.first_name',searchable: false},
                    {title:'new',data: 'new', name: 'new',searchable: false},
                    {title:'pending',data: 'pending', name: 'pending',searchable: false},
                    {title:'solved',data: 'solved', name: 'solved',searchable: false}


                ],

            })




            function drawChart(json) {
                var xAxisData=[];
                var yAxisDataStaff=[];

                //var status={'solved':[],'pending':[],'new':[]};
               // var statusArray=['solved','pending','new'];
                <?='var statusArray='.json_encode($ticketStatus)?>;
              //  var status=[{name:'solved',data:[]},{name:'pending',data:[]},{name:'new',data:[]}];
                var status=[];
                var statusStaff=[];
                for(var q in statusArray) {
                    status.push({name:statusArray[q],data:[]}) ;
                    statusStaff.push({name:statusArray[q],data:[]}) ;
                }
                // call your function here
                json.data.forEach(function(key) {

                    console.log(key.role)
                    if(key.role=='client'){
                        xAxisData.push(key.name);
                        for(var k in status) {
                            status[k].data.push(key[status[k].name]);
                        }
                    }else{
                        yAxisDataStaff.push(key.name);
                        for(var k in status) {
                            statusStaff[k].data.push(key[statusStaff[k].name]);

                        }
                    }


                    //status[1].push(key.new);

                });
                Highcharts.chart('barchart', {
                    chart: {
                        type: 'bar',
                        height: window.outerHeight
                    },
                    title: {
                        text: 'Client vs Tickets'
                    },
                    xAxis: {
                        categories: xAxisData
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Tickets'
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: status
                });


                Highcharts.chart('barchartStaff', {
                    chart: {
                        type: 'bar',
                        //height: window.outerHeight
                    },
                    title: {
                        text: 'Client(Staff) vs Tickets'
                    },
                    xAxis: {
                        categories: yAxisDataStaff
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Tickets'
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: statusStaff
                });
            }
        });


    </script>

@endsection