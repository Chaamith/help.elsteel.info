
@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )


@endif
@section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )

@section('content')

    <div class="content">

        <div class="row content-body">

            <div class="row">
                <form id="chartform">
                    <div class="col-md-5">
                        <label>Chart Type</label>
                        <div class="form-group subject-group">

                            <select name="chart" id="chart" class="form-control subject" >
                                <option selected value="subject-wise" data-formsetup="setup1" data-chartfunc="drawChart"> Subject vs Tickets ( group by departments ) </option>
                                <option  value="country-wise-subject" data-formsetup="setup3" data-chartfunc="drawCountryVsSubject"> Subject vs Tickets ( all country at once ) </option>
                                <option value="timely-count"  data-formsetup="setup2" data-chartfunc="drawLineChart"> Timely Ticket Count ( department wise ) </option>
                            </select>
                            <span class="help-block"></span>

                        </div>
                    </div>
                    <div class="col-md-2 select-div" id="location-div">
                        <label>Location</label>
                        <div class="form-group location-group">

                            <select name="location" id="location" class="form-control" >
                                <option value="all" selected > All Locations </option>
                                @foreach(config('settings.location') as $k=>$location)
                                    <option value="{{ $k }}" > {{ $location}} </option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>

                        </div>
                    </div>

                    <div class="col-md-2 select-div" id="month-div">
                        <label>Month</label>
                        <div class="form-group location-group">

                            <select name="month" id="month" class="form-control" >
                                <option value="all" selected > All Time </option>
                                @foreach($months as $month)
                                    <option value="{{ $month->month }}" > {{ $month->month}} </option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>

                        </div>
                    </div>
                    <div class="col-md-5 select-div" id="dep-div" >
                        <label>Department</label>
                        <div class="form-group department-group">

                            <select name="department_id" id="department_id" class="form-control department" >
                                <option  selected value="all">All Departments</option>
                                @foreach($departments as $department)
                                    <option value="{{ $department->id }}"> {{ $department->name }} </option>
                                @endforeach
                            </select>
                            <span class="help-block"></span>

                        </div>
                    </div>

                </form>
            </div>
            <!-- TICKETS BOX -->
            <div class="ticket-box-row-first">
                <div class="col-lg-12 ticket-big-box-col">


                    <div class="ticket-big-box statistics_box">

                        <div class="box-title clearfix">
                            <div class="title-text pull-left">
                                <h3 id="chartTitle">Subject wise ticket count of all time, grouped by department </h3>
                            </div>
                        </div>

                        <div  id="barchart"  >

                        </div>

                    </div>
                </div>
                <div class=" ticket-big-box-col ">



                </div>

            </div>

        </div>
    </div>


@stop

@section('script')


    <script>
        {!! 'var departments='.$departments.';var monthsSet='.$months.';' !!}
function setup1(){
    $('.select-div').hide();
    $('#location-div,#month-div').show();
        }
function setup2(){
    $('.select-div').hide();
    $('#dep-div').show();
}
        function setup3(){
            $('.select-div').hide();
            $('#month-div').show();
        }
function drawCountryVsSubject(json) {
    $('#barchart').html('');
    var chartType=$('#chart option:selected');
    $('#chartTitle').text(chartType.text());
    var location=$('#location option:selected').text();
    var month=$('#month option:selected').text();

    var xAxisData=[];
    var dataSeries=[];
    var dataZero=[];
    $.each(departments, function(index, value) {
        xAxisData.push(value.name);
        dataZero.push(0);
    });


    $.each(json, function(index, countryData) {
var showLegend=undefined;
var marginTop=50;
var titleY=20;
if(index=='Katunayake'){
    showLegend=true;
    marginTop=175;
    titleY=130;
}

    $('<div class="chart">')
        .appendTo('#barchart')
        .highcharts({
        chart: {
            marginTop: marginTop,
            type: 'column',

        },
        title: {
            align: 'left',
            x: 100,
            y: titleY,
            text: '('+index+'/'+month+')'
        },
        xAxis: {
            categories: xAxisData
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Tickets'
            }
        },
        legend: {
            floating: true,
            enabled: showLegend,
            verticalAlign: 'top',

            itemWidth: 160,
            itemStyle: {
                'font-size': '10px'
            }
        },
        plotOptions: {

        },
        series: countryData
    });

    });





}
function drawChart(json) {
            var chartType=$('#chart option:selected');
            $('#chartTitle').text(chartType.text());
            var location=$('#location option:selected').text();
            var month=$('#month option:selected').text();

            var xAxisData=[];
            var dataSeries=[];
            var dataZero=[];
            $.each(departments, function(index, value) {
                xAxisData.push(value.name);
                dataZero.push(0);
            });



            Highcharts.chart('barchart', {
                chart: {
                    type: 'column',

                },
                title: {
                    text: 'Subject vs Tickets Count ('+location+'/'+month+')'
                },
                xAxis: {
                    categories: xAxisData
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Tickets'
                    }
                },
                legend: {
                    itemWidth: 160,
                    itemStyle: {
                        'font-size': '10px'
                    }
                },
                plotOptions: {

                },
                series: json
            });







        }
function drawLineChart(json) {
    var location=$('#location option:selected').text();
    var month=$('#month option:selected').text();
    var department_name=$('#department_id option:selected').text();

    var xAxisData=[];
    var dataSeries=[];
    var dataZero=[];
    $.each(monthsSet, function(index, value) {
        xAxisData.push(value.month);
        dataZero.push(0);
    });



    Highcharts.chart('barchart', {
        chart: {
            type: 'line',

        },
        title: {
            text: 'Timely Tickets Count ('+department_name+')'
        },
        xAxis: {
            categories: xAxisData
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Tickets'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {

        },
        series: json
    });







}
        $('document').ready(function () {
            setup1();//to initial form set


//            var tdata = $('#quotestable').DataTable({
//                'paging': false,
//                'lengthChange': false,
//                'searching': false,
//                'ordering': true,
//                'info': true,
//                'autoWidth': false,
//                "processing": true,
//                "serverSide": true,
//                "order": [[ 0, "desc" ]],
//                'ajax': {
//                    "url": "/dashboard/reports/common-tdata",
//                    "type": "GET",
//                    data: function (d) {
//
//                        d.status = $('#tableStatus').val();
//
//                    }
//                },
//                "initComplete":function( settings, json){
//
//                    //drawChart(json);
//                },
//                'columns': [
//                    {title:'Location',data: 'name', name: 'name',searchable: false},
//                    {title:'S/H ware',data: '3', name: '3',searchable: false},
//                    {title:'CRM',data: '4', name: '4',searchable: false},
//                    {title:'EPS',data: '5', name: '5',searchable: false},
//                    {title:'Other',data: '6', name: '6',searchable: false},
//
//
//
//
//                ],
//
//            })







            $('select').change(function(){

                var chartType=$('#chart option:selected');

                $('#chartTitle').text(chartType.text());
                window[chartType.attr('data-formsetup')]();
                $.getJSON("/dashboard/reports/common-chart-data", $('#chartform').serialize(), function(data) {

                    window[chartType.attr('data-chartfunc')](data);
                });
            });

            // for window on load first chart

            $.getJSON("/dashboard/reports/common-chart-data", $('#chartform').serialize(), function(data) {

                drawChart(data)
            });
           // $('select').trigger("change");
        });


    </script>

@endsection