
@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )


@endif
@section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )

@section('content')

    <div class="content">
        <div class="row content-body">

            <!-- TICKETS BOX -->
            <div class="ticket-box-row-first">
                <div class="col-lg-12 ticket-big-box-col">


                    <div class="ticket-big-box statistics_box">
                        <div class="box-title clearfix">
                            <div class="title-text pull-left">
                                <h3>Location wise ticket count of all time</h3>
                            </div>
                        </div>
                        <div id="barchart"  >

                        </div>

                    </div>
                </div>
                <div class=" ticket-big-box-col ">
                <div class="col-md-8 notif_top">
                    <div class="ticket-big-box s-ticket-p recent-ticket-box notif_top">
                        <div class="box-title">
                            <div class="title-text pull-left">
                                @if( Request::get('term') )
                                    <h3> {{ trans('messages.tickets.searchTitle') }} <i>'{{ Request::get('term') }}'</i> </h3>
                                @else
                                    <h3> {{ trans('messages.tickets.tickets') }}</h3>
                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">

                            <table id="quotestable" class="display " width="100%">

                            </table>
                        </div>
                        </div>
                    </div>

                    <div class="col-lg-4 ticket-big-box-col">


                        <div class="ticket-big-box statistics_box">

                            <div id="piechart"  >

                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@stop

@section('script')


    <script>

        $('document').ready(function () {

            var tdata = $('#quotestable').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                "processing": true,
                "serverSide": true,

                'ajax': {
                    "url": "/dashboard/reports/location-vs-tickets-tdata",
                    "type": "GET",
                    data: function (d) {

                        d.status = $('#tableStatus').val();

                    }
                },
                "initComplete":function( settings, json){

                    drawChart(json);
                },
                'columns': [
                    {title:'Month',data: 'name', name: 'name',searchable: false},
                    {title:'Katunayake',data: 'Katunayake', name: 'Katunayake',searchable: false},
                    {title:'Pallekele',data: 'Pallekele', name: 'Pallekele',searchable: false},
                    {title:'India',data: 'India', name: 'India',searchable: false},
                    {title:'Poland',data: 'Poland', name: 'Poland',searchable: false},
                    {title:'Denmark',data: 'Denmark', name: 'Denmark',searchable: false},
                    {title:'Total',data: 'total', name: 'total',searchable: false}



                ],

            })




            function drawChart(json) {
                var xAxisData=[];
                var yAxisData=[];
                var yAxisData = new Array();
                //var status={'solved':[],'pending':[],'new':[]};
               // var statusArray=['solved','pending','new'];
                <?='var statusArray='.json_encode(config('settings.location'))?>;
              //  var status=[{name:'solved',data:[]},{name:'pending',data:[]},{name:'new',data:[]}];
                var status=[];
                var totalSet=[];
                var pieData=[];
                var countArray=[];
                for(var q in statusArray) {
                    status.push({name:statusArray[q],data:[]}) ;
                    countArray.push({name:statusArray[q],y:0}) ;
                }
                // call your function here
                json.data.forEach(function(key) {
                    xAxisData.push(key.name);

                    for(var k in status) {
                        status[k].data.push(key[status[k].name]);
                       // countArray[k].push(key[status[k].name])
                        countArray[k].y=countArray[k].y+key[status[k].name];

                    }

                    //status[1].push(key.new);

                });

                Highcharts.chart('barchart', {
                    chart: {
                        type: 'column',

                    },
                    title: {
                        text: 'Location vs Tickets'
                    },
                    xAxis: {
                        categories: xAxisData
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Tickets'
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {

                    },
                    series: status
                });


                Highcharts.chart('piechart', {
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Total Ticket Count vs Location as a percentage'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                            showInLegend: true
                        }
                    },
                    series: [{
                        name: 'Tickets',
                        colorByPoint: true,
                        data: countArray
                    }]
                });





            }


        });


    </script>

@endsection