
@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )


@endif
@section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )

@section('content')

    <div class="content">
        <div class="row content-body">

            <!-- TICKETS BOX -->
            <div class="ticket-box-row-first">
                <div class="col-lg-12 ticket-big-box-col">


                    <div class="ticket-big-box statistics_box">
                        <div class="box-title clearfix">
                            <div class="title-text pull-left">
                                <h3>Department wise ticket count of all time, grouped by location </h3>
                            </div>
                        </div>
                        <div id="barchart"  >

                        </div>

                    </div>
                </div>
                <div class=" ticket-big-box-col ">
                <div class="col-md-8 notif_top">
                    <div class="ticket-big-box s-ticket-p recent-ticket-box notif_top">
                        <div class="box-title">
                            <div class="title-text pull-left">
                                @if( Request::get('term') )
                                    <h3> {{ trans('messages.tickets.searchTitle') }} <i>'{{ Request::get('term') }}'</i> </h3>
                                @else
                                    <h3> {{ trans('messages.tickets.tickets') }}</h3>
                                @endif
                            </div>
                        </div>
                        <div class="table-responsive">

                            <table id="quotestable" class="display " width="100%">

                            </table>
                        </div>
                        </div>
                    </div>

                    <div class="col-lg-4 ticket-big-box-col">


                        <div class="ticket-big-box statistics_box">

                            <div id="piechart"  >

                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>


@stop

@section('script')


    <script>

        $('document').ready(function () {

            var tdata = $('#quotestable').DataTable({
                'paging': false,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                "processing": true,
                "serverSide": true,
                "order": [[ 0, "desc" ]],
                'ajax': {
                    "url": "/dashboard/reports/location-vs-deptickets-tdata",
                    "type": "GET",
                    data: function (d) {

                        d.status = $('#tableStatus').val();

                    }
                },
                "initComplete":function( settings, json){

                    drawChart(json);
                },
                'columns': [
                    {title:'Location',data: 'name', name: 'name',searchable: false},
                    {title:'S/H ware',data: '3', name: '3',searchable: false},
                    {title:'CRM',data: '4', name: '4',searchable: false},
                    {title:'EPS',data: '5', name: '5',searchable: false},
                    {title:'Other',data: '6', name: '6',searchable: false},




                ],

            })




            function drawChart(json) {

                var xAxisData=[];
                var status=[];
                var countArray=[];

                <?='var statusArray='.json_encode(config('settings.location'))?>;
                <?='var departments='.json_encode($departments)?>;

                for(var q in departments) {
                    status.push({name:departments[q].name,id:departments[q].id,data:[], color:'#EE'+Math.floor(departments[q].id*1133).toString(16).substr(0,4) }) ;
                    countArray.push({name:departments[q].name,y:0}) ;

                }


                json.data.forEach(function(key) {
                    xAxisData.push(key.name);

                    for(var k in status) {
                        if(key[ status[k].id]){
                            status[k].data.push(key[ status[k].id]);
                        }else{
                            status[k].data.push(0);
                        }


                    }

                    //status[1].push(key.new);

                });

                Highcharts.chart('barchart', {
                    chart: {
                        type: 'column',

                    },
                    title: {
                        text: 'Department vs Tickets Count'
                    },
                    xAxis: {
                        categories: xAxisData
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Total Tickets'
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {

                    },
                    series: status
                });







            }


        });


    </script>

@endsection