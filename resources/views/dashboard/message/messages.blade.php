@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )
    @section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )
@else
    @section('title', 'All ' . trans('messages.tickets.title') . Options::get('title')  )
@endif


@section('content')

<div class="content">
    <div class="row content-body">

        <!-- TICKETS BOX -->
        <div class="ticket-box-row-first">
            <div class="col-md-12  ticket-big-box-col">
                <div class="ticket-big-box s-ticket-p recent-ticket-box">
                    <div class="box-title">
                        <div class="title-text pull-left">
                                <h3> All Messages</h3>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover  responsive-table">
                            <thead>
                                <tr>
                                    <th>{{ trans('messages.tickets.ticketID') }}</th>
                                    <th>{{ trans('messages.tickets.ticketTitle') }}</th>
                                    <th>{{ trans('messages.tickets.status') }}</th>
                                    <th>{{ trans('messages.tickets.action') }}</th>
                                </tr>
                            </thead>

                            {{-- Check if there's any tickets --}}
                            @if( count($messages) )
                                {{-- Loop through over the tickets --}}
								@foreach( $messages as $message )
    
		                            <tr class="ticket_row">
		                                <td>#{{ $message->id }}</td>

		                                <td>{{ $message->message }}</td>

		                                <td>
		                                    <span class="status-{{ $message->status }}">
		                                    	{{ ucwords($message->status) }}
		                                    </span>
		                                </td>
                                        <td>
                                            <button class="message_remove remove-btn" type="submit" value="{{ $message->id }}"><i class="fa fa-times"></i></button>
                                        </td>
		                            </tr>

								@endforeach
                                {{-- End the Loop --}}
	
                            @else
                                
                                {{-- If no rows found --}}
                            	<tr>
                            		<td colspan="7" align="center">{{ trans('messages.tickets.noTickets') }} </td>
                            	</tr>
                            	
                            @endif
                            {{-- End the if condiction --}}

                        </table>                              
                    </div>
                </div>

                {{ $messages->appends(['term' => Request::get('term')])->render() }}
                
            </div>
        </div>

    </div>
</div>


@stop
