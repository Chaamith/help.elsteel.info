@extends('layouts.dashboard')

@section('title', $ticket->subject . ' | ' . Options::get('title') )

@section('content')

<div class="content">
    

    <!-- SINGLE TICKET -->
    <div class="row content-body ticket-single-page">
        <div class="col-lg-8 col-md-12 ticket-details">

            <!-- TICKET BOX, TICKET DETAIL -->
            <div class="ticket-box">
                <div class="ticket-header">
                    <h4> {{ trans('messages.singleTicket.id') }} {{ $ticket->id }}</h4>
                </div>
                <div class="ticket-title row">
                    <div class="col-md-8 col-lg-9">
                        <h3> {{ $ticket->subject.' ('.@config('settings.location')[$ticket->location].')' }} </h3>
                    </div>
                    <div class="col-md-4 col-lg-3 ">

                         <!-- Check If the date is over 1 month or not -->
                   		<?php $date = $ticket->created_at; ?>
                        <p class="t-t-d">   {{ $date->diffInMonths(Carbon::now()) >= 1 ? $date->format('j M Y, g:ia') : $date->diffForHumans() }}  </p>

                    </div>
                </div>
                <div class="ticket-detail">
                    {!! $ticket->message !!}
					
					@if( $ticket->files )
                        <div class="file-attached">
                            <div class="file-single">
                                <div class="file-t">
                                    <span>{{ trans('messages.singleTicket.attachedFile') }}</span>
                                </div>
                                <div class="file-name">
                                    <span>
                                        <a href="/uploads/{{ $ticket->files }}"> {{ $ticket->files }} </a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>

                @include('partials.site.replies')

            </div>
        </div>
        
    
        @include('partials.dashboard.ticket_information')
        

    </div>

</div>

@stop

{{-- Include scripts --}}
@section('script')
	
    <script src="{{ URL::asset('assets/js/tinymce/tinymce.min.js') }}"></script>
    <script>
        // Tiny MCE Editor for the Custom Textarea,
        tinymce.init({
            selector: 'textarea', 
            height: 150,
            menubar: false,
            paste_auto_cleanup_on_paste : true,
            paste_remove_styles: true,
            fullpage_default_fontsize: "18px",
            paste_as_text: true,
            paste_remove_styles_if_webkit: true,
            paste_strip_class_attributes: "all",
            plugins : 'paste advlist autolink link image lists charmap print preview',
            toolbar: false,
            content_css: siteUrl + '/assets/css/tinymce.site.css',
            setup: function (editor) {
                editor.addButton('pasteCode', {
                text: 'Paste code',
                icon: false,
                onclick: function () {
                    editor.insertContent('<pre><code>"paste your code inside the quotation, then remove the quotation marks."</code></pre>');
                }
                });
            },
        });

    </script>
@stop