@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )
    @section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )
@else
    @section('title', ucwords($view) . trans('messages.tickets.title') . Options::get('title')  )
@endif


@section('content')

<div class="content">
    <div class="row content-body">

        <!-- TICKETS BOX -->
        <div class="ticket-box-row-first">
            <div class="col-md-12  ticket-big-box-col">
                <div class="ticket-big-box s-ticket-p recent-ticket-box">
                    <div class="box-title">
                        <div class="title-text pull-left">
                            @if( Request::get('term') )
                                <h3> {{ trans('messages.tickets.searchTitle') }} <i>'{{ Request::get('term') }}'</i> </h3>
                            @else
                                <h3> {{ $view }} {{ trans('messages.tickets.tickets') }}</h3>
                            @endif
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover  responsive-table">
                            <thead>
                                <tr>
                                    <th>{{ trans('messages.tickets.ticketID') }}</th>
                                    <th>{{ trans('messages.tickets.assignedTo') }}</th>
                                    <th>{{ trans('messages.tickets.ticketTitle') }}</th>
                                    <th>{{ trans('messages.tickets.department') }}</th>
                                    <th>{{ trans('messages.tickets.date') }}</th>
                                    <th>{{ trans('messages.tickets.client') }}</th>
                                    <th>{{ trans('messages.tickets.status') }}</th>

                                    <th>{{ trans('messages.tickets.action') }}</th>
                                </tr>
                            </thead>

                            {{-- Check if there's any tickets --}}
                            @if( count($tickets) )
                                {{-- Loop through over the tickets --}}
								@foreach( $tickets as $ticket )
    
		                            <tr class="ticket_row">
		                                <td>#{{ $ticket->id }}</td>
                                        <td>	<a href="{!! route('dashboard.tickets', ['staff?id='.$ticket->assigned_to]) !!}">
                                                {{ $ticket->getAssignedStaff() }}</a>
                                        </td>
	                                	<td>
	                                    	<a href="{!! route('dashboard.single.ticket', [
                                                'subject' => $ticket->FormatSubject(), 
                                                'id' => $ticket->id
                                            ]) !!}">
	                                    		{{ str_limit($ticket->subject, 30) }}
	                                    	</a>
	                                    </td>
		                                <td>{{ $ticket->getDepartment() }}</td>
		                                <td>{{ $ticket->created_at->diffForHumans() }}</td>
		                                <td>{{ $ticket->user->fullName() }}</td>
		                                <td>
		                                    <span class="status-{{ $ticket->status }}">
		                                    	{{ ucwords($ticket->status) }}
		                                    </span>
		                                </td>

                                        <td>
                                            <button class="ticket_remove remove-btn" type="submit" value="{{ $ticket->id }}"><i class="fa fa-times"></i></button>
                                        </td>
		                            </tr>

								@endforeach
                                {{-- End the Loop --}}
	
                            @else
                                
                                {{-- If no rows found --}}
                            	<tr>
                            		<td colspan="7" align="center">{{ trans('messages.tickets.noTickets') }} </td>
                            	</tr>
                            	
                            @endif
                            {{-- End the if condiction --}}

                        </table>                              
                    </div>
                </div>

                {{ $tickets->appends(['term' => Request::get('term')])->render() }}
                
            </div>
        </div>

    </div>
</div>


@stop
