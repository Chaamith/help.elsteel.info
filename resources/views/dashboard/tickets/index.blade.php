@extends('layouts.dashboard')
{{-- Dynamic site title --}}
@if( Request::get('term') )


@endif
@section('title', trans('messages.tickets.pageTitle') .  Request::get('term') . ' | ' . Options::get('title')  )

@section('content')




    <!-- TICKETS BOX -->

    <div class="ticket-big-box s-ticket-p notif_top">
        <div class="box-title row">
            <div class="title-text ">
                @if( Request::get('term') )
                    <h3> {{ trans('messages.tickets.searchTitle') }} <i>'{{ Request::get('term') }}'</i></h3>
                @else
                    <h3> {{ trans('messages.tickets.tickets') }}</h3>
                @endif
            </div>
        </div>
        <div class="row">
            <form id="chartform" class="">
                <div class="col-md-2 select-div pull-left" id="location-div">
                    <label>Location</label>
                    <div class="form-group location-group">

                        <select name="location" id="location" class="form-control">
                            <option value="" selected> All Locations</option>
                            @foreach(config('settings.location') as $k=>$location)
                                <option value="{{ $k }}"> {{ $location}} </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>

                    </div>
                </div>


                <div class="col-md-2 select-div" id="month-div">
                    <label>Status</label>
                    <div class="form-group location-group">

                        <select name="status" id="status" class="form-control">
                            <option value="" selected> All Status</option>
                            @foreach($ticketStatus as $Status)
                                @if(request()->input('status')==$Status)
                                    <option selected value="{{ $Status }}"> {{ $Status}} </option>
                                @else
                                    <option value="{{ $Status }}"> {{ $Status}} </option>
                                @endif
                            @endforeach
                        </select>
                        <span class="help-block"></span>

                    </div>
                </div>
                <div class="col-md-4 select-div" id="dep-div">
                    <label>Department</label>
                    <div class="form-group department-group">

                        <select name="department_id" id="department_id" class="form-control department">
                            <option selected value="">All Departments</option>
                            @foreach($departments as $department)
                                <option value="{{ $department->id }}"> {{ $department->name }} </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>

                    </div>
                </div>
                <div class="col-md-4 select-div" id="month-div">
                    <label>Subjects</label>
                    <div class="form-group location-group">

                        <select name="subject" id="subject" class="form-control">
                            <option value="" selected> All Subjects</option>
                            @foreach($subjects as $subject)
                                <option value="{{ $subject->name }}"> {{ $subject->name}} </option>
                            @endforeach
                        </select>
                        <span class="help-block"></span>

                    </div>
                </div>
            </form>
        </div>
        <div>

            <table id="ticketstable" width="100%">

            </table>
        </div>
    </div>








@stop

@section('script')


    <script>

        $('document').ready(function () {
            function rendNA(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                return data;
            }

            function rendUser(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                return '<a href="/dashboard/tickets/staff?id=' + row.assigned_to.id + '">' + data + ' ' + row.assigned_to.last_name + '</a>';

            }

            function rendName(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                return data + ' ' + row.user.last_name;
            }

            function rendStatus(data, type, row) {
                if (!data) {
                    return 'N/A';
                }
                return '<span class="status-' + data + '">' + data + '</span>';
            }

            $('.search').click(function () {
                $('#' + $(this).attr('input-id')).val($(this).attr('input-val'));
                tdata.draw();
            })

            function QuotationNo(data, type, row) {
                if (row.revision_no > 0) {
                    return data + '-' + row.revision_no;
                }
                return data;
            }

            function status(data, type, row) {
                if (data == '0') {
                    return 'Pending';
                } else if (data == '1') {
                    return 'In Progress';
                } else if (data == '2') {
                    return 'Repaired';
                } else if (data == '3') {
                    return 'Done';
                }
                return data;
            }

            var tdata = $('#ticketstable').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false,
                "processing": true,
                "serverSide": true,
                "order": [[0, "desc"]],
                'ajax': {
                    "url": "/dashboard/tickets/tdata/",
                    "type": "GET",
                    data: function (d, t) {
                        d.columns[3].search.value = $('#location').val();
                        d.columns[2].search.value = $('#subject').val();
                        d.columns[6].search.value = $('#status').val();
                        d.columns[7].search.value = $('#department_id').val();


                    }
                },
                'columns': [
                    {title: 'TICKET ID', data: 'id', name: 'id', searchable: false},
                    {
                        title: 'ASSIGNED TO',
                        data: 'assigned_to.first_name',
                        name: 'assignedTo.first_name',
                        render: rendUser
                    },
                    {title: 'TICKET TITLE', data: 'subject', name: 'subject', searchable: true},
                    {title: 'LOCATION', data: 'location', name: 'location', searchable: true},
                    {title: 'DATE', data: 'created_at', name: 'created_at', searchable: false},
                    {
                        title: 'CLIENT',
                        data: 'user.first_name',
                        name: 'user.first_name',
                        render: rendName,
                        searchable: false
                    },
                    {title: 'STATUS', data: 'status', name: 'status', searchable: true, render: rendStatus},

                    {
                        title: 'DEPARTMENTS',
                        data: 'department_id',
                        name: 'department_id',
                        searchable: true,
                        visible: false
                    },
                    {title: 'ACTION', data: 'action', name: 'action', orderable: false, searchable: false},

                ],

            })

// =======================================================================//
//                 Delete a new record                                    //
// =======================================================================//


            // Opening a alert model before delete
            $('#quotestable').on('click', '.delete', function () {
                $('#deleteModal').modal('show');
                //Set row id in alert message
                $('#colid').html($(this).attr('data-para'));

            })
            // Opening a alert model before delete
            $('select').change(function () {
                tdata.ajax.reload();

            })
            //Sending the delete request
            $('#Delete').click(function () {
                // create token parameter for laravel
                var data = {'_token': '{{ csrf_token() }}'};
                var id = $('#colid').text();
                var url = '/quotes/' + id;
                // make ajax request for delete
                $.ajax({
                    dataType: 'json',
                    type: 'DELETE',
                    url: url,
                    data: data
                }).done(function (data) {
                    // hide model after success delete
                    $('#deleteModal').modal('hide');
                    //Create the alert in content heder section
                    //Create the alert
                    makeAlert(data, '.content-header');
                    // reload datatable
                    tdata.ajax.reload();
                });

            });

            $('#department_id').change(function () {

                $.getJSON("/get/subjects", {id: $(this).val()}, function (data) {
                    var subject = $('#subject');
                    subject.empty();
                    subject.append('<option selected="" value=""> All Subjects </option>');
                    $.each(data, function (index, element) {
                        subject.append("<option value='" + element.name + "'>" + element.name + "</option>");
                    });
                });
            });
        });


    </script>
@endsection