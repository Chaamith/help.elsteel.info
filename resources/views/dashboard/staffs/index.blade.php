@extends('layouts.dashboard')

@section('title', trans('messages.allStaffs.pageTitle') . Options::get('title') )

@section('content')

                <div class="content">
                    <div class="row content-body">
                        
                        <!--  STAFF BOX -->
                        <div class="ticket-box-row-first">
                            <div class="col-md-12  ticket-big-box-col">
                                <div class="staffs-p">
                                    <div class="box-title">
                                        <div class="title-text pull-left">
                                            <h3>{{ trans('messages.allStaffs.title')}}</h3>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-hover responsive-table">
                                            <thead>
                                                <tr>
                                                    <th>{{ trans('messages.allStaffs.name') }}</th>
                                                    <th>{{ trans('messages.allStaffs.email') }}</th>
                                                    <th>{{ trans('messages.allStaffs.department') }}</th>
                                                    <th class=" text-center">{{ trans('messages.allStaffs.staffImage') }}</th>
                                                    <th>{{ trans('messages.allStaffs.assignedTickets') }}</th>
                                                    <th>{{ trans('messages.allStaffs.solvedTickets') }}</th>
                                                    <th >{{ trans('messages.allStaffs.registeredAt') }}</th>
                                                    <th class="text-center">{{ trans('messages.allStaffs.action') }}</th>
                                                </tr>
                                            </thead>

                                            @if( count($staffs) )

                                            	@foreach($staffs as $staff)

		                                            <tr class="staff_row">

                                                        <td>
                                                            <a href="{!! route('dashboard.tickets', ['staff?id='.$staff->id]) !!}">
                                                                {{ $staff->fullName() }}</a>
                                                        </td>
                                                        <td><a href="mailto:{{ $staff->email }}">{{ $staff->email }}</a></td>
                                                        <td>{{ $staff->getDepartmentName() }}</td>
		                                                <td class="text-center tbl-img">
		                                                    <img src="{{ $staff->profileImg(50) }}" alt="">
		                                                </td>
                                                        <td>{{ $staff->assignedTickets()->count() }}</td>
                                                        <td>{{ $staff->solvedTickets()->count() }}</td>
		                                                <td>{{ $staff->created_at->diffForHumans() }}</td>
		                                                <td class=" text-center tbl-btn">
                                                            <!-- Not for Mobile Browser -->
                                                            <button class="staff_edit edit-btn md-trigger hidden-xs" data-modal="editStaff" value="{{ $staff->id }}">
                                                                <i class="fa fa-edit"></i>
                                                            </button>

                                                            <!-- Only for Mobile Browser -->
                                                            <a href="{{ route('dashboard.staffs.getPageEdit', $staff->id) }}" class="staff_edit edit-btn visible-xs">
                                                                <i class="fa fa-edit"></i>
                                                            </a>

                                                            <button class="staff_remove remove-btn"  type="submit" value="{{ $staff->id }}">
                                                                <i class="fa fa-times"></i>
                                                            </button>
		                                                </td>
		                                            </tr>

	                                            @endforeach

                                            @else
                                            	<tr>
                                            		<td colspan="8" align="center"> {{ trans('messages.allStaffs.noStaffs')}} </td>
                                            	</tr>
                                            @endif

                                        </table>                              
                                    </div>

                                </div>

                                <!-- Pagination -->
                                {{ $staffs->render() }}
                            </div>
                        </div>

                    </div>
                </div>

@stop
