@extends('layouts.dashboard')

@section('title', $staff->exists ? trans('messages.addNewStaff.editTitle') . ' | ' : trans('messages.addNewStaff.title') . ' | ' . Options::get('title') )

@section('content')
	<link rel="stylesheet" href="/assets/css/bootstrap-datetimepicker.min.css" />

                <div class="content">
                    <div class="row content-body">
						<div class="get-create-form settings-tab">
						    <h4>{{ $staff->exists ? 
						    trans('New Server Status') :
						    trans('New Server Status') }}</h4>

						    {{ Form::model($staff, [
						    	'route' => $staff->exists ? 'dashboard.server.store' : 'dashboard.server.store',
						    	'method' => 'post', 
						    	'id' => 'server_status_form'
						    ]) }}

						    	@if( $staff->exists )
									<input type="hidden" name="staff_id" value="{{ $staff->id }}">
						    	@endif
							<div class="form-group">
								{{ Form::label('location', 'Location') }}
								{{ Form::select('location', config('settings.location'), null, ['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								{{ Form::label('server_id', 'Server') }}
								{{ Form::select('server_id', config('settings.servers'), null, ['class' => 'form-control'])}}
							</div>
							<div class="form-group">
								{{ Form::label('status', 'Servers Status') }}
								{{ Form::select('status', config('settings.serverStatus'), null, ['class' => 'form-control'])}}
							</div>
						        <div class="form-group staff_first_name_group">
						            {{ Form::label( trans('Status Time') ) }}
									<input class="form-control" name="logDateTime" id="datetimepicker1" type="text" required="1">
						        </div>



						        <div class="form-status staff_form_status"></div>


						        <div class="form-group  submit-btns">
						            <button type="submit" class="btn btn-success">{{$staff->exists ? 
						            'Add Status'  :
						            'Add Status' }}</button>
						        </div>
						    {{ Form::close() }}
						</div>
                    </div>
                </div>

@stop
