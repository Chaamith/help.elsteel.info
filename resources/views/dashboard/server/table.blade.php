@extends('layouts.dashboard')

@section('title', trans('messages.allStaffs.pageTitle') . Options::get('title') )

@section('content')

                <div class="content">
                    <div class="row content-body">
                        
                        <!--  STAFF BOX -->
                        <div class="ticket-box-row-first">
                            <div class="col-md-12  ticket-big-box-col">
                                <div class="staffs-p">
                                    <div class="box-title">
                                        <div class="title-text pull-left">
                                            <h3>{{ 'Server Status'}}</h3>
                                        </div>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table table-hover responsive-table">
                                            <thead>
                                                <tr>
                                                    <th>Server Location</th>
                                                    <th>Sever Name</th>
                                                    <th>Status</th>
                                                    <th>Time Du.</th>
                                                    <th>Date Time Range</th>
                                                    <th></th>

                                                </tr>
                                            </thead>

                                            @if( count($staffs) )

                                            	@foreach($staffs as $staff)

		                                            <tr class="staff_row">

                                                        <td>

                                                                {{ config('settings.location')[$staff->location] }}
                                                        </td>

                                                        <td>{{ config('settings.servers')[$staff->server_id] }}</td>
                                                        <td>{{ config('settings.serverStatus')[$staff->status] }}</td>
                                                        <td>{{ $staff->diff }}</td>
                                                        <td>{!!  $staff->start.' <b>to</b> '.$staff->end !!}</td>
		                                                <td class=" text-center tbl-btn">


                                                            <form action="{{ route('dashboard.server.destroy', $staff->id) }}" method="POST" class="ng-pristine ng-valid">
                                                                <button type="submit" class="remove-btn"><i class="fa fa-trash-o"></i></button>
                                                                <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                                                <input name="_method" value="DELETE" type="hidden">
                                                            </form>

		                                                </td>
		                                            </tr>

	                                            @endforeach

                                            @else
                                            	<tr>
                                            		<td colspan="8" align="center"> No Records. </td>
                                            	</tr>
                                            @endif

                                        </table>                              
                                    </div>

                                </div>

                                <!-- Pagination -->
                                {{ $staffs->render() }}
                            </div>
                        </div>

                    </div>
                </div>

@stop
