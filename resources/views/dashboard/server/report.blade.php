@extends('layouts.dashboard')

@section('title', trans('messages.allStaffs.pageTitle') . Options::get('title') )

@section('content')

    <div class="content">
        <div class="row content-body">
            <div class="col-lg-12 ticket-big-box-col">


                <div class="ticket-big-box statistics_box">
<p>&nbsp;</p>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            <a href="?" class="btn btn-default btn-block btn-sm {{request()->input('location')?:'active'}} ">All</a>
                        </div>
                            @foreach(config('settings.location') as $k=>$v)
                                <div class="col-md-2">
                                    <a href="?location={{$k}}" class="btn btn-default btn-block btn-sm {{request()->input('location')==$k?'active':''}}">{{$v}}</a>
                                </div>
                            @endforeach

                    </div>
                    <p>&nbsp;</p>

                    <div id="lineChart">

                    </div>

                </div>
            </div>
            <!--  STAFF BOX -->
            <div class="ticket-box-row-first">
                <div class="col-md-12  ticket-big-box-col">
                    <div class="staffs-p">
                        <div class="box-title">
                            <div class="title-text pull-left">
                                <h3>{{ 'Server Status'}}</h3>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover responsive-table">
                                <thead>
                                <tr>
                                    <th>Server Location</th>
                                    <th>Sever Name</th>
                                    <th >Status</th>
                                    <th>Count</th>
                                    <th align="right">Total Time</th>
                                    <th>Month</th>
                                    <th></th>

                                </tr>
                                </thead>

                                @if( count($servers) )

                                    @foreach($servers as $server)

                                        <tr class="staff_row">

                                            <td>

                                                    {{ config('settings.location')[$server->location] }}
                                            </td>

                                            <td>{{ config('settings.servers')[$server->server_id] }}</td>
                                            <td>{{ config('settings.serverStatus')[$server->status] }}</td>
                                            <td>{{ $server->count }}</td>
                                            <td align="right">{{ gmdate("H:i:s", $server->total) }}</td>
                                            <td>{{ (date_format(date_create($server->start),'M-y')) }}</td>
                                            <td class=" text-center tbl-btn">


                                                <form action="{{ route('dashboard.server.destroy', $server->id) }}"
                                                      method="POST" class="ng-pristine ng-valid">
                                                    <button type="submit" class="remove-btn"><i
                                                                class="fa fa-trash-o"></i></button>
                                                    <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                                    <input name="_method" value="DELETE" type="hidden">
                                                </form>

                                            </td>
                                        </tr>

                                    @endforeach

                                @else
                                    <tr>
                                        <td colspan="8" align="center"> {{ trans('messages.allStaffs.noStaffs')}} </td>
                                    </tr>
                                @endif

                            </table>
                        </div>

                    </div>


                </div>
            </div>

        </div>
    </div>

@stop
@section('script')


    <script>
        {!! 'var monthsSet='.$months.';'.'var chartData='.json_encode($dataArray).';' !!}
        console.log(chartData)
        drawLineChart(chartData);

        function drawLineChart(json) {
            var location = $('#location option:selected').text();
            var month = $('#month option:selected').text();
            var department_name = $('#department_id option:selected').text();

            var xAxisData = [];
            var dataSeries = [];
            var dataZero = [];
            $.each(monthsSet, function (index, value) {
                xAxisData.push(value.month);
                dataZero.push(0);
            });


            Highcharts.chart('lineChart', {
                chart: {
                    type: 'line',

                },
                title: {
                    text: 'Server Downtime'
                },
                xAxis: {
                    categories: xAxisData
                },
                yAxis: {

                    title: {
                        text: 'Total Time (HH:MM)'
                    }, labels: {
                        formatter: function () {
                            var time = this.value;
                            var hours1 = parseInt(time / 3600);
                            var mins1 = parseInt((parseInt(time % 3600)) / 60);
                            return hours1 + ':' + mins1;
                        }
                    }
                },
                tooltip: {
                    formatter: function () {
                        var time = this.y;
                        var hours1 = parseInt(time / 3600);
                        var mins1 = parseInt((parseInt(time % 3600)) / 60);
                       var t= hours1 + ':' + mins1;
                        return this.series.name+' <b>' + this.x +
                            '</b> time <b>' + t + '</b>';

                    }
                },
                legend: {
                    reversed: true
                },
                plotOptions: {},
                series: json
            });


        }
    </script>

@endsection