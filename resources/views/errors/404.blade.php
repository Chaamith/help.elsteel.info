<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>404 | {{ Options::get('title') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- NORMALIZING CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css') }}">

    <!-- BOOTSTRAP FRONTEND FRAMEWORD -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.css') }}">

    <!-- ICONIC FONT - FONT AWESOME -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-awesome.min.css') }}">
    
    <!-- LOAD WEB FONTS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/font-stylesheet.css') }}">

    <!-- MAIN STYLESHEET FILE -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/error_style.css') }}">
</head>
<body>
    
    <div class="wrapper">
        <div class="outer">
            <div class="inner">
                <div class="content error-area">

                
                    	<img src="{{ URL::asset('assets/img/' . Options::getLogo() ) }}" alt="">
                        <h1>404 Error!</h1>
                        <p>The page or content you are looking for cannot be found! <br>Please  go back to home.</p>

                        <a href="/"><i class="fa fa-long-arrow-left"></i> Go back to home</a>
                        

                </div>
            </div>
        </div>
    </div>

</body>
</html>