<?php

namespace App\Models;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
	
	protected $fillable = ['body', 'file'];

	/**
	 *
	 * A reply belongs to a ticket
	 * @return App\Models\Ticket
	 *
	 */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

	/**
	 *
	 * A reply belongs to a user
	 * @return App\Models\User
	 */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
