<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;


class Lead extends Model
{
    use  SoftDeletes;

    protected $fillable = ['company', 'contact1', 'contact2', 'phone', 'fax','email','business','turnover','notes','files','employees','catalogue_during_the_fair','catalogue_after_the_fair','future_action','created_by','updated_by','deleted_by'];


    /**
     *
     * Ticket belongs to the user
     * @return App\Models\User
     *
     */
//    public  function user()
//    {
//        return $this->belongsTo(User::class);
//    }
    /**
     *
     * Ticket has a assigned user
     * @return App\Models\User
     *
     */
//    public  function assignedTo()
//    {
//        return $this->belongsTo(User::class,'assigned_to');
//    }

    /**
     *
     * Ticket has many replies
     * @return App\Models\Branch
     *
     */
    public function branch_type_selections()
    {
        return $this->hasMany(BranchTypeSelection::class,'lead_id','id');
    }

    public function addSelectedBranchTypes($data)
    {
        $intances = [];

        if (count($data)) {
            $intances = $this->branch_type_selections()->createMany($data);
        }

        return $intances;
    }



    /**
     *
     * Ticket belongs to the department
     * @return App\Models\Department
     *
     */
//    public function department()
//    {
//        return $this->belongsTo(Department::class);
//    }


    /**
     *
     * Get the department name
     * @return App\Models\Department
     * 
     *
     */
//    public function getDepartment(  )
//    {
//        $department =  $this->attributes['department_id'];
//
//        if( $department ) {
//            return $this->department->name;
//        } else {
//            return 'No department.';
//        }
//    }

    /**
     *
     * Format the subject
     * @return string
     *
     */
//    public function FormatSubject(  )
//    {
//        $value =  $this->attributes['subject'];
//
//        return str_replace(' ', '-', $value);
//    }


    /**
     *
     * Reformat the subject
     * @param $subject
     * @return string
     *
     */
//    public static function reformatTicket( $subject )
//    {
//        return str_replace('-', ' ', $subject);
//    }



    /**
     *
     * Get the assigned user
     * @return App\Models\User
     *
     */
//    public function getAssignedStaff()
//    {
//        $assigned_to = $this->attributes['assigned_to'];
//
//        if( $assigned_to == 0) {
//            return trans('messages.singleTicket.notAssignedYet');
//        } else {
//            $user = User::find($assigned_to);
//
//            if( $user ) {
//                return $user->fullName() ;
//            }
//        }
//    }

    /**
     *
     * Get the assigned user
     * @return App\Models\User
     *
     */
//    public function getSolvedBy()
//    {
//        $solved_by = $this->attributes['solved_by'];
//
//        if( $solved_by == 0) {
//            return 'Not solved yet.';
//        } else {
//            $user = User::find($solved_by);
//
//            if( $user ) {
//                return $user->fullName();
//            }
//        }
//    }


}
