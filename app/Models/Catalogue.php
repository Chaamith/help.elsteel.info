<?php

namespace App\Models;

use App\Models\Ticket;
use Illuminate\Database\Eloquent\Model;

class Catalogue extends Model
{
    protected $fillable = ['name'];

}
