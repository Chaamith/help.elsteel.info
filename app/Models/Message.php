<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message', 'status'];

    /**
     *
     * Get the status of the
     * @return App\Models\Message
     *
     */
    public function statusMessage()
    {
        $status = $this->attributes['status'];
        switch ($status) {
            case 'working':
                return '<span class="fa fa-check-circle-o" style="color:#008a32"></span>  ';
                break;
            case 'not-working':
                return '<span class="fa fa fa-times-circle" style="color:#ff0003"></span>  ';
                break;
            case 'warning':
                return '<span class="glyphicon glyphicon-exclamation-sign" style="color:#ffc554"></span>  ';
                break;
            default:
                return '<span class="glyphicon glyphicon-bell" style="color:#00bdff"></span>  ';
        }

    }

}
