<?php

namespace App\Models;

use App\Models\User;
use App\Models\Ticket;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['user_id', 'ticket_id', 'title', 'type'];


    /**
     *
     * Notification belongs to any user
     * @return App\Models\User
     *
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     *
     * Notification belongs to any ticket
     * @return App\Models\User
     *
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
