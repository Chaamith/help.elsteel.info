<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = ['agent', 'ip', 'type'];

    protected $dates = ['created_at', 'Date'];
}
