<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Role;
use App\Models\Ticket;
use App\Models\Department;

class User extends Authenticatable
{
    use EntrustUserTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'department_id', 'role_name','location'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     *
     * Hash the password
     * @param  $value
     * 
     */
    public function setPasswordAttribute( $value )
    {
        $this->attributes['password'] = \Hash::make( $value );
    }


    /**
     *
     * Return Role  of the user
     * @return App\Models\Role
     *
     */
    public function role()
    {
       // return $this->hasMany(Role::class);
        return $this->belongsToMany(Role::class);
    }


    /**
     *
     * User Has many tickets
     * @return App\Models\Department
     *
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }


    /**
     *
     * User Has many tickets
     * @return App\Models\Ticket
     *
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }


    /**
     *
     * Get the full name of the user
     * @return App\Models\User
     *
     */
    public function fullName()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

    
    /**
     *
     * Get the gravatar image of the user
     * @param $size
     * @return string
     *
     */
    
    public function gravatarImage($size)
    {
        $email = $this->attributes['email'];
        $size = $size;

        return $grav_url = url('/').('/assets/img/users/avatar.png' );
        //return $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=mm&s=" . $size;
    }


    /**
     *
     * Get the profile image
     * @param $size
     * @return $this->gravatarImage
     */
    
    public function profileImg( $size )
    {
        $profileImg = $this->attributes['profile_img'];

        if( $profileImg ) {
            return '/uploads/profile_images/' . $profileImg;
        } else {
           // return $this->gravatarImage( $size );
        }
    }



    /**
     *
     * Get the department for the user
     * @return App\Models\Department
     *
     */
    public function getDepartmentName()
    {
        $department_id = $this->attributes['department_id'];

        // Find the department
        $department = Department::find($department_id);

        if( $department )  {
            return $department->name;
        } else {
            return 'No department';
        }
    }


    /**
     *
     * Get the assigned tickets for user
     * @return App\Models\Ticket
     *
     */
    public function assignedTickets()
    {
        return $this->hasMany(Ticket::class, 'assigned_to');
    }


    /**
     *
     * Get solved tickets for the user
     * @return App\Models\Ticket
     *
     */
    public function solvedTickets()
    {
        return $this->hasMany(Ticket::class, 'solved_by');
    }
    
}
