<?php

namespace App\Models;

use App\Models\Ticket;
use Illuminate\Database\Eloquent\Model;

class BranchTypeSelection extends Model
{
    protected $fillable = ['lead_id','selected_branch_type'];


    /**
     *
     * Branch Has many tickets
     * @return App\Models\Lead
     *
     */
//    public function leads()
//    {
//        return $this->hasMany(Lead::class,'id','lead_id');
//    }

}
