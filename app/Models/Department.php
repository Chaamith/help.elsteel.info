<?php

namespace App\Models;

use App\Models\Ticket;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['name'];


    /**
     *
     * Departhmen Has many tickets
     * @return App\Models\Ticket
     *
     */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }

}
