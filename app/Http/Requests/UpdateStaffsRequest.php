<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateStaffsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    =>      'required|min:3',
            'department_id' =>      'required',
           // 'password'      =>      'required|min:6',
            'staff_img'     =>      'mimes:jpg,jpeg,bmp,png',
            'role_name'     =>      'required',
        ];
    }
}
