<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateLeadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'file'              => 'mimes:jpg,jpeg,bmp,png,doc,docx,xls,csv'
            'file'              => 'mimes:jpg,jpeg,png'
        ];
    }
}
