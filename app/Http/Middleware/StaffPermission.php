<?php

namespace App\Http\Middleware;

use Closure;
use App\Classes\Options;
use Illuminate\Support\Facades\Auth;

class StaffPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if( Auth::user()->hasRole('staff') ) {
            $staffPerm = Options::get('staff_permission');

            if( $staffPerm == 'none' ) {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
