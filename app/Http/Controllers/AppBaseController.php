<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Option;
use App\Http\Requests;

class AppBaseController extends Controller
{    
	
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct()
    {
    	$timezone = Option::where('name', 'timezone')->first();
    	
    	// Setting the default timezone
    	date_default_timezone_set($timezone->value);
    }
}
