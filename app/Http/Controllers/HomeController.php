<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Catalogue;
use App\Models\Employee;
use App\Models\User;
use App\Models\Department;
use App\Models\Subject;
use App\Models\Option;
use App\Models\Message;
use App\Models\Role;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HomeController extends HomeBaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Option $options)
    {
        parent::__construct();
        $this->options = $options;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        // If application is not installed
        if ( ! file_exists( storage_path('installed') ) ) {
            return redirect('install');
        } 

        // Get the theme style from database
        $style_option = $this->options
                        ->where('name', 'style')
                        ->first();

        // Get all departments
        $departments = Department::all();
        // Get all messages
        $messages = Message::all();
        // Get all subjects
        $subjects = Subject::orderBy('name')->get();
        // Get all branches
        $branches = Branch::all();
        // Get all No of Employees
        $employees = Employee::all();
        // Get all Catalogues
        $catalogues = Catalogue::all();

        // Dynamically change view according to admin preference
        if( $style_option->value == 'homepage_style_1') {
            return view('home', compact('departments','subjects','messages','branches','employees','catalogues'));
        } elseif( $style_option->value == 'homepage_style_2') {
            return view('homepage2', compact('departments','subjects','messages','branches','employees','catalogues'));
        } else {
            return view('home', compact('departments','subjects','messages','branches','employees','catalogues'));
        }
    }


    /**
     *
     * Homepage style 2
     * @return View
     *
     */
    public function homepage2()
    {
        $departments = Department::all();
        return view('homepage2', compact('departments','subjects'));
    }
    public function testmail()
    {
        if (file_exists('./web.config')) {
echo 'success';

        }




    }

    /**
     *
     * User Settings Page
     * @return View
     *
     */
    public function settings()
    {
        $user = \Auth::user();
        return view('profile.settings', compact('user'));
    }

    public function getSubjects(){
        $id=\request('id');

        $subjects = Subject::orderBy('name');
        if($id){
            $subjects->where('departments_id',$id);
        }
        return ($subjects->get(['id','name']));
    }
}
