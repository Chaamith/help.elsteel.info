<?php

namespace App\Http\Controllers;

use App\Models\BranchTypeSelection;
use Illuminate\Http\Request;
use App\Models\Lead;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\Catalogue;
use App\Http\Requests;
use Auth;

class LeadController extends HomeBaseController
{

    /**
     *
     * @return void
     *
     */

    function __construct(Lead $leads)
    {
        parent::__construct();
    	$this->leads = $leads;
    }

    /**
     *
     * Return all leads from database model
     * @return App\Models\Lead
     * 
     */
    public function allLeads()
    {
        $leads = Lead::orderBy('created_at', 'DESC')->paginate(20);


        return view('leads.all', compact('leads'));
    }


    /**
     *
     * Create lead
     * @param  App\Http\Requests
     * @return App\Models\Lead
     * 
     */
    public function createLead(Requests\CreateLeadRequest $request)
    {

        $request['created_by'] = Auth::user()->id;

            $lead = Lead::create($request->only(
                    'company',
                    'contact1',
                    'contact2',
                    'phone',
                    'fax',
                    'email',
                    'business',
                    'turnover',
                    'notes',
                    'employees',
                    'catalogue_during_the_fair',
                    'catalogue_after_the_fair',
                    'future_action',
                    'created_by'
                ));


            // Process the file
            $this->processFile($request, $lead);

            //adjusting branch_types array according to our requirnment before save
            for ($i = 0; $i < count($request['branch_types']); $i++) {
                $formData['branch_types_array'][$i]['selected_branch_type']=$request['branch_types'][$i];
            }

            //save branch type selection
            if (isset($formData['branch_types_array'])){
                $lead->addSelectedBranchTypes($formData['branch_types_array']);
            }


            //return redirect('/');
        return redirect('/all-leads')->with('message', 'Lead has been created');

    }


    /**
     *
     * Lead edit page
     * @param  Illuminate\Http\Request, $id
     * @return App\Models\Department
     *
     */
    public function getEditLeads(Request $request, $id)
    {
        $lead = $this->leads->find($id);
        $lead->load('branch_type_selections');

        $branches = Branch::all();
        $employees = Employee::all();
        $catalogues = Catalogue::all();

        return view('leads.edit', compact('lead','branches','employees','catalogues'));

    }


    /**
     *
     * Update The Edited Lead
     * @param  App\Http\Requests, $id
     * @return App\Models\Lead
     *
     */
    public function postEditLeads(Requests\UpdateLeadRequest $request, $id)
    {
        $request['updated_by'] = Auth::user()->id;

        $lead = $this->leads->find($id);

        $lead->fill($request->only('company', 'contact1', 'contact2','phone','fax','email','business','turnover','notes','employees','catalogue_during_the_fair','catalogue_after_the_fair','future_action','updated_by'))->save();

        // Process the updated file
        $this->processFileUpdate($request, $lead);

        //delete all selected branch types related to this lead
        BranchTypeSelection::where('lead_id', $lead->id)->delete();

        //adjusting branch_types array according to our requirnment before save
        for ($i = 0; $i < count($request['branch_types']); $i++) {
            $formData['branch_types_array'][$i]['selected_branch_type']=$request['branch_types'][$i];
        }

        //save branch type selection
        if (isset($formData['branch_types_array'])){
            $lead->addSelectedBranchTypes($formData['branch_types_array']);
        }

        return redirect()->back()->with('message', 'Lead has been updated');

    }

    /**
     *
     * Process the file
     * @param  $request, $lead
     *
     */
    public function processFile($request, $lead)
    {
        // Check if the form has any file
        if( $request->file('file') ) :

            // Make the folder if not exists
            if (!file_exists('uploads')) {
                mkdir('uploads', 0777, true);
            }

            // Get the requested file
            $file = $request->file('file');
            $name = time() . '_' . $file->getClientOriginalName();

            // Move the file to the folder
            $file->move('uploads', $name);

            // Update the database
            $lead->files = $name;
            $lead->save();

        endif;   
    }



    /**
     *
     * Process the upldated file
     * @param  $request, $lead
     *
     */
    public function processFileUpdate($request, $lead)
    {
        // Check if the form has any file
        if( $request->file('file') ) :


            // Make the folder if not exists
            if (!file_exists('uploads')) {
                mkdir('uploads', 0777, true);
            }

            // Get the requested file
            $file = $request->file('file');
            $name = time() . '_' . $file->getClientOriginalName();

            // Move the file to the folder
            $file->move('uploads', $name);

            // if has any exixting file, delete that first
            if( $lead->files ) {
                unlink('uploads/' . $lead->files);
            }

            // Update the database
            $lead->files = $name;
            $lead->save();

        endif;   
    }
}
