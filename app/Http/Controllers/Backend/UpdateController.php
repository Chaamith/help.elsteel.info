<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Option;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class UpdateController extends Controller
{

	protected $permissions;


    function __construct(Option $options)
    {
    	$this->options = $options;
    	$this->runOptionsTableSeeder();
    }

    public function index()
    {
        return redirect()->route('dashboard.index')->with('message', 'Your application has been updated');
    }

    public function runOptionsTableSeeder()
    {
        $stfPerm = $this->options->where('name', 'staff_permission')->first();

        if( ! $stfPerm ) {
        	$option = new Option;
        	$option->name = 'staff_permission';
        	$option->value = 'can_view_clients';
        	$option->save;
        	// $this->options->create(['name'=> 'staff_permission', 'value' => 'can_view_clients']);
        }


    }
}
