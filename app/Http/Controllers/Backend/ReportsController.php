<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Ticket;
use App\Models\Visitor;
use App\Models\Department;
use App\Models\Subject;
use App\Http\Requests;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Collection;
use Yajra\Datatables\Datatables;

class ReportsController extends AppBaseController
{

    /**
     *
     * @param  App\Models\Visitor
     * @return void
     *
     */
    public $ticketStatus = array('new', 'pending', 'solved');

    function __construct(Visitor $visitors)
    {
        parent::__construct();

        $this->visitors = $visitors;
    }


    /**
     *
     * Dashboard Page
     * @return App\Models\Ticket
     */
    public function index()
    {
        // Get only 7 tickets for the dashboard
        $tickets = Ticket::take(7)->orderBy('created_at', 'DESC')->get();

        // Ticket Counting
        $all_ticket_counting = Ticket::count();
        $new_ticket_counting = Ticket::where('status', 'new')->count();
        //$my_ticket_counting = Ticket::where('assigned_to', Auth::user()->id)->count();
        $pending_ticket_counting = Ticket::where('status', 'pending')->count();
        $solved_ticket_counting = Ticket::where('status', 'solved')->count();

        // Get clients
        $role = Role::where('name', 'client')->first();
        $clients = Role::find($role->id)->users()->orderBy('created_at', 'DESC')->take(6)->get();

        // Get staffs
        $role = Role::where('name', 'staff')->first();
        $staffs = Role::find($role->id)->users()->orderBy('created_at', 'DESC')->take(10)->get();

        $departments = Department::lists('name', 'id');

        // $visitors = $this->visitors->all();
        // $clients = $this->visitors->whereMonth('created_at', '=', date('m'))->where('type', 'client')->get();
        // $visitors = $this->visitors->whereMonth('created_at', '=', date('m'))->where('type', 'guest')->get();

        $data = compact('tickets', 'departments', 'my_ticket_counting', 'all_ticket_counting', 'new_ticket_counting', 'pending_ticket_counting', 'solved_ticket_counting', 'clients', 'staffs');

        return view('dashboard.home', $data);
    }


    /**
     *
     * Return the authenticated user as JSON
     * @return App\Models\User
     */
    public function depvstkt()
    {
        return view('dashboard.reports.depvstkt', array('ticketStatus' => $this->ticketStatus));
    }

    public function depvstkt_tdata()
    {

        //$tickets = Ticket::select(['tickets.*', \DB::raw('count(sp_tickets.department_id) as count')])->with('department')->groupBy('tickets.department_id');

        // return Datatables::of($tickets)->make(true);
        $tickets = Ticket::select(['tickets.*', \DB::raw('count(sp_tickets.department_id) as count')])->with('department')->groupBy('tickets.department_id', 'tickets.status')->get();
        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;
        foreach ($tickets as $ticket) {
            $newTickets[$ticket->department_id]['name'] = $ticket->department->name;
            $newTickets[$ticket->department_id][$ticket->status] = $ticket->count;
            foreach ($statusArray as $status) {
                if ($status == $ticket->status) {
                    $newTickets[$ticket->department_id][$status] = $ticket->count;
                } elseif (!isset($newTickets[$ticket->department_id][$status])) {
                    $newTickets[$ticket->department_id][$status] = 0;
                }
            }

        }
        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);
    }

    public function clientvstkt()
    {
        return view('dashboard.reports.clientvstkt', array('ticketStatus' => $this->ticketStatus));
    }

    public function clientvstkt_tdata()
    {

//        $tickets = Ticket::select(['tickets.*',\DB::raw('count(sp_tickets.user_id) as count')])->with('user')->groupBy('tickets.user_id');
//
//        return Datatables::of($tickets)->make(true);
        //$tickets = Role::find(5)->users()->select(['tickets.*', \DB::raw('count(sp_tickets.user_id) as count')])->orderBy('created_at', 'DESC')->paginate(20);
        //$tickets = Ticket::select(['tickets.*', \DB::raw('count(sp_tickets.user_id) as count')])->with('user.role')->groupBy('tickets.user_id', 'tickets.status')->get();
        //dd($tickets->toArray());
        $tickets = Ticket::select(['tickets.*', \DB::raw('count(sp_tickets.user_id) as count')])->with('user.role')->groupBy('tickets.user_id', 'tickets.status')->get();

        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;
        foreach ($tickets as $ticket) {
            $newTickets[$ticket->user_id]['name'] = $ticket->user->first_name;
            $newTickets[$ticket->user_id]['role'] = $ticket->user->role[0]->name;
            $newTickets[$ticket->user_id][$ticket->status] = $ticket->count;
            foreach ($statusArray as $status) {
                if ($status == $ticket->status) {
                    $newTickets[$ticket->user_id][$status] = $ticket->count;
                } elseif (!isset($newTickets[$ticket->user_id][$status])) {
                    $newTickets[$ticket->user_id][$status] = 0;
                }
            }

        }

        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);

    }

    public function uservstkt()
    {
        return view('dashboard.reports.uservstkt', array('ticketStatus' => $this->ticketStatus));
    }

    public function uservstkt_tdata()
    {

//        $tickets = Ticket::select(['tickets.*',\DB::raw('count(sp_tickets.user_id) as count')])->with('user')->groupBy('tickets.user_id');
//
//        return Datatables::of($tickets)->make(true);

        $tickets = Ticket::select(['tickets.*', \DB::raw('count(sp_tickets.assigned_to) as count')])->with('assignedTo')->groupBy('tickets.assigned_to', 'tickets.status')->get();


        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;

        foreach ($tickets as $ticket) {

            $newTickets[$ticket->assigned_to]['name'] = isset($ticket->assignedTo->first_name) ? $ticket->assignedTo->first_name : '';
            $newTickets[$ticket->assigned_to][$ticket->status] = $ticket->count;
            foreach ($statusArray as $status) {
                if ($status == $ticket->status) {
                    $newTickets[$ticket->assigned_to][$status] = $ticket->count;
                } elseif (!isset($newTickets[$ticket->assigned_to][$status])) {
                    $newTickets[$ticket->assigned_to][$status] = 0;
                }
            }

        }

        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);

    }

    public function mystats()
    {
        return view('dashboard.reports.mystats', array('ticketStatus' => $this->ticketStatus));
    }

    public function mystats_tdata()
    {

//        $tickets = Ticket::select(['tickets.*',\DB::raw('count(sp_tickets.user_id) as count')])->with('user')->groupBy('tickets.user_id');
//
//        return Datatables::of($tickets)->make(true);

        $tickets = Ticket::select(['tickets.*', \DB::raw('count(sp_tickets.assigned_to) as count')])->with('assignedTo')->groupBy(\DB::raw('DATE_FORMAT(sp_tickets.created_at, "%m-%Y")'), 'tickets.status')->where('assigned_to', \Auth::user()->id)->get();
        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;
        foreach ($tickets as $ticket) {
            $month = $ticket->created_at->format('M-y');
            $newTickets[$month]['name'] = $month;
            $newTickets[$month][$ticket->status] = $ticket->count;

            foreach ($statusArray as $status) {
                if ($status == $ticket->status) {
                    $newTickets[$month][$status] = $ticket->count;
                    $total[] = $ticket->count;
                } elseif (!isset($newTickets[$month][$status])) {
                    $newTickets[$month][$status] = 0;
                }
            }

            $newTickets[$month]['total'] = (isset($newTickets[$month]['total']) ? $newTickets[$month]['total'] : 0) + $ticket->count;
        }
        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);

    }

    public function locationvstkt()
    {
        return view('dashboard.reports.locationvstkt', array('ticketStatus' => $this->ticketStatus));
    }

    public function locationvstkt_tdata()
    {

//        $tickets = Ticket::select(['tickets.*',\DB::raw('count(sp_tickets.user_id) as count')])->with('user')->groupBy('tickets.user_id');
//
//        return Datatables::of($tickets)->make(true);

        $tickets = Ticket::select(['tickets.*', \DB::raw('count(*) as count')])->groupBy(\DB::raw('DATE_FORMAT(sp_tickets.created_at, "%m-%Y")'), 'location')->get();
        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;
        $locations = config('settings.location');
        foreach ($tickets as $ticket) {
            $month = $ticket->created_at->format('Y-m');
           // $month = $ticket->created_at->format('y-M');
            $newTickets[$month]['name'] = $month;
            $newTickets[$month][$ticket->location] = $ticket->count;

            foreach ($locations as $location => $locationName) {
                if ($location == $ticket->location) {
                    $newTickets[$month][$locationName] = $ticket->count;
                    $total[] = $ticket->count;
                } elseif (!isset($newTickets[$month][$location])) {
                    $newTickets[$month][$locationName] = 0;
                }
            }

            $newTickets[$month]['total'] = (isset($newTickets[$month]['total']) ? $newTickets[$month]['total'] : 0) + $ticket->count;
        }

        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);

    }

    public function locationvsdeptkt()
    {
        $departments = Department::all(['id', 'name']);
        return view('dashboard.reports.locationvsdeptkt', array('ticketStatus' => $this->ticketStatus, 'departments' => $departments));
    }

    public function locationvsdeptkt_tdata()
    {

//        $tickets = Ticket::select(['tickets.*',\DB::raw('count(sp_tickets.user_id) as count')])->with('user')->groupBy('tickets.user_id');
//
//        return Datatables::of($tickets)->make(true);

        //$tickets = Ticket::select(['tickets.*', \DB::raw('count(*) as count')])->groupBy(\DB::raw('DATE_FORMAT(sp_tickets.created_at, "%m-%Y")'),'location')->get();
        $departments = Department::all(['id', 'name']);
        $locations = config('settings.location');
        foreach ($departments as $v) {
            $zero[$v['id']] = 0;

        }

        foreach ($locations as $k => $v) {
            $newTickets[$k] = $zero;
            $newTickets[$k]['name'] = $v;

        }

        $tickets = Ticket::select(['tickets.*', \DB::raw('count(*) as count')])->with('department')->groupBy('tickets.department_id', 'location')->get();
        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;


        foreach ($tickets as $ticket) {


            $department = $ticket->department_id;
            $newTickets[$ticket->location]['name'] = $locations[$ticket->location];
            $newTickets[$ticket->location][$department] = $ticket->count;

//            foreach ($locations as $location =>$locationName){
//                if($location==$ticket->location){
//                    $newTickets[$locationName][$department] =  $ticket->count;
//                    $total[]= $ticket->count;
//                }elseif(!isset($newTickets[$location][$department])){
//                    $newTickets[$locationName][$department] =  0;
//                }
//            }
//
//            $newTickets[$locationName]['total'] =  (isset($newTickets[$locationName]['total'])?$newTickets[$locationName]['total']:0)+$ticket->count;
//
        }

        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);

    }


    public function commonreport()
    {
        $subjects = Subject::orderBy('name')->get();
        $departments = Department::all(['id', 'name']);
        $months = Ticket::all([\DB::raw('DISTINCT DATE_FORMAT(sp_tickets.created_at, "%Y-%m") AS month')]);
        return view('dashboard.reports.commonreport', array('ticketStatus' => $this->ticketStatus, 'departments' => $departments, 'subjects' => $subjects, 'months' => $months));
    }

    public function commonreport_tdata()
    {

//        $tickets = Ticket::select(['tickets.*',\DB::raw('count(sp_tickets.user_id) as count')])->with('user')->groupBy('tickets.user_id');
//
//        return Datatables::of($tickets)->make(true);

        //$tickets = Ticket::select(['tickets.*', \DB::raw('count(*) as count')])->groupBy(\DB::raw('DATE_FORMAT(sp_tickets.created_at, "%m-%Y")'),'location')->get();

        $departments = Department::all(['id', 'name']);
        $locations = config('settings.location');
        foreach ($departments as $v) {
            $zero[$v['id']] = 0;

        }

        foreach ($locations as $k => $v) {
            $newTickets[$k] = $zero;
            $newTickets[$k]['name'] = $v;

        }

        $tickets = Ticket::select(['tickets.*', \DB::raw('count(*) as count')])->with('department')->groupBy('tickets.department_id', 'location')->get();
        // $newTickets = new Collection;
        $statusArray = $this->ticketStatus;


        foreach ($tickets as $ticket) {


            $department = $ticket->department_id;
            $newTickets[$ticket->location]['name'] = $locations[$ticket->location];
            $newTickets[$ticket->location][$department] = $ticket->count;

//            foreach ($locations as $location =>$locationName){
//                if($location==$ticket->location){
//                    $newTickets[$locationName][$department] =  $ticket->count;
//                    $total[]= $ticket->count;
//                }elseif(!isset($newTickets[$location][$department])){
//                    $newTickets[$locationName][$department] =  0;
//                }
//            }
//
//            $newTickets[$locationName]['total'] =  (isset($newTickets[$locationName]['total'])?$newTickets[$locationName]['total']:0)+$ticket->count;
//
        }

        $newTickets = collect($newTickets);

        return Datatables::of($newTickets)->make(true);

    }

    public function commonchart_tdata()
    {
        $location = \request('location');
        $month = \request('month');
        $department_id = \request('department_id');
        $chart = \request('chart');
        $subjects = Subject::all(['id', 'name']);
        $departments = Department::all(['id', 'name']);
        $locations = config('settings.location');

        $tickets = Ticket::select(['tickets.*', \DB::raw('count(*) as count')]);
if($chart=='timely-count'){
    $months = Ticket::all([\DB::raw('DISTINCT DATE_FORMAT(sp_tickets.created_at, "%Y-%m") AS month')]);
    foreach ($months as $v) {
        $zero[$v->month] = null;

    }
    if ($department_id!='all') {
        $tickets->where('department_id', $department_id);
    }
    $tickets = $tickets->groupBy(\DB::raw('DATE_FORMAT(sp_tickets.created_at, "%Y-%m")'), 'tickets.location')->get();
    $dataArray = [];
    foreach ($tickets as $ticket) {
        $month = $ticket->created_at->format('Y-m');
        $newTickets[$ticket->location]['name'] = $locations[$ticket->location];
        $newTickets[$ticket->location]['data'][$month] = $ticket->count;
    }

    foreach ($newTickets as $tkt) {
        $new = array('data' => array_values(array_merge($zero,$tkt['data'])), 'name' => $tkt['name']);
        $dataArray[] = $new;
    }

}
elseif($chart=='subject-wise'){
    foreach ($departments as $v) {
        $zero[$v['id']] = 0;

    }

    foreach ($subjects as $k => $v) {

        $newTickets[$v['name']]['data'] = $zero;
        $newTickets[$v['name']]['name'] = $v['name'];
        $newTickets[$v['name']]['id'] = $v['id'];

    }

    if ($location!='all') {
        $tickets->where('location', $location);
    }
    if ($month!='all') {
        $tickets->where('created_at','LIKE', $month.'%');
    }
    $tickets = $tickets->groupBy('tickets.subject', 'tickets.department_id')->get();
    $dataArray = [];
    foreach ($tickets as $ticket) {

        $newTickets[$ticket->subject]['data'][$ticket->department_id] = $ticket->count;
    }

    foreach ($newTickets as $tkt) {
        $new = array('data' => array_values($tkt['data']), 'name' => $tkt['name']);
        $dataArray[] = $new;
    }
}else{

        foreach ($departments as $v) {
            $zero[$v['id']] = 0;

        }

    foreach ($locations as $x=>$y) {
        foreach ($subjects as $k => $v) {

            $newTickets[$x][$v['name']]['data'] = $zero;
            $newTickets[$x][$v['name']]['name'] = $v['name'];
            $newTickets[$x][$v['name']]['id'] = $v['id'];
        }
    }

        if ($location!='all') {
           // $tickets->where('location', $location);
        }
        if ($month!='all') {
            $tickets->where('created_at','LIKE', $month.'%');
        }

        $tickets =$tickets->groupBy('tickets.location','tickets.department_id','tickets.subject')->get();

        $dataArray = [];
        foreach ($tickets as $ticket) {

            $newTickets[$ticket->location][$ticket->subject]['data'][$ticket->department_id] = $ticket->count;
        }

    foreach ($locations as $x=>$y) {
        foreach ($newTickets[$x] as $tkt) {
            $new = array('data' => array_values($tkt['data']), 'name' => $tkt['name']);
            $dataArray[$y][] = $new;
        }
    }
    }

        return ($dataArray);

    }
}
