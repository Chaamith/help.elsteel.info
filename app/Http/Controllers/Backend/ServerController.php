<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Server;
use App\Models\Department;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Ticket;
use App\Models\Notification;
use App\Http\Requests;
use App\Http\Controllers\AppBaseController;
use Yajra\Datatables\Datatables;
use Exception;

class ServerController extends AppBaseController
{


    /**
     *
     * @return void
     *
     */
    function __construct(Server $server, Permission $permissions)
    {
        parent::__construct();
        $this->server = $server;
        $this->permissions = $permissions;
    }

    /**
     *
     * Return All clients list
     * @return App\Models\Role
     */
    public function allStatus()
    {
        $staffs = $this->server->select(['*',\DB::raw('TIMEDIFF(end,start) AS diff')])->orderBy('created_at', 'DESC')->paginate(20);

        return view('dashboard.server.index', compact('staffs'));
    }

    public function tdata()
    {
        $view = 'all';
        $servers = $this->server->select(['*',\DB::raw('TIMEDIFF(end,start) AS diff')]);

        return Datatables::of($servers)->editColumn('created_at', function ($tickets) {
            return @($tickets->created_at->diffForHumans());
        })->editColumn('location', function ($servers) {
            return   config('settings.location')[$servers->location];

        })->editColumn('status', function ($servers) {
            return   config('settings.serverStatus')[$servers->status];

        })->editColumn('server_id', function ($servers) {
            return   config('settings.servers')[$servers->server_id];

        })->addColumn('range', function ($servers) {
            return   $servers->start.' <b>to</b> '.$servers->end;

        })->addColumn('action', function ($servers) {

            return '<form action="'.route('dashboard.server.destroy', $servers->id).'" method="POST" class="ng-pristine ng-valid">'.
                                                                '<button type="submit" class="remove-btn"><i class="fa fa-trash-o"></i></button>'.
                                                                '<input name="_token" value="'.csrf_token().'" type="hidden">'.
                                                                '<input name="_method" value="DELETE" type="hidden">
                                                            </form>';
        })->make(true);

    }
    public function statusReport()
    {
        $location=request()->input('location')?:'active';
        $locations = config('settings.location');
        $serversList = config('settings.servers');
        $months = $this->server->select([\DB::raw('DISTINCT DATE_FORMAT(sp_server_status.start, "%Y-%m") AS month')])->orderBy('server_status.start', 'DESC')->get();;

        $servers = $this->server->select(['*',\DB::raw('(SUM(TIME_TO_SEC(TIMEDIFF(end,start)))) AS total'),\DB::raw('COUNT(*) AS count')]);
        if($location!='active'){

            $servers->where('location', $location);
        }
        $servers = $servers->groupBy(\DB::raw('DATE_FORMAT(sp_server_status.start, "%Y-%m")'), 'server_status.server_id')->orderBy('server_status.start', 'DESC')->get();


        $dataArray = [];
        $newTickets=[];

       // $zero=array_fill(0,count($months),null);
        foreach ($months as $v) {
            $zero[$v->month] = null;

        }

        foreach ($servers as $server) {

            $month = date_format(date_create($server->start),'Y-m');

            $newTickets[$server->server_id]['name'] = $serversList[$server->server_id];
            $newTickets[$server->server_id]['data'][$month] = intval($server->total);
        }

        foreach ($newTickets as $tkt) {

            $new = array('data' => array_values(array_merge($zero,$tkt['data'])), 'name' => $tkt['name']);

            $dataArray[] = $new;
        }



        return view('dashboard.server.report', compact('servers','dataArray','months'));
    }
    /**
     *
     * @param App\Models\User
     * @return App\Models\User
     *
     */

    public function getCreate(User $staff)
    {
        // Get all departments
        $departments = Department::lists('name', 'id');
        return view('dashboard.server.form', compact('staff', 'departments'));
    }

    public function getPageEdit($id)
    {
        $staff = $this->users->find($id);
        if ($staff) {
            return view('dashboard.staffs.form', compact('staff'));
        }
    }


    /**
     *
     * Get Edit Page For staffs
     * @param  Illuminate\Http\Request
     * @return App\Models\User
     *
     */
    public function getEdit(Request $request)
    {
        // Check the request type
        if ($request->ajax()) :

            $id = $request->get('id');

            $staff = $this->users->find($id);

            if ($staff) :
                return \Response::json(['staff' => $staff]);
            endif;

        else:
            return redirect()->route('dashboard.staffs.getPageEdit');
        endif;
    }


    /**
     *
     * Create a Staff
     *
     * @param Illuminate\Http\Request
     * @return App\Models\User
     *
     */
    public function store(Request $request)
    {

        $inputs = $request->all();
        $dates = explode(' - ', $inputs['logDateTime']);
        $inputs['start'] = trim($dates[0]);
        $inputs['end'] = trim($dates[1]);

        // Create the user
        $user = $this->server->create($inputs);
        return redirect('/dashboard/server')->with(
            'message', 'Status has been successfully created'
        );


    }

    /**
     *
     * Update Staff
     *
     * @param Illuminate\Http\Request
     * @return App\Models\User
     *
     */
    public function update(Requests\UpdateStaffsRequest $request)
    {
        $id = $request->get('staff_id');

        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $id
        ]);

        //  Check the request type
        if ($request->ajax()) :


            // Get the user the user
            $staff = $this->users->find($id);

            if ($staff):

                // Process the profile image
                $this->updateProfileImage($request, $staff);
                $parameter = ['first_name', 'last_name', 'email', 'department_id', 'role_name', 'location'];
                if (($request->input('password') != '')) {
                    array_push($parameter, 'password');
                }
                // Update the record
                $staff->fill($request->only($parameter))->save();

                return \Response::json(['success' => 'Staff has been updated']);
            endif;

        else:

            return redirect('/');

        endif;

    }


    /**
     *
     * Remove all notifications of the ticket
     *
     * @param App\Models\Ticket
     * @return App\Models\Notification
     */

    public function destroy($id)
    {
        $serverSatus = Server::findOrFail($id);

        $serverSatus->delete();
        return redirect('/dashboard/server');
    }


}
