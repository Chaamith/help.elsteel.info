<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\Notification;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{

	/**
	 *
	 * Remove single notification
	 * @param  Illuminate\Http\Request,  App\Models\Notification
	 * @return  App\Models\Notification
	 *
	 */
    public function removeNotif(Request $request, Notification $notification)
    {	
    	// Check the request type
    	if( $request->ajax() ) :
	        $id = $request->get('id');

	    	// Find the notification
	        $notification = $notification->find($id);

	        if($notification) {
	        	// If has the notification delete it.
	        	$notification->delete();

	        	return \Response::json(['success', 'Notification has been removed.']);
	        } else {
	        	return \Response::json(['success', 'Faild to remove notification.'], 500);

	        }
        else:
        	return redirect('/');
        endif;

    }



	/**
	 *
	 * Remove all notification
	 * @param  Illuminate\Http\Request,  App\Models\Notification
	 * @return  App\Models\Notification
	 *
	 */
    public function removeAllNotif(Request $request, Notification $notification)
    {	
    	// Check the request type
    	if( $request->ajax() ) :

	        	$notification->truncate();

	        	return \Response::json(['success', 'Notifications has been removed.']);
	        	
        else:
        	return redirect('/');
        endif;

    }
}
