<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Lead;
use App\Models\Ticket;
use App\Models\Notification;
use App\Models\Role;
use App\Models\User;
use App\Models\Department;
use App\Models\Subject;
use App\Http\Requests;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;

class TicketController extends AppBaseController
{

    public $ticketStatus = array('new', 'pending', 'solved');
    /**
     *
     * @return void
     * 
     */
    function __construct(Ticket $tickets, Lead $leads)
    {
        parent::__construct();
    	$this->tickets = $tickets;

        parent::__construct();
        $this->leads = $leads;
    }

    /**
     *
     * Return all tickets from database model
     * @return App\Models\Ticket
     *
     */
    public function tickets($view)
    {

        if( $view == 'all' ) {
            $tickets = $this->tickets->orderBy('created_at', 'DESC')->paginate(20);
        } else if ( $view == 'new' ) {
            $tickets = $this->tickets->where('status', 'new')->orderBy('created_at', 'DESC')->paginate(20);
        } else if ( $view == 'my' ) {
            $tickets = $this->tickets->where('assigned_to', Auth::user()->id)->orderBy('created_at', 'DESC')->paginate(20);
        }else if ( $view == 'staff' ) {
            $tickets = $this->tickets->where('assigned_to',request()->get('id'))->orderBy('created_at', 'DESC')->paginate(20);
        }  else if ( $view == 'pending' ) {
            $tickets = $this->tickets->where('status', 'pending')->orderBy('created_at', 'DESC')->paginate(20);
        } else if ( $view == 'solved' ) {
            $tickets = $this->tickets->where('status', 'solved')->orderBy('created_at', 'DESC')->paginate(20);
        } else {
            return redirect('/dashboard');
        }

        return view('dashboard.tickets.tickets', compact('tickets', 'view'));
    }
    public function home()
    {        $subjects = Subject::orderBy('name')->get();
        $departments = Department::all(['id', 'name']);
        $months = Ticket::all([\DB::raw('DISTINCT DATE_FORMAT(sp_tickets.created_at, "%Y-%m") AS month')]);

        return view('dashboard.tickets.index', array('ticketStatus' => $this->ticketStatus, 'departments' => $departments, 'subjects' => $subjects, 'months' => $months));
    }
    public function tdata()
    {
        $view = 'all';
        $tickets = $this->tickets->with('assignedTo','department','user');

        return Datatables::of($tickets)->editColumn('created_at', function ($tickets) {
            return @($tickets->created_at->diffForHumans());
        })->editColumn('subject', function ($tickets) {
        return   '<a href="'.route('dashboard.single.ticket', [ 'subject' => $tickets->FormatSubject(),'id' => $tickets->id ]).'">'. str_limit($tickets->subject, 30).'</a>';

        })->editColumn('location', function ($tickets) {
            return   config('settings.location')[$tickets->location];

        })->addColumn('action', function ($tickets) {
            return' <button class="ticket_remove remove-btn" type="submit" value="'.$tickets->id.'"><i class="fa fa-times"></i></button>';
        })->make(true);


    }
    public function report()
    {

        $subjects = Subject::orderBy('name')->get();
        $departments = Department::all(['id', 'name']);
        $months = Ticket::all([\DB::raw('DISTINCT DATE_FORMAT(sp_tickets.created_at, "%Y-%m") AS month')]);

        return view('dashboard.tickets.report', array('ticketStatus' => $this->ticketStatus, 'departments' => $departments, 'subjects' => $subjects, 'months' => $months));
    }

public function number_of_working_days($from, $to) {
        $workingDays = [1, 2, 3, 4, 5]; # date format = N (1 = Monday, ...)
        $holidayDays = []; # variable and fixed holidays

        $from = new \DateTime($from);
        $to = new \DateTime($to);
        $to->modify('+1 day');
        $interval = new \DateInterval('P1D');
        $periods = new \DatePeriod($from, $interval, $to);

        $days = 0;
        foreach ($periods as $period) {
            if (!in_array($period->format('N'), $workingDays)) continue;
            if (in_array($period->format('Y-m-d'), $holidayDays)) continue;
            if (in_array($period->format('*-m-d'), $holidayDays)) continue;
            $days++;
        }
        return $days;
    }
    public function reportTdata()
    {
        $view = 'all';
        $tickets = $this->tickets->with('assignedTo','department','user');

        return Datatables::of($tickets)->editColumn('subject', function ($tickets) {
            return   '<a href="'.route('dashboard.single.ticket', [ 'subject' => $tickets->FormatSubject(),'id' => $tickets->id ]).'">'. str_limit($tickets->subject, 30).'</a>';

        })->editColumn('location', function ($tickets) {
            return   config('settings.location')[$tickets->location];

        })->editColumn('created_at', function ($tickets) {
            return $tickets->created_at.'<br/>'.@($tickets->created_at->diffForHumans());
        })->addColumn('time', function ($tickets) {

             //   return  date('h:i:s', strtotime($tickets->created_at)-strtotime($tickets->updated_at));
                return (new \Carbon($tickets->updated_at))->diff(new \Carbon($tickets->created_at))->format('%d days %h:%I');

            //return  date('G:i', strtotime($tickets->updated_at) - strtotime($tickets->created_at));

        })->addColumn('timeOnlyWorkingHours', function ($tickets) {
            $diff=(new \Carbon($tickets->updated_at))->diff(new \Carbon($tickets->created_at));
            $created_at=$tickets->created_at->toDateString();
            $updated_at=$tickets->updated_at->toDateString();
            $carbonCreatedAt=(new \Carbon($tickets->created_at));
            $carbonUpdatedAt=(new \Carbon($tickets->updated_at));
            if($updated_at==$created_at){
                //   return  date('h:i:s', strtotime($tickets->created_at)-strtotime($tickets->updated_at));

                return $diff->format('%h:%I:%s');
            }else{
                $workinDays=$this->number_of_working_days($created_at, $updated_at);
                $createdAtOffHour=$created_at.' 18:00:00';
                $updatedAtOnHour=$updated_at.' 08:00:00';
                $firstdayTime=strtotime($createdAtOffHour)-strtotime($tickets->created_at);
                $LastdayTime=strtotime($tickets->updated_at)-strtotime($updatedAtOnHour);
                    return ($workinDays-2>=1?$workinDays.'(days)':'').gmdate("H:i:s", (abs($firstdayTime)+abs($LastdayTime)));



            }
            //return  date('G:i', strtotime($tickets->updated_at) - strtotime($tickets->created_at));

        })->make(true);


    }
    /**
     *
     * Show a sinlge ticket
     * @param $id, $subject
     * @return View
     *
     */
    public function showTickets($id, $subject)
    {
        
        // Reformat title
        $subject = Ticket::reformatTicket( $subject );

        // Find the ticket
        $ticket = $this->tickets->where( compact('id') )->first();

        if( $ticket ) {

            $replies = $ticket->replies()->paginate(15);

            $role = Role::where('name', 'staff')->first();
            $staffs = Role::find($role->id)->users()->get();

            return view('dashboard.tickets.single', compact('ticket', 'replies', 'staffs'));

        } else {
            abort(404);
        }

    }



    /**
     *
     * Search for a ticket
     * @param  Illuminate\Http\Request
     * @return App\Models\Ticket
     */
    public function searchTicket(Request $request)
    {
        $term = $request->get('term');

        $tickets = Ticket::where('subject', 'LIKE', '%' . $term . '%')->orWhere('message', 'LIKE', '%' . $term . '%')->orWhere('id', $term)->orderBy('created_at', 'DESC')->paginate(20);
        $view = '';

        return view('dashboard.tickets.tickets', compact('tickets', 'view'));
    }


    /**
     *
     * Assign The ticket to a user
     * @param  Illuminate\Http\Request, $id
     * @return App\Models\Ticket
     *
     */
    public function assignTicket(Request $request, $id)
    {
        // Check the request type
        if( $request->ajax() ) :
            // find the ticket
            $ticket = $this->tickets->find($id);

            if( $ticket ) {
                $ticket->assigned_to = $request->get('assign_user');


                $ticket->save();
                //ticket_assigned_to_staff
                $user = User::find( $ticket->assigned_to );
                Mail::send('mail.ticket_assigned_to_staff', ['user' => $user, 'ticket' => $ticket], function ($m) use ($user) {

                    $m->from($user->email, $user->fullName());

                    $m->to($user->email)->subject('Ticket Assigned.');

                });
                // return the success response
                return \Response::json(['success' => 'Ticket has been assigned.']);
            } else {
                // return the error response
                return \Response::json(['error' => 'Sorrr, can not assign the ticket.', 500]);
            }

        else :
            // redirect if the request type is not ajax
            return redirect('/');

        endif;
    }


    /**
     *
     * Update ticket status
     * @param  Illuminate\Http\Request, $id
     * @return App\Models\Ticket
     *
     */
    public function updateStatus(Request $request, $id)
    {
        $ticket_status = $request->get('ticket_status');
        $user = \Auth::user();
        // Check the request type
        if( $request->ajax() ) :
            // find the ticket
            $ticket = $this->tickets->find($id);

            if( $ticket ) {

                // Update the ticket status
                $ticket->status = $ticket_status;
                $ticket->save();

                $this->makeNotification( $ticket );


                // Update the Solved by field
                // So that we can determine who solved it
                if( $ticket_status == 'solved') {
                    $ticket->solved_by = $user->id;
                    $ticket->save();
                } else if ($ticket_status == 'pending') {
                    $ticket->solved_by = 0;
                    $ticket->save(); 
                }

                // return the success response
                return \Response::json(['success' => 'Ticket status has been updated.', 'user' => $user]);
            } else {
                // return the error response
                return \Response::json(['error' => 'Sorrr, can not update the ticket.', 500]);
            }

        else :
            // redirect if the request type is not ajax
            return redirect('/');

        endif;
    }



    /**
     *
     * Remove ticket
     * @param  Illuminate\Http\Request
     * @return App\Models\Ticket
     *
     */
    public function removeTicket(Request $request)
    {
        $id = $request->get('id');
        $user_id = \Auth::user()->id;
        // Check the request type
        if( $request->ajax() ) :
            // find the ticket
            $ticket = $this->tickets->find($id);

            if( $ticket && ( 
                $ticket->user_id == $user_id || 
                \Auth::user()->hasRole(['admin', 'staff']) )
            ) {

                // Delete the ticket associted file first
                if( $ticket->files !== NULL ) {
                    if( file_exists( public_path( 'uploads/' . $ticket->files ) ) ) {
                        unlink( public_path( 'uploads/' . $ticket->files ) );
                    }
                }

                // Remove ticket notifications
                $this->removeTicketNotifications( $ticket );

                // DElete all replies related to it
                foreach( $ticket->replies as $reply) {
                    // Delete the files of reply
                    if( $reply->file !== NULL ) {
                        if( file_exists( public_path( 'uploads/' . $reply->file ) ) ) {
                            unlink( public_path( 'uploads/' . $reply->file ) );
                        }
                    }

                    // Delete the reply
                    $reply->delete();
                }

                // Delete the ticket
                $ticket->delete();

                // return the success response
                return \Response::json(['success' => 'Ticket has been deleted.']);
            } else {
                // return the error response
                return \Response::json(['error' => 'Sorry, can not delete the ticket.', 500]);
            }

        else :
            // redirect if the request type is not ajax
            return redirect('/');

        endif;
    }

    /**
     *
     * Remove lead
     * @param  Illuminate\Http\Request
     * @return App\Models\Leads
     *
     */
    public function removeLead(Request $request)
    {
        $id = $request->get('id');
        // find the lead
        $lead = $this->leads->find($id);

        // Delete the lead associted file first
        if( $lead->files !== NULL ) {
            if( file_exists( public_path( 'uploads/' . $lead->files ) ) ) {
                unlink( public_path( 'uploads/' . $lead->files ) );
            }
        }

        //to record who is deleting
        $request['deleted_by'] = Auth::user()->id;
        $lead = $this->leads->find($id);
        $lead->fill($request->only('deleted_by'))->save();

        // Delete the lead
        $lead->delete();

        // return the success response
        return \Response::json(['success' => 'Lead has been deleted.']);

    }


    /**
     *
     * Make notification for the action
     * @param App\Models\Ticket
     * @return App\Models\Notification
     *
     */
    public function makeNotification( $ticket )
    {
        $userId = \Auth::user()->id;
        Notification::create([
            'user_id'   => $userId,
            'ticket_id'   => $ticket->id,
            'title'   => 'is now ' . $ticket->status,
            'type'   => $ticket->status
        ]);
    }



    /**
     *
     * Remove all notifications of the ticket
     * @param App\Models\Ticket
     * @return App\Models\Notification
     *
     */
    
    public function removeTicketNotifications( $ticket )
    {
        $notifications = Notification::where('ticket_id', $ticket->id)->get();

        foreach( $notifications as $notification )
        {
            $notification->delete();
        }
        // dd($notifications);
    }

}
