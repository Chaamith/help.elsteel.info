<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Ticket;
use App\Models\Department;
use App\Models\Notification;
use App\Models\Visitor;
use App\Models\Option;
use App\Models\Role;
use App\Models\Message;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PageViewComposer
{

    function __construct(Ticket $tickets, Department $departments, Option $options, Notification $notifications, Visitor $visitors)
    {

        // Get the Site options
        $this->notifications = $notifications->orderBy('created_at', 'DESC')->get();


        $this->tickets = $tickets;
        $this->all_ticket_count = $this->tickets->count();
        $this->new_ticket_count = $this->tickets->where('status', 'new')->count();
        $this->my_ticket_count = Ticket::where('assigned_to', \Auth::user()->id)->count();
        $this->pending_ticket_count = $this->tickets->where('status', 'pending')->count();
        $this->solved_ticket_count = $this->tickets->where('status', 'solved')->count();

        // Get visitors
        $this->visitors = $visitors;

        //  Get all departments
        $this->departments = $departments;
        $this->departmentsLists = $this->departments->lists('name', 'id');


        // Get the Site options
        $this->options = $options;


        // Get Clients count
        $role = Role::where('name', 'client')->first();

        $date = strtotime('-2 days');
        $this->new_clients_count = Role::find($role->id)->users()->where('created_at', '>=', $date)->count();
        $this->all_clients_count = Role::find($role->id)->users()->count();

        // Get Staffs count
        $staffRole = Role::where('name', 'staff')->first();

        $date = strtotime('-2 days');
        $this->new_staffs_count = Role::find($staffRole->id)->users()->where('created_at', '>=', $date)->count();
        $this->all_staffs_count = Role::find($staffRole->id)->users()->count();
        //All Messages Count
        $this->new_messages_count = Message::count();
    }

    public function compose(View $view)
    {
        $view
            ->with('all_ticket_count', $this->all_ticket_count)
            ->with('new_ticket_count', $this->new_ticket_count)
            ->with('pending_ticket_count', $this->pending_ticket_count)
            ->with('solved_ticket_count', $this->solved_ticket_count)
            ->with('new_clients_count', $this->new_clients_count)
            ->with('all_clients_count', $this->all_clients_count)
            ->with('new_staffs_count', $this->new_staffs_count)
            ->with('all_staffs_count', $this->all_staffs_count)
            ->with('new_messages_count', $this->new_messages_count)
            ->with('my_ticket_count', $this->my_ticket_count);
    }


    public function statistics(View $view)
    {
        // Get all days of the current month grouped by created_at
        $days = $this->visitors
            ->select('created_at')
            ->whereMonth('created_at', '=', date('m'))
            ->orderBy('created_at', 'ASC')
            ->groupBy(DB::raw('DATE(created_at)'))
            ->get();


        // Gell all visitors count for the current month groupted_by created_at
        $clients = $this->visitors
            ->select(
                DB::raw('DATE(created_at) as Date'),
                DB::raw('count(created_at) AS clientCount')
            )
            ->whereMonth('created_at', '=', date('m'))
            ->where('type', 'client')
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('created_at', 'ASC')
            ->get();


        // Gell all quests count for the current month groupted_by created_at
        $guests = $this->visitors
            ->select(
                DB::raw('DATE(created_at) as Date'),
                DB::raw('count(created_at) AS guestCount')
            )
            ->whereMonth('created_at', '=', date('m'))
            ->where('type', 'guest')
            ->groupBy(DB::raw('DATE(created_at)'))
            ->orderBy('created_at', 'ASC')
            ->get();


        $view
            ->with('days', $days)
            ->with('clients', $clients)
            ->with('guests', $guests);
    }


    /**
     *
     * Sending notification data to navbar
     *
     */
    public function navigation(View $view)
    {
        $view->with('notifications', $this->notifications);
    }

    /**
     *
     * Sending notification data to navbar
     *
     */
    public function messages_status(View $view)
    {
        $view->with('messages_status', ['working' => 'working', 'not-working' => 'not-working', 'warning' => 'warning']);
    }

    /**
     *
     * Sending data for Add Staff and Add Client modal
     *
     */
    public function departments(View $view)
    {
        $view->with('departments', $this->departmentsLists);
    }


    /**
     *
     * Send data to settings page ticket partial
     *
     */
    public function ticketDepartments(View $view)
    {
        $view->with('departments', $this->departments->all());
    }


    /**
     *
     * Send data to settings page theme partial
     *
     */
    public function themeOptions(View $view)
    {

        $style_option = $this->options
            ->where('name', 'style')
            ->first();

        $extra_css_option = $this->options
            ->where('name', 'extra_css')
            ->first();

        $view
            ->with('style', $style_option)
            ->with('extra_css', $extra_css_option);
    }
}