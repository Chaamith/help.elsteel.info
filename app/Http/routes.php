<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::auth();




// Ticket Route
Route::group(['middleware' => 'auth'], function() {

    Route::get('/', 'HomeController@index');

    Route::get('/homepage-2', 'HomeController@homepage2');
    Route::get('/it-policy',function (){
        return view('pdf');
    });
    Route::post('create-leads', [
        'as' => 'create.lead',
        'uses' => 'LeadController@createLead'
    ]);
    Route::get('all-leads', [
        'as' => 'all.leads',
        'uses' => 'LeadController@allLeads'
    ]);
    Route::get('edit-lead/{id}', [
        'as' => 'edit.lead',
        'uses' => 'LeadController@getEditLeads'
    ]);
    Route::post('edit-leads/{id}', [
        'as' => 'edit.post.lead',
        'uses' => 'LeadController@postEditLeads'
    ]);
    Route::post('/lead/remove', [
        'as'	=> 	'dashboard.lead.remove',
        'uses'	=> 	'Backend\LeadController@removeLead',
    ]);
    // Lead remove
    Route::post('/lead/remove', [
        'as'	=> 	'dashboard.lead.remove',
        'uses'	=> 	'Backend\TicketController@removeLead',
    ]);

    // test mail
    Route::get('/testmail', 'HomeController@testmail');
	Route::post('create-tickets', [
		'as' => 'create.ticket', 
		'uses' => 'TicketController@createTicket'
	]);

	Route::get('edit-ticket/{id}', [
		'as' => 'edit.ticket', 
		'uses' => 'TicketController@getEditTickets'
	]);

	Route::post('edit-tickets/{id}', [
		'as' => 'edit.post.ticket', 
		'uses' => 'TicketController@postEditTickets'
	]);

	Route::get('all-tickets', [
		'as' => 'all.tickets', 
		'uses' => 'TicketController@allTickets'
	]);

	Route::get('ticket/{id}/{subject}', [
		'as' => 'single.ticket', 
		'uses' => 'TicketController@showTickets'
	]);

	// Ticket remove
	Route::post('/ticket/remove', [
		'as'	=> 	'dashboard.ticket.remove',
		'uses'	=> 	'Backend\TicketController@removeTicket',
	]);
    // get  subjects
    Route::get('/get/subjects', [
        'as'	=> 	'get.subjects',
        'uses'	=> 	'HomeController@getSubjects',
    ]);
});


// Reply Route
Route::group(['middleware' => 'auth'], function() {

	Route::post('{id}/create-reply', [
		'as' => 'create.reply', 
		'uses' => 'ReplyController@createReply'
	]);

	Route::post('/remove-reply/{id}', [
		'as' => 'remove.reply', 
		'uses' => 'ReplyController@removeReply'
	]);

});

// Profile Settings
Route::group(['middleware' => 'auth'], function() {

	Route::get('profile/settings', [
		'as' => 'proifle.settings', 
		'uses' => 'HomeController@settings'
	]);

	 // Update profile
	Route::post('/profile/update', [
		'as'	=> 	'profile.update',
		'uses'	=> 	'Backend\SettingsController@profileUpdate',
	]);


});

//////////////////////
// Dashboard Routes //
//////////////////////
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'role:admin|staff'], 'namespace' => 'Backend'], function() {

	Route::get('/', [
		'as' => 'dashboard.index', 
		'uses' => 'DashboardController@index'
	]);

	Route::post('/get-authenticated-user', [
		'as' => 'get.auth.user', 
		'uses' => 'DashboardController@getAuthUser'
	]);
    /////////////////////////////
    // Dashboard Message Routes //
    /////////////////////////////
    Route::get('all-messages', [
        'as' => 'dashboard.all.messages',
        'uses' => 'MessageController@allMessages'
    ]);
    // Ticket remove
    Route::post('/message/remove', [
        'as'	=> 	'dashboard.message.remove',
        'uses'	=> 	'MessageController@removeMessage',
    ]);
    Route::post('/message/create', [
        'as'	=> 	'dashboard.message.create',
        'uses'	=> 	'MessageController@create',
    ]);
	/////////////////////////////
	// Dashboard Ticket Routes //
	/////////////////////////////
    Route::get('/tickets/tdata', [
        'as'	=> 	'dashboard.tickets.tdata',
        'uses'	=> 	'TicketController@tdata',
    ]);
    Route::get('/tickets/home', [
        'as'	=> 	'dashboard.tickets.home',
        'uses'	=> 	'TicketController@home',
    ]);
    Route::get('/tickets/report', [
        'as' => 'dashboard.tickets.report',
        'uses' => 'TicketController@report'
    ]);
    Route::get('/tickets/reportTdata', [
        'as'	=> 	'dashboard.tickets.reportTdata',
        'uses'	=> 	'TicketController@reportTdata',
    ]);
	Route::get('/tickets/{view}', [
		'as'	=> 	'dashboard.tickets',
		'uses'	=> 	'TicketController@tickets',
	]);


    Route::get('/tickets', function() {
		return redirect('/dashboard/tickets/all');
	});

	Route::get('/tickets/{id}/{subject}', [
		'as' => 'dashboard.single.ticket', 
		'uses' => 'TicketController@showTickets'
	]);

	// assign ticket to staff
	Route::post('/assign/ticket/{id}', [
		'as'	=> 	'ticket.assign.user',
		'uses'	=> 	'TicketController@assignTicket',
	]);

	// update ticket status
	Route::post('/update/ticket/{id}/status', [
		'as'	=> 	'update.ticket.status',
		'uses'	=> 	'TicketController@updateStatus',
	]);



	// Ticket search
	Route::get('/ticket/search', [
		'as'	=> 	'dashboard.ticket.search',
		'uses'	=> 	'TicketController@searchTicket',
	]);


	///////////////////////////////////
	// Dashboard Noticiations Routes //
	///////////////////////////////////

	// Remove single notification

	Route::post('/remove/single/notification', [
		'as'	=> 	'remove.single.notification',
		'uses'	=> 	'NotificationController@removeNotif',
	]);

	// Remove all notification
	Route::post('/remove/all/notification', [
		'as'	=> 	'remove.all.notification',
		'uses'	=> 	'NotificationController@removeAllNotif',
	]);

    ///////////////////////////////////////
    // Dashboard Reports route           //
    ///////////////////////////////////////
    Route::get('/reports/my-stats', [
        'as'	=> 	'dashboard.reports.mystats',
        'uses'	=> 	'ReportsController@mystats',
    ]);
    Route::get('/reports/mystats-tdata', [
        'as'	=> 	'dashboard.reports.mystats_tdata',
        'uses'	=> 	'ReportsController@mystats_tdata',
    ]);
    Route::get('/reports/department-vs-tickets', [
        'as'	=> 	'dashboard.reports.depvstkt',
        'uses'	=> 	'ReportsController@depvstkt',
    ]);
    Route::get('/reports/department-vs-tickets-tdata', [
        'as'	=> 	'dashboard.reports.depvstkt_tdata',
        'uses'	=> 	'ReportsController@depvstkt_tdata',
    ]);
    Route::get('/reports/client-vs-tickets', [
        'as'	=> 	'dashboard.reports.clientvstkt',
        'uses'	=> 	'ReportsController@clientvstkt',
    ]);
    Route::get('/reports/client-vs-tickets-tdata', [
        'as'	=> 	'dashboard.reports.clientvstkt_tdata',
        'uses'	=> 	'ReportsController@clientvstkt_tdata',
    ]);
    Route::get('/reports/user-vs-tickets', [
        'as'	=> 	'dashboard.reports.uservstkt',
        'uses'	=> 	'ReportsController@uservstkt',
    ]);
    Route::get('/reports/user-vs-tickets-tdata', [
        'as'	=> 	'dashboard.reports.uservstkt_tdata',
        'uses'	=> 	'ReportsController@uservstkt_tdata',
    ]);
    Route::get('/reports/location-vs-tickets', [
        'as'	=> 	'dashboard.reports.locationvstkt',
        'uses'	=> 	'ReportsController@locationvstkt',
    ]);
    Route::get('/reports/location-vs-tickets-tdata', [
        'as'	=> 	'dashboard.reports.locationvstkt_tdata',
        'uses'	=> 	'ReportsController@locationvstkt_tdata',
    ]);
    Route::get('/reports/location-vs-deptickets', [
        'as'	=> 	'dashboard.reports.locationvsdeptkt',
        'uses'	=> 	'ReportsController@locationvsdeptkt',
    ]);
    Route::get('/reports/location-vs-deptickets-tdata', [
        'as'	=> 	'dashboard.reports.locationvsdeptkt_tdata',
        'uses'	=> 	'ReportsController@locationvsdeptkt_tdata',
    ]);
    Route::get('/reports/common', [
        'as'	=> 	'dashboard.reports.commonreport',
        'uses'	=> 	'ReportsController@commonreport',
    ]);
    Route::get('/reports/common-tdata', [
        'as'	=> 	'dashboard.reports.commonreport_tdata',
        'uses'	=> 	'ReportsController@commonreport_tdata',
    ]);
    Route::get('/reports/common-chart-data', [
        'as'	=> 	'dashboard.reports.commonchart_tdata',
        'uses'	=> 	'ReportsController@commonchart_tdata',
    ]);
	///////////////////////////////////////
	// Dashboard Staff and Clients route //
	///////////////////////////////////////


	// Dashbaord Staffs
	Route::get('/staffs/all', [
		'as'	=> 	'dashboard.staffs.all',
		'uses'	=> 	'StaffController@allStaffs',
	]);

	// Staff create
	Route::get('/staffs/create', [
		'as'	=> 	'dashboard.staffs.create',
		'uses'	=> 	'StaffController@getCreate',
	]);

	// Staff Create
	Route::post('/staffs/create', [
		'as'	=> 	'dashboard.staffs.postCreate',
		'uses'	=> 	'StaffController@create',
	]);

	// Get staff edit ajax result
	Route::post('/staffs/edit', [
		'as'	=> 	'dashboard.staffs.getEdit',
		'uses'	=> 	'StaffController@getEdit',
	]);

	// Staff Update
	Route::post('/staffs/update', [
		'as'	=> 	'dashboard.staffs.update',
		'uses'	=> 	'StaffController@update',
	]);

	// Get Staff edit page
	Route::get('/staffs/get-edit/{id}', [
		'as'	=> 	'dashboard.staffs.getPageEdit',
		'uses'	=> 	'StaffController@getPageEdit',
	]);


	// Staff Remove
	Route::post('/staffs/remove', [
		'as'	=> 	'dashboard.staffs.remove',
		'uses'	=> 	'StaffController@removeStaff',
	]);

	// Dashbaord Clients
	Route::get('/clients/all', [
		'as'	=> 	'dashboard.clients.all',
		'uses'	=> 	'ClientController@allClients',
	])->middleware('staffPermission');

	Route::post('/clients/create', [
		'as'	=> 	'dashboard.clients.create',
		'uses'	=> 	'ClientController@create',
	]);
    Route::post('/clients/edit', [
        'as'	=> 	'dashboard.clients.edit',
        'uses'	=> 	'ClientController@edit',
    ]);
	Route::post('/clients/remove', [
		'as'	=> 	'dashboard.clients.remove',
		'uses'	=> 	'ClientController@removeClient',
	]);
// Server Status
    Route::get('/server/', [
        'as'	=> 	'dashboard.server.all',
        'uses'	=> 	'ServerController@allStatus',
    ]);
    Route::get('/server/report/tdata', [
        'as'	=> 	'dashboard.server.tdata',
        'uses'	=> 	'ServerController@tdata',
    ]);
    Route::get('/server/report', [
        'as'	=> 	'dashboard.server.report',
        'uses'	=> 	'ServerController@statusReport',
    ]);
    Route::get('/server/create', [
        'as'	=> 	'dashboard.server.create',
        'uses'	=> 	'ServerController@getCreate',
    ]);
    Route::post('/server/store', [
        'as'	=> 	'dashboard.server.store',
        'uses'	=> 	'ServerController@store',
    ]);
    Route::delete('/server/destroy/{id}', [
        'as'	=> 	'dashboard.server.destroy',
        'uses'	=> 	'ServerController@destroy',
    ]);
	///////////////////////////////////
	// Dashboard Settings Page Route //
	///////////////////////////////////

	// Settings page
	Route::get('/settings', [
		'as'	=> 	'dashboard.settings.index',
		'uses'	=> 	'SettingsController@index',
	]);

	// General Site Settings
	Route::post('/general/settings', [
		'as'	=> 	'dashboard.general.settings',
		'uses'	=> 	'SettingsController@generalSettings',
	]);

	// Theme Update Settings
	Route::post('/theme/update', [
		'as'	=> 	'dashboard.theme.update',
		'uses'	=> 	'SettingsController@themeUpdate',
	]);

	// Add  Department
	Route::post('/add/department', [
		'as'	=> 	'dashboard.add.department',
		'uses'	=> 	'SettingsController@addDepartment',
	]);

	// Edit  Department
	Route::post('/edit/department/{id}', [
		'as'	=> 	'dashboard.edit.department',
		'uses'	=> 	'SettingsController@editDepartment',
	]);

	// Remove  Department
	Route::post('/remove/department/{id}', [
		'as'	=> 	'dashboard.remove.department',
		'uses'	=> 	'SettingsController@removeDepartment',
	]);

	// Theme Update Settings
	Route::post('/app/settings', [
		'as'	=> 	'dashboard.app.settings',
		'uses'	=> 	'SettingsController@appSettings',
	]);




	///////////////////////
	// APPLICATIO UPDATE //
	///////////////////////

	Route::get('/update', [
		'as'	=> 	'dashboard.update',
		'uses'	=> 	'UpdateController@index',
	])->middleware('role:admin');


});
