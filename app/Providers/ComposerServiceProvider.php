<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\PageViewComposer;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Send some important data to sidebar
        view()->composer('partials.dashboard.sidebar', PageViewComposer::class);

        // Send data to navbar
        view()->composer('partials.dashboard.nav', 'App\Http\ViewComposers\PageViewComposer@navigation');

        // Send visitors data to statistics partial
        view()->composer('partials.dashboard.statistics', 'App\Http\ViewComposers\PageViewComposer@statistics');

        // Sending data for Add Staff and Add Client modal
        view()->composer('partials.dashboard.add_staff_and_client', 'App\Http\ViewComposers\PageViewComposer@departments');
        view()->composer('partials.dashboard.add_staff_and_client', 'App\Http\ViewComposers\PageViewComposer@messages_status');
        // Sending data for Edit Staff and Edit Client modal
        view()->composer('partials.dashboard.edit_staff_and_client', 'App\Http\ViewComposers\PageViewComposer@departments');

        // Sending data for Get Staff create page
        view()->composer('dashboard.staffs.form', 'App\Http\ViewComposers\PageViewComposer@departments');

        // Send data to settings page ticket partial 
        view()->composer('partials.dashboard.settings.ticket', 'App\Http\ViewComposers\PageViewComposer@ticketDepartments');

        // Send data to settings page theme partial 
        view()->composer('partials.dashboard.settings.theme', 'App\Http\ViewComposers\PageViewComposer@themeOptions');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
