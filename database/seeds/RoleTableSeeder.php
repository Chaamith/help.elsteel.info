<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	DB::table('roles')->delete();

    	// Role::truncate();

    	Role::insert([
    		[
    			'name'	=> 'admin',
    			'display_name'	=> 'Admin'
    		],
    		[
    			'name'	=> 'staff',
    			'display_name'	=> 'Staff'
    		],
    		[
    			'name'	=> 'client',
    			'display_name'	=> 'Client'
    		]
    	]);


    	// DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    }
}
