<?php

use Illuminate\Database\Seeder;
use App\Models\Option;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Option::truncate();

		Option::insert([
			[
				'name' => 'title',
				'value' => 'Support Pro'
			],
			[
				'name' => 'timezone',
				'value' => 'UTC'
			],
			[
				'name' => 'style',
				'value' => 'homepage_style_1'
			],
			[
				'name' => 'extra_css',
				'value' => ''
			],
			[
				'name' => 'logo',
				'value' => ''
			],
			[
				'name' => 'dashboard_logo',
				'value' => ''
			],
			[
				'name' => 'email_notification',
				'value' => ''
			],
			[
				'name' => 'user_registration',
				'value' => 'on'
			],
			[
				'name' => 'edit_ticket',
				'value' => 'on'
			],
			[
				'name' => 'staff_permission',
				'value' => 'can_view_clients'
			]
		]);
    }
}
