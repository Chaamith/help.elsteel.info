<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('name', 'admin')->first();

        $user = User::create([
        	'first_name'	=> 'admin',
        	'last_name'		=> 'test',
        	'email'			=> 'admin@gmail.com',
        	'password'		=> '111111',
        	'role_name'		=> 'Admin'
        ]);

        $user->attachRole($role);
    }
}
