-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.10.10    Database: eps_help
-- ------------------------------------------------------
-- Server version	5.7.18-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sp_subjects`
--

DROP TABLE IF EXISTS `sp_subjects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sp_subjects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sp_subjects`
--

LOCK TABLES `sp_subjects` WRITE;
/*!40000 ALTER TABLE `sp_subjects` DISABLE KEYS */;
INSERT INTO `sp_subjects` VALUES (1,'My Computer - Software','2016-11-30 23:44:34','2017-09-26 03:40:55'),(2,'My Computer - Hardware','2016-11-30 23:47:23','2017-09-26 03:41:00'),(3,'EPS Machine','2017-09-26 03:37:08','2017-09-26 03:41:04'),(4,'Barcode Reader','2017-09-26 03:37:51','2017-09-26 03:41:08'),(5,'EPS Software','2017-09-26 03:38:30','2017-09-26 03:43:21'),(6,'Printing','2017-09-26 03:39:08','2017-09-26 03:41:14'),(7,'File Server','2017-09-26 03:39:18','2017-09-26 03:41:19'),(8,'Vault Sever','2017-09-26 03:39:39','2017-09-26 03:41:22'),(9,'Drawing','2017-09-26 03:40:05','2017-09-26 03:41:32'),(10,'CRM','2017-09-26 03:40:23','2017-09-26 03:41:36'),(11,'Navision','2017-09-26 03:40:40','2017-09-26 03:41:40'),(12,'Internet/Wifi','2017-09-26 03:42:30','2017-09-26 03:42:30'),(13,'E-mail / Spam','2017-09-26 03:43:00','2017-09-26 03:44:17'),(14,'BOM','2017-09-26 03:43:27','2017-09-26 03:43:27'),(15,'Anti Virus','2017-09-26 03:43:42','2017-09-26 03:48:09'),(16,'Mobile Phone','2017-09-26 03:45:19','2017-09-26 03:45:19'),(17,'Construction Manuel','2017-09-26 03:45:53','2017-09-26 03:45:53'),(18,'Website','2017-09-26 03:46:09','2017-09-26 03:46:09'),(19,'Newsletter','2017-09-26 03:46:22','2017-09-26 03:46:22'),(20,'TMD Software','2017-09-26 03:47:03','2017-09-26 03:47:03'),(21,'CCTV','2017-09-26 03:47:14','2017-09-26 03:47:14');
/*!40000 ALTER TABLE `sp_subjects` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-26 16:19:57
