<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Defined Variables
    |--------------------------------------------------------------------------
    |
    | This is a set of variables that are made specific to this application
    | that are better placed here rather than in .env file.
    | Use config('your_key') to get the values.
    |
    */

    'location' => [1=>'Katunayake',2=>'Pallekele',3=>'India',4=>'Poland',5=>'Denmark'],
    'servers' => [1=>'EPS Server',2=>'File Server',3=>'Mail Server',4=>'Vault Sever',5=>'HRIS Server',6=>'CRM Server',
        7=>'Document Archive',8=>'Event Manager',9=>'Kaspersky',10=>'Cyberoam Firewall',11=>'DHCP',12=>'DNS',13=>'GIT Lab',14=>'AP 100',15=>'Internet',16=>'VPN'],
    'serverStatus' => [2=>'Maintenance',3=>'Off-Line'],

];