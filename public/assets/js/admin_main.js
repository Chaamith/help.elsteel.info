$(document).ready(function() {

    // Activating Popover Menu
    $('[data-role="popover"]').popover();


    //////////////////
    // Sidebar Menu //
    //////////////////

    // Do not follow the link if it has a dropdown  
    $('.has-dropdown > a').on('click', function(e) {
      e.preventDefault();
    });


    // If any menu is activated, then slide down the sidebar-dropdown-menu
    $('.sidebar-menu  li.has-dropdown.dropdown-active > .sidebar-dropdown-menu').slideDown();
    /*=====  Toggle the menu on click  ======*/
    $('.sidebar-menu > li.has-dropdown').on('click', function (e){
        var $this = $(this);

        $this.toggleClass('dropdown-active');

        if($this.hasClass('dropdown-active')) {
            $this.find('.sidebar-dropdown-menu').first().slideDown();
            $this.siblings().find('.sidebar-dropdown-menu').slideUp();
            $this.siblings().removeClass('dropdown-active');
        } else {
            $this.find('.sidebar-dropdown-menu').first().slideUp();        
        }

    });


    /*=====  Mobile Menu  ======*/
    $('.mb-menu').on('click', function() {
        $('.main-sidebar').toggleClass('sidebar-active');
        $('.main-content').toggleClass('sidebar-active');
        $('.navigation').toggleClass('sidebar-active');
    });


    /////////////////////////////////////
    // Assign New Staff Button trigger //
    /////////////////////////////////////
    $('button.assign-new-trigger').on('click', function() {
        var updateStatus = $('.update-status');
        // Check if the update status up box active
        if(updateStatus.hasClass('drop-active')){
            updateStatus.removeClass('drop-active');
        }

        $('.assign-staff-up-box').toggleClass('drop-active');
    });


    /////////////////////////////////////////
    // Update Ticket Status Button trigger //
    /////////////////////////////////////////
    $('button.update_status_trigger').on('click', function() {
        var assignStaffBox = $('.assign-staff-up-box');
        
        // Check if the assign new user up box active
        if(assignStaffBox.hasClass('drop-active')){
            assignStaffBox.removeClass('drop-active');
        }

        $('.update-status').toggleClass('drop-active');
    });

    /*=====  Remove up info box on click  ======*/
    $('.up-box-cancel').on('click', function() {
        var $this = $(this);
        $this.closest('.up-info-box').removeClass('drop-active');
    });


    ///////////////////
    // Message  Form Submit //
    ///////////////////
    $('#add_message_form').on('submit', function(e) {
        e.preventDefault();

        // Initialize variables
        var route= $('#add_message_form').attr('action'),
            data = new FormData( $("#add_message_form")[0] ),
            message = $('#message').val(),
            messages_status = $('#status').val();

        // Check the last name field
        if( ! message && message.length <= 3 ) {
            valid = 'invalid';
            $('.message_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.message_group').removeClass('has-error');
        }

        // Check the first name field
        if( ! messages_status && messages_status.length <= 3 ) {
            valid = 'invalid';
            $('.messages_status_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.messages_status_group').removeClass('has-error')
        }


        if( valid !== '') {
            $('.client_form_status').removeClass('alert-success').addClass('animated fadeInUp alert-danger').find('.help-block').html('Please fill out the form properly');
        } else {
            $('.client_form_status').removeClass('alert-danger').html(' ');

            // Perform the AJAX Ticket Creating Submission
            ajaxMessageCreate(route, data);

        }

        /*----------  Ajax Message Creation  ----------*/
        function ajaxMessageCreate(route, data) {
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.message_form_status').removeClass('alert-warning').addClass('animated fadeInUp alert alert-info').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing data...');
                },
                success: function(data) {
                    $('.message_form_status').removeClass('alert-warning alert-info').addClass('animated fadeInUp alert alert-success').html('<i class="fa fa-check"></i> Client has been added.');

                    // Reset the form
                    $("#add_message_form")[0].reset();

                },
                error: function(data) {

                    var errMsg = '';

                    var errors = (data.responseJSON);

                    $.each(errors, function(index, value){

                        errMsg += '<li>'+ value +'</li>';

                    });

                    $('.message_form_status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
                }
            });
        }

    });

    ///////////////////
    // Create Client //
    ///////////////////
    $('#add_client_form').on('submit', function(e) {
        e.preventDefault();

        // Initialize variables
        var route= $('#add_client_form').attr('action'),
            data = new FormData( $("#add_client_form")[0] ),
            first_name = $('#first_name').val(),
            last_name = $('#last_name').val(),
            email = $('#email').val(),
            password = $('#password').val(),
            valid = '';

        // Check if the password length is less than or equal to 6
        if( ! password && password.length <= 6) {
            valid = 'invalid';
            $('.password_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.password_group').removeClass('has-error')
        }

        // Check the email field
        if( ! email ) {
            valid = 'invalid';
            $('.email_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.email_group').removeClass('has-error')
        }

        // Check the last name field
        if( ! last_name && last_name.length <= 3 ) {
            valid = 'invalid';
            $('.last_name_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.last_name_group').removeClass('has-error')
        }

        // Check the first name field
        if( ! first_name && first_name.length <= 3 ) {
            valid = 'invalid';
            $('.first_name_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.first_name_group').removeClass('has-error')
        }


        if( valid !== '') {
            $('.client_form_status').removeClass('alert-success').addClass('animated fadeInUp alert-danger').find('.help-block').html('Please fill out the form properly');
        } else {
            $('.client_form_status').removeClass('alert-danger').html(' ');

            // Perform the AJAX Ticket Creating Submission
            ajaxClientCreate(route, data);

        }

        /*----------  Ajax Client Creation  ----------*/
        function ajaxClientCreate(route, data) {
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.client_form_status').removeClass('alert-warning').addClass('animated fadeInUp alert alert-info').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing data...');
                },
                success: function(data) {
                     $('.client_form_status').removeClass('alert-warning alert-info').addClass('animated fadeInUp alert alert-success').html('<i class="fa fa-check"></i> Client has been added.');

                     // Reset the form
                     $("#add_client_form")[0].reset();
                     $('.client_img_g label span').html('Select client image&hellip;');
                },
                error: function(data) {

                    var errMsg = '';

                    var errors = (data.responseJSON);

                    $.each(errors, function(index, value){

                        errMsg += '<li>'+ value +'</li>';

                    });

                    $('.client_form_status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
                }
            });
        }

    });


    
    //////////////////
    // Staff Client //
    //////////////////
    $('#add_staffs_form').on('submit', function(e) {
        e.preventDefault();

        // Initialize variables
        var route= $('#add_staffs_form').attr('action'),
            data = new FormData( $("#add_staffs_form")[0] ),
            first_name = $('.staff_first_name').val(),
            staff_department = $('.staff_department').val(),
            email = $('.staff_email').val(),
            password = $('.staff_password').val(),
            role = $('.staff_role').val(),
            valid = '';

        // Check if the role name
        if( ! role ) {
            valid = 'invalid';
            $('.role_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.role_group').removeClass('has-error')
        }

        // Check if the password length is less than or equal to 6
        if( ! password && password.length <= 6) {
            valid = 'invalid';
            $('.staff_password_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.staff_password_group').removeClass('has-error')
        }

        // Check the email field
        if( ! email ) {
            valid = 'invalid';
            $('.staff_email_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.staff_email_group').removeClass('has-error')
        }

        // Check the last name field
        if( ! staff_department  ) {
            valid = 'invalid';
            $('.staff_department_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.staff_department_group').removeClass('has-error')
        }

        // Check the first name field
        if( ! first_name && first_name.length <= 3 ) {
            valid = 'invalid';
            $('.staff_first_name_group').removeClass('has-success').addClass('has-error');
        } else {
            valid = '';
            $('.staff_first_name_group').removeClass('has-error')
        }


        if( valid !== '') {
            $('.staff_form_status').removeClass('alert-success').addClass('animated fadeInUp alert-danger').find('.help-block').html('Please fill out the form properly');
        } else {
            $('.staff_form_status').removeClass('alert-danger').html(' ');

            // Perform the AJAX Ticket Creating Submission
            ajaxStaffCreate(route, data);

        }

        /*----------  Ajax Client Creation  ----------*/
        function ajaxStaffCreate(route, data) {
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.staff_form_status').removeClass('alert-warning').addClass('animated fadeInUp alert alert-info').html('<i class="fa fa-circle-o-notch fa-spin"></i> Processing data...');
                },
                success: function(data) {
                     $('.staff_form_status').removeClass('alert-warning alert-info').addClass('animated fadeInUp alert alert-success').html('<i class="fa fa-check"></i> Staff has been added.');

                     // Reset the form
                     $("#add_staffs_form")[0].reset();
                     $('.staff_img_g label span').html('Select staff image&hellip;');
                },
                error: function(data) {

                    var errMsg = '';

                    var errors = (data.responseJSON);

                    $.each(errors, function(index, value){

                        errMsg += '<li>'+ value +'</li>';

                    });

                    $('.staff_form_status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
                }
            });
        }

    });


    ///////////////////////////////
    // Open the staff edit modal //
    ///////////////////////////////
    $('.staff_edit').on('click', function( e ) {

        $('.editLoader').show();
        var $this = $(this);
        var id = $this.val();
        var formID='#'+$this.attr('data-modal');

        $.ajax({
            url: siteUrl + '/dashboard/staffs/edit',
            type: 'POST',
            data: {id : id,  _token: token},
            success: function(data) {

                $('.editLoader').fadeOut();

                $(formID+' .user_id').val( data.staff.id );
                $(formID+' .staff_e_last_name').val( data.staff.last_name );
                $(formID+' .staff_e_first_name').val( data.staff.first_name );
                $(formID+' .staff_e_email').val( data.staff.email );
                $(formID+' #department_id').val( data.staff.department_id );
                $(formID+' .staff_e_role').val( data.staff.role_name );
                $(formID+' .staff_e_location').val( data.staff.location );


            },
            error: function(data) {

            }
        });  
    });

    ///////////////////////////////
    // AJAX UPDATE STAFF         //
    ///////////////////////////////
    $('#update_staffs_form').on('submit', function( e ) {
        e.preventDefault();

        var route= $('#update_staffs_form').attr('action'),
            data = new FormData( $("#update_staffs_form")[0] )

        $.ajax({
            url: route,
            type: 'POST',
            data: data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(data) {
                 $('.e_staff_form_status').removeClass('alert-warning alert-info').addClass('animated fadeInUp alert alert-success').html('<i class="fa fa-check"></i> Staff has been updated.');

                 // Reset the form
                 $("#add_staffs_form")[0].reset();
                 $('.staff_img_g label span').html('Select client image&hellip;');
            },
            error: function(data) {

                var errMsg = '';

                var errors = (data.responseJSON);

                $.each(errors, function(index, value){

                    errMsg += '<li>'+ value +'</li>';

                });

                $('.e_staff_form_status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
            }
        });  
    });
    ///////////////////////////////
    // AJAX UPDATE Client         //
    ///////////////////////////////
    $('#update_client_form').on('submit', function( e ) {
        e.preventDefault();

        var route= $('#update_client_form').attr('action'),
            data = new FormData( $("#update_client_form")[0] )

        $.ajax({
            url: route,
            type: 'POST',
            data: data,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(data) {
                $('.client_form_status').removeClass('alert-warning alert-info').addClass('animated fadeInUp alert alert-success').html('<i class="fa fa-check"></i> Client has been updated.');

                // Reset the form
                $("#update_client_form")[0].reset();
                $('.client_img_g label span').html('Select client image&hellip;');
            },
            error: function(data) {

                var errMsg = '';

                var errors = (data.responseJSON);

                $.each(errors, function(index, value){

                    errMsg += '<li>'+ value +'</li>';

                });

                $('.client_form_status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
            }
        });
    });


    //////////////////////////
    // Assign Ticket status //
    //////////////////////////
    $('.assign_user').on('click', function() {

        var $this = $(this);
        var staffName = $this.closest('.media-body').find('.media-heading').html();
        var route = $('#ticket_assign_user').attr('action'),
            data  = new FormData( $("#ticket_assign_user")[0] );

            //  Send the ajax request
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.assign_upbox').show().html('<i class="fa fa-circle-o-notch fa-spin status-success"></i>');

                    // Change the DOM according to the actions
                    $('.ticket-info-assigned-staff').html(staffName);

                },
                success: function(data) {
                     $('.assign_upbox').show().html('<i class="fa fa-check status-success"></i>');
                },
                error: function(data) {
                    $('.assign_upbox').show().html('<i class="fa fa-times status-error"></i>');
                }
            });   
    });


    //////////////////////////
    // Update ticket status //
    //////////////////////////
    $('.ticket_status').on('click', function() {

        var $this = $(this);
        var thisVal = $this.val();

        var route = $('#ticket_assign_form').attr('action'),
            data  = new FormData( $("#ticket_assign_form")[0] );


            if( !$this.hasClass('checked') ) {

                // Run the Ticket status update ajax call
                ticektStatusUpdateAJAX($this, thisVal);
            }

        function ticektStatusUpdateAJAX($this, thisVal) {
            //  Send the ajax request
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.statusUpBx').show().html('<i class="fa fa-circle-o-notch fa-spin status-success"></i>');
                },
                success: function(data) {
                    $('.statusUpBx').show().html('<i class="fa fa-check status-success"></i>');
                    $('.ticket_status').removeClass('checked');
                    $this.addClass('checked');

                    // Change the DOM according to the actions
                     $('.template-ticket-status').html('<span class="status-'+ thisVal +' "> '+ thisVal +' </span>' );

                    // Chane the DOM element according to the user actions
                     if( thisVal == 'solved' ) {
                        appendTicketSolvedByUser(data);
                     } else {
                        $('.solved_by_staff').remove();
                     }

                },
                error: function(data) {
                    $('.statusUpBx').show().html('<i class="fa fa-times status-error"></i>');
                }
            });   
        }

    }); 

    // Append the data to the table
    function appendTicketSolvedByUser(data) {

        var output = '<tr class="solved_by_staff"> <th>Solved By:</th>  <td class=" ticket-info-solved-staff">' + 
            data.user.first_name + ' ' + data.user.last_name
        '</td> </tr>';
        $('.ticket-info-body .table').append( output );
       
     
    }



    ///////////////////
    // STAFFS REMOVE //
    ///////////////////
    $('.staff_remove').on('click', function() {
        var $this = $(this);
        var id = $this.val();

        swal({   
            title: "Are you sure to delete?",   
            text: "By deleting any staff it can not be undone.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#FF474C",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "Cancel!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, 
        function(isConfirm){   
            if(isConfirm) {

                processStaffRemoveAjaxRequest($this, id);

            } else {
                swal({   
                    title: "Cancelled",   
                    type: 'error',
                    text: "Deletion action has been cancelled.",   
                    timer: 1500,   
                    showConfirmButton: true 
                });
            }
        });

    });

    // Process Staff Remove Ajax Reqeust
    function processStaffRemoveAjaxRequest($this, id) {

        //  Send the ajax request
        $.ajax({
            url: siteUrl + '/dashboard/staffs/remove',
            type: 'POST',
            data: {id : id, _token: token},
            success: function(data) {

                // Delete the row
                $this.closest('.staff_row').remove();
                // Notify the user
                swal({   
                    title: "Deleted",   
                    type: 'success',
                    text: "Deleted successfully.",   
                    timer: 2000,   
                    showConfirmButton: true 
                });

            },
            error: function(data) {

                swal({   
                    title: "Error",   
                    type: 'error',
                    text: "Sorry, an error occured. Please try again later.",   
                    timer: 1500,   
                    showConfirmButton: true 
                });
            }
        });  
    }



    /////////////////////
    // CLIENTS REMOVE  //
    /////////////////////
    $('.client_remove').on('click', function() {
        var $this = $(this);
        var id = $this.val();

        swal({   
            title: "Are you sure to delete?",   
            text: "By deleting any client it can not be undone.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#FF474C",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "Cancel!",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, 
        function(isConfirm){   
            if(isConfirm) {

                processClientRemoveAjaxRequest($this, id);

            } else {
                swal({   
                    title: "Cancelled",   
                    type: 'error',
                    text: "Deletion action has been cancelled.",   
                    timer: 1500,   
                    showConfirmButton: true 
                });
            }
        });

    });

    // Process Client Remove Ajax Reqeust
    function processClientRemoveAjaxRequest($this, id) {

        //  Send the ajax request
        $.ajax({
            url: siteUrl + '/dashboard/clients/remove',
            type: 'POST',
            data: {id : id, _token: token},
            success: function(data) {

                // Delete the row
                $this.closest('.client_row').remove();
                // Notify the user
                swal({   
                    title: "Deleted",   
                    type: 'success',
                    text: "Deleted successfully.",   
                    timer: 2000,   
                    showConfirmButton: true 
                });

            },
            error: function(data) {

                swal({   
                    title: "Error",   
                    type: 'error',
                    text: "Sorry, an error occured. Please try again later.",   
                    timer: 1500,   
                    showConfirmButton: true 
                });
            }
        });  
    }


    //////////////////////
    // General settings //
    //////////////////////
    $('#general_settings_form').on('submit', function(e) {
        e.preventDefault();

        var route = $('#general_settings_form').attr('action'),
            data  = new FormData( $("#general_settings_form")[0] );

            //  Send the ajax request
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.general_settings_btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Updating ');
                },
                success: function(data) {
                    $('.general_settings_btn').html('<i class="fa fa-check"></i> Updated ');

                    $('.form-status').removeClass('alert-warning').html(' ');

                },
                error: function(data) {
                    $('.general_settings_btn').html('<i class="fa fa-times"></i> Update ');

                    var errMsg = '';

                    var errors = (data.responseJSON);

                    $.each(errors, function(index, value){

                        errMsg += '<li>'+ value +'</li>';

                    });

                    $('.form-status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
                }
            });   
    });


    
    ///////////////////////////
    // Update theme settings //
    ///////////////////////////
    $('#theme_update_form').on('submit', function(e) {
        e.preventDefault();

        var route = $('#theme_update_form').attr('action'),
            data  = new FormData( $("#theme_update_form")[0] );

            //  Send the ajax request
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.update_theme_btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Updated ');
                },
                success: function(data) {
                    $('.update_theme_btn').html('<i class="fa fa-check"></i> Updated ');
                },
                error: function(data) {
                    $('.update_theme_btn').html('<i class="fa fa-times"></i> Update ');
                }
            });   
    });



    ////////////////////
    // Add Department //
    ////////////////////
    $('#add_department_form').on('submit', function(e) {
        e.preventDefault();

        var route = $('#add_department_form').attr('action'),
            data  = new FormData( $("#add_department_form")[0] );

            //  Send the ajax request
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.add_department_btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Add ');
                },
                success: function(data) {
                    $('.add_department_btn').html('<i class="fa fa-check"></i> Added ');
                    
                    var department = data.department;

                    // Make the list item
                    var output = '<li class="list-department">' +
                                                    '<span class="content">' +
                                                        '<span class="name">'+ department.name +'</span>' +
                                                        '<input type="text" name="name" id="input_name" onblur="editDepartmentFunc(this, '+ department.id +');" value="Plugin">' +
                                                    '</span>' +
                                               ' </li>';

                    // Append the data
                    $('.all-departments ul').append(output);

                    // Reset the form
                    $("#add_department_form")[0].reset();
                    $('.deparment_form_group .help-block').removeClass('text-error').html(' ');
                },
                error: function(data) {
                    $('.add_department_btn').html('<i class="fa fa-times"></i> Add ');

                    // Get the error data
                    var data = data.responseJSON;
                    $('.deparment_form_group .help-block').addClass('text-error').html( data.name );

                }
            });   
    });

 

    ////////////////////////
    // Remove Department  //
    ////////////////////////
    $('.list-department span.delete button').on('click', function(){
        var id = $(this).val(),
            $this = $(this);

            // Alert to the user
            swal({   
                title: "Are you sure to delete?",   
                text: "By deleting any department all tickets related to it will be un assigned to the department.",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#FF474C",   
                confirmButtonText: "Yes, delete it!",   
                cancelButtonText: "Cancel!",   
                closeOnConfirm: false,   
                closeOnCancel: false 
            }, function(isConfirm){   
                if (isConfirm) {     

                    // Run delete department function
                    deleteDepartment( id, $this);

       
                } else {     

                    // Notify the user
                    swal({   
                        title: "Cancelled",   
                        type: 'error',
                        text: "This is ok that you don't want to delete. :)",   
                        timer: 1500,   
                        showConfirmButton: true 
                    });  
                } 
            });

            // Delete Department function                        
            function deleteDepartment(id, $this){

                $.ajax({
                    url: siteUrl + '/dashboard/remove/department/' + id,
                    type: 'POST',
                    data: {id : id, _token: token},
                    success: function(data) {
                        $this.closest('.list-department').remove();
                        swal({   
                            title: "Deleted",   
                            type: 'success',
                            text: "Deleted successfully.",   
                            timer: 2000,   
                            showConfirmButton: true 
                        });  
                    },
                    error: function(data) {
                        $('.form-status').addClass('animated fadeInUp alert alert-warning').html('Could not able to delete the department.');
                    }
                });  

            }
    });


    /////////////////////
    // Edit Department //
    /////////////////////
    $('.list-department span.edit button').on('click', function(){

        $(this).closest('.list-department').find('.content .name').hide();
        $(this).closest('.list-department').find('.content #input_name').show().focus();

    });




    //////////////////////////////
    // APP ENVIRONMENT settings //
    //////////////////////////////
    $('#app_settings_form').on('submit', function(e) {
        e.preventDefault();

        var route = $('#app_settings_form').attr('action'),
            data  = new FormData( $("#app_settings_form")[0] );

            //  Send the ajax request
            $.ajax({
                url: route,
                type: 'POST',
                data: data,
                dataType: 'json',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('.app_settings_btn').html('<i class="fa fa-circle-o-notch fa-spin"></i> Updating ');
                },
                success: function(data) {
                    $('.app_settings_btn').html('<i class="fa fa-check"></i> Updated ');
                    console.log(data);
                    $('.app_form_status').removeClass('alert').html(' ');

                },
                error: function(data) {
                    $('.app_settings_btn').html('<i class="fa fa-times"></i> Update ');
                    console.log(data);
                    var errMsg = '';

                    var errors = (data.responseJSON);

                    $.each(errors, function(index, value){

                        errMsg += '<li>'+ value +'</li>';

                    });

                    $('.app_form_status').removeClass('alert-success').addClass('animated fadeInUp alert alert-warning').html( errMsg );
                }
            });   
    });


    ////////////////////////////////
    // Remove Single Notification //
    ////////////////////////////////
    $('.remove_notif').on('click', function(e) {
        e.preventDefault();
        
        var $this = $(this);
        var id = $this.val();
        var notifCnt = $('.notification-count');
        var notifCount = parseInt(notifCnt.html());


        $.ajax({
            url: siteUrl + '/dashboard/remove/single/notification',
            type: 'POST',
            data: {id : id,  _token: token},
            beforeSend: function() {
                $this.closest('.notif-item').slideUp();
            },
            success: function(data) {

                notifCnt.html(notifCount - 1);

            },
            error: function(data) {

                // Notify the user
                swal({   
                    title: "Error",   
                    type: 'error',
                    text: "Sorry an error occured.",   
                    timer: 1500,   
                    showConfirmButton: true 
                });  
            }
        });  
    });



    ////////////////////////////////
    // Remove Single Notification //
    ////////////////////////////////
    $('.remove_all_notif').on('click', function(e) {

        e.preventDefault();
        var $this = $(this);

        var notifCnt = $('.notification-count');


        $.ajax({
            url: siteUrl + '/dashboard/remove/all/notification',
            type: 'POST',
            data: {_token: token},
            beforeSend: function() {
                $('.notif-item').slideUp();
                notifCnt.html(0);
            },
            success: function(data) {

            },
            error: function(data) {
                
                // Notify the user
                swal({   
                    title: "Error",   
                    type: 'error',
                    text: "Sorry an error occured.",   
                    timer: 1500,   
                    showConfirmButton: true 
                });  
            }
        });  
    });


});


/*===== On blur to the input field
        Edit Department will fire 
        and edit the department
======*/

function editDepartmentFunc(thisObject, id) {

    var $this = $(thisObject);
    var name = $(thisObject).val();
    $this.closest('.list-department').find('.content .name').html( name ).show();
    $this.closest('.list-department').find('.content #input_name').hide();

    $.ajax({
        url: siteUrl + '/dashboard/edit/department/' + id,
        type: 'POST',
        data: {id : id, name: name, _token: token},
        success: function(data) {

        },
        error: function(data) {
            
            // Notify the user
            swal({   
                title: "Error",   
                type: 'error',
                text: "Could not able to process the request. :)",   
                timer: 1500,   
                showConfirmButton: true 
            });  
        }
    });  
}